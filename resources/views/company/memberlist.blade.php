@extends('hauper_admin') 
@section('content')
<div class="row">
	
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<!-- /.box-header -->
			<div class="box-header">
				@if(Session::has('message'))
					@if(json_decode(Session::get('message'))->type == 'success')
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<p><i class='fa fa-check' aria-hidden='true'></i> {{ json_decode(Session::get('message'))->message }}</p>
					</div>
					@else
					<div class="alert alert-warning alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<p><i class='fa fa-exclamation-triangle' aria-hidden='true'></i> {{ json_decode(Session::get('message'))->message }}</i></p>
					</div>
					@endif
				@endif
			</div>
			<div class="box-body">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Member Name</th>
							<th>Member Email</th>
							<!-- <th style="text-align:center;">Status</th>
							<th style="text-align:center;">Actions</th> -->
						</tr>
					</thead>
					<tbody>
						
						@foreach ($members as $member)
						<tr>
							<td>{{$member->name}}</td>
							<td>{{$member->email}}</td>
							<!-- <td width="10%">
								@if ($member->active == '1')
								<a href="{{ env('baseURL') }}/member/togglestatus/{{$member->id}}" type="button" class="btn btn-block btn-info btn-xs">Active</a>
								@else
								<a href="{{ env('baseURL') }}/member/togglestatus/{{$member->id}}" type="button" class="btn btn-block btn-warning btn-xs">Inactive</a>
								@endif
							</td> -->
                           <!--  <td width="10%">
                            	<a href="{{ env('baseURL') }}/member/edit/{{$member->id}}" type="button" class="btn btn-block btn-info btn-xs">Edit</a>
                                <a onclick="deleteSwal('member/delete/{{$member->id}}')" type="button" class="btn btn-block btn-danger btn-xs">Delete</a>
							</td> -->
						</tr>
						@endforeach 
					
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
@endsection
@section('javascript')
<script>
$(function () {
	$('#example1').DataTable()
})

</script>
@stop