<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Applications Routes
// Route::get('/', 'Auth\LoginController@index');
// Route::get('/home',function(){
//     return redirect('/users');
// });

Route::get('/', 'SearchController@list');
Route::get('autocomplete', 'SearchController@autocomplete');
Route::get('/visitordetail/{id}', 'SearchController@viewVisitor');


Route::get('login', 'Auth\LoginController@index');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');

// Admin routes
Route::get('/profile', 'UserController@userDetail')->middleware('guest_not');
Route::post('/profile', 'UserController@updateProfile')->middleware('guest_not');
Route::get('forgotpassword','UserController@forgotPassword');
Route::post('forgotpassword','UserController@resetPassword');
Route::get('reset-password/success','UserController@resetSuccess');
Route::get('reset-password/{token}','UserController@resetLink');
Route::post('changepassword','UserController@changePassword');


// DashBord Routes
Route::get('/dashbord', 'DashbordController@dashbord')->middleware('guest_not');
Route::get('/admindashbord', 'DashbordController@admindashbordDashbord')->middleware('admin_only');

Route::get('/allcompanies', 'DashbordController@allCompanies')->middleware('admin_only');
Route::get('/allbulkmails', 'DashbordController@allBulkmails')->middleware('admin_only');


// User Module Routes
Route::get('/users', 'UserController@userList')->middleware('guest_not');
Route::get('/user/togglestatus/{id}', 'UserController@toggleStatus')->middleware('guest_not');
Route::get('/user/delete/{id}', 'UserController@deleteUser')->middleware('guest_not');
Route::get('/user/edit/{id}', 'UserController@editUser')->middleware('guest_not');
Route::get('/user/add', 'UserController@editUser')->middleware('guest_not');
Route::post('/user/add', 'UserController@addUser')->middleware('guest_not');
Route::post('/user/update', 'UserController@updateUser')->middleware('guest_not');
Route::get('/user/view/{id}', 'UserController@viewUser')->middleware('guest_not');

// Member Module Routes
Route::get('/members', 'MemberController@memberList')->middleware('guest_not');
Route::get('/activemembers', 'MemberController@activememberList')->middleware('guest_not');
Route::get('/deactivemembers', 'MemberController@deactivememberList')->middleware('guest_not');
Route::get('/member/togglestatus/{id}', 'MemberController@toggleStatus')->middleware('guest_not');
Route::get('/member/delete/{id}', 'MemberController@deleteMember')->middleware('guest_not');
Route::get('/member/edit/{id}', 'MemberController@editMember')->middleware('guest_not');
Route::get('/member/add', 'MemberController@editMember')->middleware('guest_not');
Route::post('/member/add', 'MemberController@addMember')->middleware('guest_not');
Route::post('/member/update', 'MemberController@updateMember')->middleware('guest_not');

Route::get('/member/import', 'MemberController@importMember')->middleware('guest_not');
Route::post('/member/importExcel', 'MemberController@importExcel')->middleware('guest_not');
Route::get('/member/export', 'MemberController@exportMember')->middleware('guest_not');
Route::post('/member/downloadExcel', 'MemberController@downloadExcel')->middleware('guest_not');

// Route::get('/member/view/{id}', 'MemberController@viewMember')->middleware('guest_not');

// Companies as visitor Module Routes
Route::get('/companies', 'CompanyController@companyList')->middleware('guest_not');
Route::get('/company/add', 'CompanyController@editCompany')->middleware('guest_not');
Route::post('/company/add', 'CompanyController@addCompany')->middleware('guest_not');
Route::get('/company/edit/{id}', 'CompanyController@editCompany')->middleware('guest_not');
Route::post('/company/update', 'CompanyController@updateCompany')->middleware('guest_not');
Route::get('/company/delete/{id}', 'CompanyController@deleteCompany')->middleware('guest_not');
Route::get('/company/companystatus/{id}', 'CompanyController@companystatus')->middleware('guest_not');
Route::get('/companies/{id}', 'CompanyController@memberBycompany')->middleware('guest_not');

Route::get('/company/getcompanydetail', 'CompanyController@getCompanyDetail')->middleware('guest_not');

//import export visitor as company
Route::get('/company/import', 'CompanyController@importMember')->middleware('guest_not');
Route::post('/company/importExcel', 'CompanyController@importExcel')->middleware('guest_not');
Route::get('/company/export', 'CompanyController@exportMember')->middleware('guest_not');
Route::post('/company/downloadExcel', 'CompanyController@downloadExcel')->middleware('guest_not');



// BulkMail Module Routes
Route::get('/bulkmails', 'BulkMailController@bulkmailList')->middleware('guest_not');
Route::post('mail/add', 'BulkMailController@addBulkmail')->middleware('guest_not');
Route::get('memberdetail/{id}', 'BulkMailController@memberListByresolution')->middleware('guest_not');
Route::get('/settings', 'BulkMailController@sendMail')->middleware('guest_not');
Route::get('/getroledetail', 'BulkMailController@getroledetail')->middleware('guest_not');

Route::get('get-member-detail-by-id', 'BulkMailController@getMemberDetailById');
Route::post('update-member-detail-by-id', 'BulkMailController@updateMemberDetailById');
Route::get('get-mail-detail-by-id', 'BulkMailController@getMailDetailById');


Route::get('resend-member-mail/{MEMBERID}', 'BulkMailController@sendMailMember');

Route::get('sendmailbulkmailapproved', 'BulkMailController@sendmailbulkmailapproved');
// Route::get('/bulkmail/memberedit/{id}', 'BulkMailController@bulkMemberEdit')->middleware('guest_not');

// Route::get('/settings', 'SettingController@sendMail')->middleware('guest_not');

// // Designation Module Routes
// Route::get('/designations', 'DesignationController@designationList')->middleware('admin_only');
// Route::get('/designation/add', 'DesignationController@editDesignation')->middleware('admin_only');
// Route::post('/designation/add', 'DesignationController@addDesignation')->middleware('admin_only');
// Route::get('/designation/edit/{id}', 'DesignationController@editDesignation')->middleware('admin_only');
// Route::post('/designation/update', 'DesignationController@updateDesignation')->middleware('admin_only');
// Route::get('/designation/delete/{id}', 'DesignationController@deleteDesignation')->middleware('admin_only');
// Route::get('/designation/designationstatus/{id}', 'DesignationController@designationstatus')->middleware('admin_only');

// // Department Module Routes
// Route::get('/departments', 'DepartmentController@departmentList')->middleware('admin_only');
// Route::get('/department/add', 'DepartmentController@editDepartment')->middleware('admin_only');
// Route::post('/department/add', 'DepartmentController@addDepartment')->middleware('admin_only');
// Route::get('/department/edit/{id}', 'DepartmentController@editDepartment')->middleware('admin_only');
// Route::post('/department/update', 'DepartmentController@updateDepartment')->middleware('admin_only');
// Route::get('/department/delete/{id}', 'DepartmentController@deleteDepartment')->middleware('admin_only');
// Route::get('/department/departmentstatus/{id}', 'DepartmentController@departmentstatus')->middleware('admin_only');


// // State Module Routes
// Route::get('/states', 'StateController@stateList')->middleware('admin_only');
// Route::get('/state/add', 'StateController@editState')->middleware('admin_only');
// Route::post('/state/add', 'StateController@addState')->middleware('admin_only');
// Route::get('/state/edit/{id}', 'StateController@editState')->middleware('admin_only');
// Route::post('/state/update', 'StateController@updateState')->middleware('admin_only');
// Route::get('/state/delete/{id}', 'StateController@deleteState')->middleware('admin_only');
// Route::get('/state/statestatus/{id}', 'StateController@statestatus')->middleware('admin_only');

// // Pincode Module Routes
// Route::get('/pincodes', 'PincodeController@pincodeList')->middleware('admin_only');
// Route::get('/pincode/add', 'PincodeController@editPincode')->middleware('admin_only');
// Route::post('/pincode/add', 'PincodeController@addPincode')->middleware('admin_only');
// Route::get('/pincode/edit/{id}', 'PincodeController@editPincode')->middleware('admin_only');
// Route::post('/pincode/update', 'PincodeController@updatePincode')->middleware('admin_only');
// Route::get('/pincode/delete/{id}', 'PincodeController@deletePincode')->middleware('admin_only');
// Route::get('/pincode/pincodestatus/{id}', 'PincodeController@pincodestatus')->middleware('admin_only');


// Role Module Routes with Master Role
Route::get('/roles', 'RoleController@roleList')->middleware('guest_not');
Route::get('/role/add', 'RoleController@editRole')->middleware('guest_not');
Route::post('/role/add', 'RoleController@addRole')->middleware('guest_not');
Route::get('/role/edit/{id}', 'RoleController@editRole')->middleware('guest_not');
Route::post('/role/update', 'RoleController@updateRole')->middleware('guest_not');
Route::get('/role/delete/{id}', 'RoleController@deleteRole')->middleware('guest_not');
Route::get('/role/rolestatus/{id}', 'RoleController@rolestatus')->middleware('guest_not');


Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    Artisan::call('config:clear');

    return "Cache is cleared";
});