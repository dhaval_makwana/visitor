<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\Hash;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Mail;


class UserController extends Controller{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	
	public function __construct(){
		//session_start();
	}

	public function userList(Request $request){
		$loginuser_id = session('userdata')->id;
		$user=User::with('Company')->where([
			['type', '=' , "0"],
			['added_by', '=', $loginuser_id]
		])->orderBy('created_at', 'desc')->get();
		
		session(['backurl' => 'users']);
		return view("user.list", ["users"=>$user, "page_title"=>"All Users"]);
	}    

	public function addUser(Request $request){
		$inputdata=$request->input();
		$password = $inputdata['password'];
		$email = $inputdata['email'];
		$inputdata['password']=bcrypt($inputdata['password']);
		$inputdata['active']="1";
		// echo $request->file('file');
		// echo "<br>";
		if($request->file('file')){
			$imageName = time().'.'.$request->file('file')->getClientOriginalExtension();
			$request->file('file')->move(
				base_path() . '/public/images/profile_pic/', $imageName
			);
			$inputdata['profile_pic']=$imageName;
		}
		// print_r($inputdata);die;
		if (User::where('email', '=', $request['email'])->exists()) {
            Session::flash('message', 'User with same email/phone number already exists!');
            return Redirect::to('user/add');
        } else{
        	$user=User::create($inputdata);
			Session::flash('message', 'User added successfully!');
			return Redirect::to('users');
        }
	}   

	public function deleteUser(Request $request, $id){
		$user=User::find($id);
		if(empty($user)){
			Session::flash('message', 'Sorry, User record not found!');
			return Redirect::to('profile');
		}else{
			$user->delete();
			Session::flash('message', 'User Deleted!');
			return Redirect::to('users');
		}
	}

	public function userDetail(Request $request){
		$id = session('userdata')->id;
		$user=User::find($id);
		if(empty($user)){
			return Redirect::back()->withErrors(['message', 'User not found!']);
		}else{
			return view("user.profile", ["user"=>$user, "page_title"=>"Profile"]);
		}
	}

	public function editUser(Request $request, $id = ''){
		$user=User::find($id);
		$loginuser_id = session('userdata')->id;
		$adminlist=User::where([
			['type', '=' , "0"],
			['added_by', '=', $loginuser_id]
		])->orderBy('name', 'asc')->get();
		if(empty($user)){
			return view("user.add", ["adminlist"=>$adminlist, "page_title"=>"Add User"]);
		}else{
			return view("user.edit", ["user"=>$user, "adminlist"=>$adminlist, "page_title"=>"Edit User"]);
		}
	}

	function updateProfile(Request $request){
		$id = session('userdata')->id;
		$user=User::find($id);
		if(empty($user)){
			return Redirect::back()->withErrors(['message', 'User not found!']);
		}else{
			$user->name=$request->name;	
			if(isset($request->password) && !empty($request->password)){
				$user->password=bcrypt($request->password);
			}

			if($request->file('file')){
				$imageName = time().'.'.$request->file('file')->getClientOriginalExtension();

				$request->file('file')->move(
					base_path() . '/public/images/profile_pic/', $imageName
				);
				$user->profile_pic=$imageName;
			}
			
			$user->save();
			if(!$user->profile_pic){
                $imageName = 'public/avatar.png';
            } else{
                $imageName = 'public/images/profile_pic/'.$user->profile_pic;
            }
            $user->profile_pic=$imageName;

			Session(['userdata' => $user]);
			Session::flash('message', 'Profile Updated!');
			return Redirect::to('profile');
		}       
	}

	function updateUser(Request $request){
		$id = $request->id;
		$user=User::find($id);
		if(empty($user)){
			return Redirect::back()->withErrors(['message', 'User not found!']);
		}else{
			if(User::where(function($result) use ($request) {
					$result->where("email" , $request['email'])->where('id' , '!=' ,$request->id);
				})->exists()) { 
				Session::flash('message', 'User with same email already exists!');
	            return Redirect::to('user/edit/'.$id);
			}else{
				$user->name=$request->name;	
				$user->email=$request->email;	
				$user->phone=$request->phone;	
				$user->address=$request->address;	
				if(isset($request->password) && !empty($request->password)){
					$user->password=bcrypt($request->password);
				}

				if($request->file('file')){
					$imageName = time().'.'.$request->file('file')->getClientOriginalExtension();

					$request->file('file')->move(
						base_path() . '/public/images/profile_pic/', $imageName
					);
					$user->profile_pic=$imageName;
				}	
				$user->save();
				Session::flash('message', 'User Updated!');

				$backurl =Session::get('backurl');
				if($backurl == 'users'){
					return Redirect::to('users');
				}else{
					return Redirect::to('user/view/'.$id);
				}
			}			
		}       
	}

	function toggleStatus(Request $request, $id){
		$user=User::find($id);
		if(empty($user)){
			return Redirect::back()->withErrors(['message', 'User not found!']);
		}else{
			$email = $user->email;
			if($user->active == '1'){
				$user->active='0';
				$message = "Account Deactivated!";
			} else{
				$user->active='1';
				$message = "Account Activated!";
			}
			$user->save();

			Session::flash('message', $message);
			return Redirect::back();
		}
	}

	public function viewUser(Request $request, $id){
		$user=User::find($id);
		session(['backurl' => 'view']);
		if($user){
			return view("user.view", ["user"=>$user, "page_title"=>"User Details"]);	
		}else{
			return Redirect::to('users');
		}
		
	}
	
	public function changepw(Request $request){
		$validator = Validator::make($request->all(), [            
			'reset_token' => 'required',
			'password' => 'required',
		]);        
		if ($validator->fails()){            
			$errors=$validator->errors()->all();            
			$results['status']="error";
			$results['message']=implode("<br>",$errors);            
		}else{                                    
			$user=User::where('reset_token',$request->reset_token)->where('valid_reset_token',1)->first();
			if(empty($user)){
				$results['status']="error";
				$results['message']="Sorry, Invalid Token.";
			}else{                
				$password = Hash::make($request->password);

				$user->password=$password;
				$user->reset_token="";
				$user->valid_reset_token=0;
				$user->save();

				$results['status']="success";
				$results['message']="Password changed!";
			}
		}
		echo json_encode($results);exit;
	}

	public function forgotPassword(){
		return view("admin.adminforgotpassword");
	}

	public function resetPassword(Request $request){
		$inputdata=$request->input();
		$validator = Validator::make($request->all(), [
            'email' => 'required',
    	]);
        if ($validator->fails()) {
            $response['status']="error";
			$response['message']="The Scrutinizer email field is required!"; 
        }else{
        	$user=User::where([
        		['email',$inputdata['email']],
        	])->first();
			
			$mail_from= env("MAIL_FROM_ADDRESS");

        	if($user){
        		$email = $inputdata['email'];
				$code = str_replace('/', '',bcrypt(uniqid()));
            	$user->reset_token = $code;
           		$user->save();
           		$templeteurl = url("reset-password");
           		$reset_link= $templeteurl.'/'.$code;
	            Mail::send('emails.adminresetpassword', ['link' => $reset_link], function ($m) use ($email,$mail_from) {
	                $m->from($mail_from, 'BulkMail');
	                $m->to($email)->subject('Bulkmail -- Reset password!');
	            });

	            Session::flash('message', 'Reset link has been sent, please follow instruction from email!');
				return Redirect::to('forgotpassword');
			}else{
				Session::flash('message', 'Your email is not registration in our system!');
				return Redirect::to('forgotpassword');
			}
			
        }
	}
	public function resetLink($token)
    {
        return view('admin.adminreset',compact('token'));
    }

    public function changepassword(Request $request){
    	$inputdata=$request->input();
        $validator = Validator::make($request->all(), [
            'reset_token' => 'required',
            'password' => 'required',
            'ConfirmPassword' => 'required|same:password',     
        ]);
        if ($validator->fails()) {
        	$errors = $validator->errors(); 
        	Session::flash('message', $errors->first('ConfirmPassword'));
			return Redirect::to('reset-password/'.$inputdata['reset_token']);         
        }else{
        	$user=User::where([
                ['reset_token','=',$inputdata['reset_token']] 
            ])->first();
            $mail_from= env("MAIL_FROM_ADDRESS");
            if($user){
                $user->password = bcrypt($inputdata['password']);
                $user->reset_token = '';
                $user->save();
                $msg='Your password has been changed';
                $email=$user->email;
                Mail::send('emails.sucess-email', ['msg' => $msg], function ($m) use ($email,$mail_from) {
                    $m->from($mail_from, 'BulkMail');
                    $m->to($email)->subject('BulkMail project -- Changed password!');
                });
                Session::flash('message', 'Password changed ');
				return Redirect::to('reset-password/success');
            }else{
            	Session::flash('message', 'Invalid Token!');
				return Redirect::to('reset-password/'.$inputdata['reset_token']);
            }
        }
    }

    public function resetSuccess(Request $request){
    	return view('sucess');
    }
}
