<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\Hash;
use App\Designation;
use App\Company;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;


class DesignationController extends Controller{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	
	public function designationList(Request $request){
		$designation=Designation::orderBy('created_at', 'desc')->get();
		return view("designation.list", ["designations"=>$designation, "page_title"=>"All Designations"]);
	}    

	public function addDesignation(Request $request){
		$message = array();
		$validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
	        if ($validator->fails()) {
	        	$message['message'] = "Name required!";
				$message['type'] = "success";
	        	Session::flash('message', json_encode($message));
	        	return Redirect::to('designations');
	        }else{
	        	$inputdata=$request->input();
				
	        	if (Designation::where('name', '=', $request['name'])->exists()) {
	        		$message['message'] = "Designation with same name already exists!";
					$message['type'] = "success";
					Session::flash('message', json_encode($message));
		            return Redirect::to('designation/add');
		        } else{
		        	$designation=Designation::create($inputdata);
		        	$message['message'] = "Designation added successfully!";
					$message['type'] = "success";
					Session::flash('message', json_encode($message));
					return Redirect::to('designations');
		        }
	        }
	} 

	public function editDesignation(Request $request, $id = ''){
		$designation=Designation::find($id);
		if(empty($designation)){
			return view("designation.add", ["page_title"=>"Add Designation"]);
		}else{
			 return view("designation.edit", ["designation"=>$designation, "page_title"=>"Edit Designation"]);
		}
	}
	public function updateDesignation(Request $request){
		$id = $request->id;
		$message = array();
		$designation=Designation::find($id);
		if(empty($designation)){
			return Redirect::back()->withErrors(['message', 'Designation not found!']);
		}else{
			if(Designation::where(function($result) use ($request) {
					$result->where("name" , $request['name'])->where('id' , '!=' ,$request->id);
				})->exists()) { 
				$message['message'] = "Designation with same name already exists!";
				$message['type'] = "success";
				Session::flash('message', json_encode($message));

				// Session::flash('message', 'Company with same Title already exists!');
	            return Redirect::to('designation/edit/'.$id);
			}else{
				$designation->name=$request->name;
				$designation->save();
				$message['message'] = "Designation Updated!";
				$message['type'] = "success";
				Session::flash('message', json_encode($message));
				return Redirect::to('designations');
			}
		}       
	}
	public function deleteDesignation(Request $request, $id){
		$message = array();
		$designation=Designation::find($id);
		if(empty($designation)){
			Session::flash('message', 'Sorry, Designation record not found!');
			return Redirect::to('designations');
		}else{
			$allreadydesignation=Designation::with('Company')->find($id);
			$alldesignation=$allreadydesignation->Company->count();
			if($alldesignation == 0){
				$designation->delete();
				$message['message'] = "Designation Deleted!";
				$message['type'] = "success";
			}else{
				$message['message'] = "Can not delete '$designation->name' as it is currently being used in an active Company!";
				$message['type'] = "error";
				}
			Session::flash('message', json_encode($message));
			return Redirect::to('designations');
		}
	}
	public function designationstatus(Request $request, $id){
		$designation=Designation::find($id);
		$message = array();
		if(empty($designation)){
			return Redirect::back()->withErrors(['message', 'Designation not found!']);
		}else{
			if($designation->status == '1'){
				$allreadydesignation=Designation::with('Company')->find($id);
				$alldesignation=$allreadydesignation->Company->count();
				if($alldesignation == 0){
					$designation->status='0';
					$message['message'] = "Designation Deactivated!";
					$message['type'] = "success";
				}else{
					$message['message'] = "Can not disable '$designation->name' as it is currently being used in an active Company!";
					$message['type'] = "error";
				}
			} else{
				$designation->status='1';
				$message['message'] = "Designation Activated!";
				$message['type'] = "success";
			}
			$designation->save();

			Session::flash('message', json_encode($message));
			return Redirect::to('designations');
		}
	}
	
}
