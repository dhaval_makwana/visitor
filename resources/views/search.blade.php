@extends('front_law') 
@section('content')
<!-- search box container start  -->
<h3 class="text-center title-color"><u>Visitor Search</u></h3>
<h6 class="text-center title-color"> Search visitor</h6>
<p>&nbsp;</p>
<div class="well">
   <div class="row">
      <div class="col-lg-10 col-lg-offset-1">
         <div class="input-group">
            <span class="input-group-addon" style="color: white; background-color: #5b518b">Visitor Search</span>
            <input type="text" autocomplete="off" id="search" class="form-control input-lg" placeholder="Enter Name Email Organization Designation Wise Here">
         </div>
      </div>
   </div>
</div>
<!-- search box container ends  -->
<div class="well">
   <div class="row">
    
      </div>
   </div>
   <hr>
   <div class="row">
     
      </div>
   </div>
   <hr>
  </div>
<div class="searchresult" id="searchresult"> </div>
@endsection
@section('javascript')
<script type="text/javascript">
   $(document).ready(function(){
       $("#search").autocomplete({
           source: "{{ env('baseURL') }}/autocomplete",
               focus: function( event, ui ) {
               $( "#search" ).val( ui.item.name ); // uncomment this line if you want to select value to search box  
               return false;
           },
           select: function( event, ui ) {
               window.location.href = ui.item.url;
           }
       }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
           var inner_html = '<a href="{{ env("baseURL") }}/visitordetail/' + item.id + '" ><div class="list_item_container"><div class="label"><h4><b>' + item.name + '</b></h4></div></div></a>';
           return $( "<li></li>" )
                   .data( "item.autocomplete", item )
                   .append(inner_html)
                   .appendTo( ul );
       };
   });
   
   
</script>
@stop