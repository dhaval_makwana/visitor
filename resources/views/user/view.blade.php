@extends('hauper_admin') 
@section('content')
<div class="row">
	<div class="box-header">
		@if(Session::has('message'))
			<div class="alert alert-info alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<p>{{ Session::get('message') }}</p>
			</div>
		@endif
	</div>
	<div class="col-md-3">
		<!-- Profile Image -->
		<div class="box box-primary">
			<div class="box-body box-profile">
				@if ($user->profile_pic)
				<img class="profile-user-img img-responsive img-circle" src="{{ env('baseURL') }}/public/images/profile_pic/{{$user->profile_pic}}">
				@else
				<img class="profile-user-img img-responsive img-circle" src="{{ env('baseURL') }}/public/avatar.png">
				@endif
				<h3 class="profile-username text-center">{{$user->name}}</h3>
				<!-- <ul class="list-group list-group-unbordered">
					<li class="list-group-item">
						
					</li>
					<li class="list-group-item">
						<b>Total Transactions</b> <a class="pull-right">$0</a>
					</li>
				</ul> -->
				@if ($user->active == '1')
				<a href="{{ env('baseURL') }}/user/togglestatus/{{$user->id}}" type="button" class="btn btn-primary btn-block"><b>User is: Active</b></a>
				@else
				<a href="{{ env('baseURL') }}/user/togglestatus/{{$user->id}}" type="button" class="btn btn-warning btn-block"><b>User is: Inactive</b></a>
				@endif
				<a href="{{ env('baseURL') }}/user/edit/{{$user->id}}" type="button" class="btn btn-info btn-block"><b>Edit User</b></a>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col -->
	<div class="col-md-9">
		<!-- About Me Box -->
		<div class="box box-primary">
			<!-- /.box-header -->
			<div class="box-body">
				<strong><i class="fa fa-envelope"></i> Email</strong>
				<p class="text-muted">
					{{$user->email}}
				</p>
				<hr>
				<strong><i class="fa fa-phone"></i> Phone</strong>
				<p class="text-muted">
					{{$user->phone ? $user->phone . '' : 'Not provided!'}} 
				</p>
				<!-- <hr>
				<strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
				<p class="text-muted">{{$user->address ? $user->address . ', ' : 'Not provided!'}}{{$user->city ? $user->city . ', ' : ''}}{{$user->state ? $user->state . ', ' : ''}}{{$user->zip ? $user->zip . '' : ''}}</p>
				<hr>
				<strong><i class="fa fa-credit-card margin-r-5"></i> Transactions</strong>
				<p>No transactions yet!</p> -->
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>
<!-- /.row -->
@endsection
