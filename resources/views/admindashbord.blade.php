@extends('hauper_admin')

@section('content')
      <!-- Small boxes (Stat box) -->
      <style type="text/css">
     /* .small-box .icon{
      	top: 0px;
   		right: 20px;
    	font-size: 50px
      }
      .small-box:hover .icon{
      	font-size: 60px;*/
      }
      </style>
      <div class="row">
         <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$users->count()}}</h3>

              <p>All Users</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-contacts"></i>
            </div>
            <a href="{{ env('baseURL') }}/users" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-teal">
            <div class="inner">
              <h3>{{$companys->count()}}</h3>

              <p>All Visitors</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{ env('baseURL') }}/allcompanies" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
       


        
       
      </div>
@endsection