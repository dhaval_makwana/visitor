<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\Hash;
use App\Member;
use App\Company;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Mail;
use Maatwebsite\Excel\Facades\Excel;
use Log;

class MemberController extends Controller{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	
	public function __construct(){
		//session_start();
	}

	public function memberList(Request $request){
		$login_useid = session('userdata')->id;
		$member=Member::with('Company')->where('added_by',$login_useid)->orderBy('created_at', 'desc')->get();
		// print_r($member);die;
		session(['backurl' => 'members']);
		return view("member.list", ["members"=>$member, "page_title"=>"Member/Contact List "]);
	}

	public function activememberList(Request $request){
		$login_useid = session('userdata')->id;
		$member=Member::with('Company')->where([['added_by',$login_useid],['active','1']])->orderBy('created_at', 'desc')->get();
		session(['backurl' => 'members']);
		return view("member.activememberlist", ["members"=>$member, "page_title"=>"Active Member/Contact List "]);
	}    
	
	public function deactivememberList(Request $request){
		$login_useid = session('userdata')->id;
		$member=Member::with('Company')->where([['added_by',$login_useid],['active','0']])->orderBy('created_at', 'desc')->get();
		session(['backurl' => 'members']);
		return view("member.deactivememberlist", ["members"=>$member, "page_title"=>"Deactive Member/Contact List "]);
	}    


	public function addMember(Request $request){
		$inputdata=$request->input();
		$email = $inputdata['email'];
		$name = $inputdata['name'];
		$inputdata['active']="1";
		// if($request->file('file')){
		// 	$imageName = time().'.'.$request->file('file')->getClientOriginalExtension();
		// 	$request->file('file')->move(
		// 		base_path() . '/public/images/profile_pic/', $imageName
		// 	);
		// 	$inputdata['profile_pic']=$imageName;
		// }

		if (Member::where([
			['email', '=', $request['email']],
			['company_id' ,'=' ,$request['company_id']],
		])->exists()) {
            Session::flash('message', 'Member with same email In this Company already exists!');
            return Redirect::to('member/add');
        } else{
        	$member=Member::create($inputdata);
			Session::flash('message', 'Member added successfully!');
			return Redirect::to('members');
        }
	}   

	public function deleteMember(Request $request, $id){
		$member=Member::find($id);
		if(empty($member)){
			Session::flash('message', 'Sorry, Member record not found!');
			return Redirect::to('profile');
		}else{
			$member->delete();
			Session::flash('message', 'Member Deleted!');
			return Redirect::to('members');
		}
	}

	public function memberDetail(Request $request){
		$id = session('userdata')->id;
		$member=Member::find($id);
		if(empty($member)){
			return Redirect::back()->withErrors(['message', 'Member not found!']);
		}else{
			return view("member.profile", ["member"=>$member, "page_title"=>"Profile"]);
		}
	}

	public function editMember(Request $request, $id = ''){
		$member=Member::find($id);
		$login_useid = session('userdata')->id;
		$company=Company::where('status', '1')->where('added_by', $login_useid)->orderBy('name', 'asc')->get();
		if(empty($member)){
			return view("member.add", ["companys"=> $company, "page_title"=>"Add Member/Contact"]);
		}else{
			return view("member.edit", ["member"=>$member, "companys"=> $company, "page_title"=>"Edit Member/Contact"]);
		}
	}

	function updateProfile(Request $request){
		$id = session('userdata')->id;
		$member=Member::find($id);
		if(empty($member)){
			return Redirect::back()->withErrors(['message', 'Member not found!']);
		}else{
			$member->name=$request->name;	
			if(isset($request->password) && !empty($request->password)){
				$member->password=bcrypt($request->password);
			}

			if($request->file('file')){
				$imageName = time().'.'.$request->file('file')->getClientOriginalExtension();

				$request->file('file')->move(
					base_path() . '/public/images/profile_pic/', $imageName
				);
				$member->profile_pic=$imageName;
			}
			
			$member->save();
			if(!$member->profile_pic){
                $imageName = 'public/avatar.png';
            } else{
                $imageName = 'public/images/profile_pic/'.$member->profile_pic;
            }
            $member->profile_pic=$imageName;

			Session(['userdata' => $member]);
			Session::flash('message', 'Profile Updated!');
			return Redirect::to('profile');
		}       
	}

	function updateMember(Request $request){
		$id = $request->id;
		$member=Member::find($id);
		if(empty($member)){
			return Redirect::back()->withErrors(['message', 'Member not found!']);
		}else{
			if(Member::where([
					['email', '=', $request['email']],
					['company_id' ,'=' ,$request['company_id']],
					['id' , '!=' ,$request->id],
					// $result->where("email" , $request['email'])->where('id' , '!=' ,$request->id);
				])->exists()) { 
				Session::flash('message', 'Member with same email already exists!');
	            return Redirect::to('member/edit/'.$id);
			}else{
				$member->name=$request->name;	
				$member->email=$request->email;	
				$member->company_id=$request->company_id;	
				$member->save();
				Session::flash('message', 'Member Updated!');

				$backurl =Session::get('backurl');
				if($backurl == 'members'){
					return Redirect::to('members');
				}else{
					return Redirect::to('member/view/'.$id);
				}
			}			
		}       
	}

	function toggleStatus(Request $request, $id){
		$member=Member::find($id);
		if(empty($member)){
			return Redirect::back()->withErrors(['message', 'Member not found!']);
		}else{
			$email = $member->email;
			if($member->active == '1'){
				$member->active='0';
				$message = "Account Deactivated!";
			} else{
				$member->active='1';
				$message = "Account Activated!";
			}
			$member->save();

			Session::flash('message', $message);
			return Redirect::back();
		}
	}

	public function viewMember(Request $request, $id){
		$member=Member::find($id);
		session(['backurl' => 'view']);
		if($user){
			return view("member.view", ["member"=>$member, "page_title"=>"Member Details"]);	
		}else{
			return Redirect::to('members');
		}
		
	}
	
	public function importMember(Request $request){
		// $company=Company::where('status', '1')->orderBy('name', 'asc')->get();
		$login_useid = session('userdata')->id;
		$company=Company::where('status', '1')->where('added_by', $login_useid)->orderBy('name', 'asc')->get();
		if(!empty($company)){
			return view("member.importMember", ["companys"=> $company, "page_title"=>"Member/Contact List Import"]);
		}
	}

    public function exportMember(Request $request){
    	// $company=Company::where('status', '1')->orderBy('name', 'asc')->get();
    	$login_useid = session('userdata')->id;
		$company=Company::where('status', '1')->where('added_by', $login_useid)->orderBy('name', 'asc')->get();
		if(!empty($company)){
			return view("member.exportMember", ["companys"=> $company, "page_title"=>"Member/Contact List Export"]);
		}
    }
 
    public function downloadExcel(Request $request){	

    	$inputdata=$request->input();
    	$added_by=session('userdata')->id;
    	
    	

    	if($added_by){

    		$data = Member::where('added_by',$added_by)->get(['name','email'])->toArray();
	        return Excel::create($added_by.'_visotorsList', function($excel) use ($data) {
	            $excel->sheet('member', function($sheet) use ($data)
	            {
	                $sheet->fromArray($data);
	            });
	        })->download('xlsx');
    	}else{
    		Session::flash('message', "Company Details Not Found!");
			return Redirect::back();
    	}
        
    }

   	public function importExcel(Request $request) {

    	$inputdata=$request->input();
		$company_id = $inputdata['company_id'];
		$added_by = $inputdata['added_by'];

        $request->validate([
            'company_id' => 'required',
            'import_file' => 'required'
        ]);
 
        $path = $request->file('import_file')->getRealPath();
        // $data = Excel::load($path)->get();
        $data = Excel::load($path, function ($reader) {
            //excel reader
            $reader->ignoreEmpty();
            // $reader->limitColumns(4);
            log::info('$reader');
        })->get();
    	log::info("Total data : " . $data->count());

        if(!empty($data) && $data->count()){
        	$isError = false;
        	$statusMsg = '';	
        	Excel::load($path, function ($reader) use (&$isError, &$statusMsg) {
            // $reader->setHeaderRow(1);
            $firstrow = $reader->first()->toArray();
            // print_r($firstrow);die;
            log::info($firstrow);
            if (isset($firstrow['sr_no']) && isset($firstrow['member_name']) &&  isset($firstrow['email'])) {
                $rows = $reader->all();
                foreach ($rows as $row) {
                	// echo $row;
                    //log::info($row->sr_no . ' ' . $row->member_name . ' ' . $row->membershares_inr . ' ' . $row->email . "<br />");
                    log::info("Check new Validation");
                    if (!preg_match("/^[a-zA-Z0-9.&@*#-_()\/ ]*$/", trim($row->member_name))) {
                        $isError = true;
                        $statusMsg = "Please verify excel sheet of member detail, Some member name are not valid.";
                    } elseif (!preg_match("/^[0-9]*$/", $row->sr_no)) {
                        $isError = true;
                        $statusMsg = "Please verify excel sheet of member detail, Some sr.no. are not valid.";
                    }  elseif (!preg_match("/^[a-zA-Z0-9@.,_-]*$/i", trim($row->email))) {
                        $isError = true;
                        $statusMsg = "Please verify excel sheet of member detail, Some emails are not valid.";
                    }
                }
            } else {
                $statusMsg = 'Title/ Column Header not found kindly verify your excel format.';
                $isError = true;
            }
        });
	        if ($isError) {
	            log::info($isError);
	            // $data = array('statusCode' => 0, 'title' => "errors", "statusMsg" => $statusMsg);
	            // return response()->json($data);
	        	return back()->with('errors', $statusMsg);

	        }
	            foreach ($data as $key => $value) {

	            	if (Member::where('email', '=', $value->email)->where('company_id', '=', $company_id)->exists()) {
	               		$arr[] = ['name' => $value->member_name, 'email' => $value->email];
	            	}else{

	            		$inputdatamember['name'] = $value->member_name;
	                    $inputdatamember['email'] = $value->email;
	                    $inputdatamember['company_id'] = $company_id;
	                    $inputdatamember['added_by'] = $added_by;
	            		Member::create($inputdatamember);
	            	}
	            }
 				// return Excel::create('existemail', function($excel) use ($arr) {
		   //          $excel->sheet('mySheet', function($sheet) use ($arr)
		   //          {
		   //              $sheet->fromArray($arr);
		   //          });
		   //      })->download('xlsx');
		        // die;
	            // if(!empty($arr)){
	            //     Member::insert($arr);
	            // }
        }
 		// $data = array('statusCode' => 1, "statusMsg" => 'Member successfully Uploaded Successfully.', 'title' => 'Success',);
   //                  return response()->json($data);
        return back()->with('success', 'Member successfully Uploaded Successfully.');
    }
	

	
	// public function importMemberVarification(Request $request){
	// 	$inputdata=$request->input();
	// 	print_r($inputdata);die;
	// 	try {
 //                log::info("Checking Validation...");
 //                $validator = Validator::make($input, [
 //                            'company_id' => 'required|int',
 //                            'inputmemberfile' => 'required|mimes:xls,xlsx',
 //                ]);

 //                if ($validator->fails()) {
 //                    Log::info("Validation Faild :- " . $validator->errors());

 //                    $messages = $validator->errors();
 //                    foreach ($messages->all(':message') as $message) {
 //                        $statusMsg = $message;
 //                    }
 //                    $data = array('statusCode' => 0, "statusMsg" => $statusMsg, 'title' => 'Error');
 //                    log::info($data);
 //                    return response()->json($data);
 //                } else {
 //                    Log::info("validation true");

 //                    DB::beginTransaction();
 //                    log::info("Start DB Transaction");

 //                    try {
 //                        log::info("Upload Share Holder Details");

 //                        $MemberFile = Input::file('inputmemberfile');
 //                        $MemberFileName = 'ShareHolderDetail_' . $request->RESOLUTIONID . '.' . $MemberFile->getClientOriginalExtension();
 //                        /* Move file into Folder
 //                          $MemberFile->move(base_path() . '/public/shareholderdetail', $MemberFileName); */

 //                        $USERID = $request->session()->get('USERID');

 //                        if (Input::hasFile('inputmemberfile')) {
 //                            $path = Input::file('inputmemberfile')->getRealPath();
 //                            //                            log::info($path);
 //                            $data = Excel::load($path, function ($reader) {
 //                                        //excel reader
 //                                        $reader->ignoreEmpty();
 //                                        //                          $reader->limitColumns(4);
 //                                        log::info('$reader');
 //                                    })->get();

 //                            log::info("Total data : " . $data->count());
 //                            if (!empty($data) && $data->count()) {
 //                                log::info('********start*******************');
 //                                $isError = false;
 //                                $statusMsg = '';
 //                                Excel::load($path, function ($reader) use (&$isError, &$statusMsg) {
 //                                    $firstrow = $reader->first()->toArray();
 //                                    //                                    log::info($firstrow);
 //                                    if (isset($firstrow['sr_no']) && isset($firstrow['member_name']) && isset($firstrow['membershares_inr']) && isset($firstrow['email'])) {
 //                                        $rows = $reader->all();
 //                                        foreach ($rows as $row) {
 //                                            //                                            log::info($row->sr_no . ' ' . $row->member_name . ' ' . $row->membershares_inr . ' ' . $row->email . "<br />");
 //                                            log::info("Check new Validation");
 //                                            if (!preg_match("/^[a-zA-Z0-9.&@*#-_()\/ ]*$/", trim($row->member_name))) {
 //                                                $isError = true;
 //                                                $statusMsg = "Please verify excel sheet of member detail, Some member name are not valid.";
 //                                            } elseif (!preg_match("/^[0-9]*$/", $row->sr_no)) {
 //                                                $isError = true;
 //                                                $statusMsg = "Please verify excel sheet of member detail, Some sr.no. are not valid.";
 //                                            }elseif (!preg_match("/^[a-zA-Z0-9@.,_-]*$/i", trim($row->email))) {
 //                                                $isError = true;
 //                                                $statusMsg = "Please verify excel sheet of member detail, Some emails are not valid.";
 //                                            }
 //                                        }
 //                                    } else {
 //                                        $statusMsg = 'Title/ Column Header not found kindly verify your excel format.';
 //                                        $isError = true;
 //                                    }
 //                                });
 //                                if ($isError) {
 //                                    log::info($isError);
 //                                    $data = array('statusCode' => 0, 'title' => "Error", "statusMsg" => $statusMsg);
 //                                    return response()->json($data);
 //                                }
 //                                //                                log::info(print_r($data, true));
 //                                foreach ($data as $value) {
 //                                    $part[] = '';
 //                                    /* Remove comma from member share */
 //                                    $membershares_inr = strip_tags($value->membershares_inr);
 //                                    $membershares_inr = trim($membershares_inr);
 //                                    $membershares_inr = preg_replace('/[$]/', "", $membershares_inr);
 //                                    $membershares_inr = preg_replace('/[,]/', "", $membershares_inr);
 //                                    //                                    log::info("member share " . $membershares_inr);
 //                                    //                                    log::info("Type Double Member shares" . (double) $membershares_inr);
 //                                    $myString = $value->member_name;
 //                                    $GrandTotal = strstr($myString, "Grand Total Shares") ? true : false;
 //                                    log::info("****************");
 //                                    log::info($value->member_name);
                                    
 //                                }
 //                            }

 //                            if (isset($ShareHolderInsert)) {
 //                                /* Move file into Folder */
 //                                $MemberFile->move(base_path() . '/public/memberdetail', $MemberFileName);
                               
 //                                /* Update only member file name */
                               
 //                                log::info("DB Changes Commited");
 //                            }
 //                        }
 //                        //return
 //                    } catch (Exception $ex) {
 //                        log::info("Db Rollback");
 //                        log::info($ex->getMessage());
 //                        $data = array('statusCode' => 0, "statusMsg" => $ex->getMessage(), "excelFormat" => 'Y', 'title' => 'Server-Error');
 //                        return response()->json($data);
 //                    }
 //                    $data = array('statusCode' => 1, "statusMsg" => 'Resolution & share holder detail Uploaded Successfully.', 'title' => 'Success', "ResolutionID" => $request->RESOLUTIONID);
 //                    return response()->json($data);
 //                }
 //            } catch (Exception $ex) {
 //                DB::rollBack();
 //                log::info("Db Rollback");
 //                log::info($ex->getMessage());
 //                $data = array('statusCode' => 0, "statusMsg" => $ex->getMessage(), 'title' => 'Error');
 //                return response()->json($data);
 //            }

	// }
}
