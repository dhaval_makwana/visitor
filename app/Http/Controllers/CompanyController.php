<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\Hash;
use App\Company;
use App\Department;
use App\Designation;
use App\Pincode;
use App\State;
use App\Role;
use App\Member;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Mail;
use Excel;
use Log;
use DB;



class CompanyController extends Controller{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	
	public function companyList(Request $request){
		$data = session()->get('userdata');
		$login_useid = $data->id;
  
	 	 $company=Company::with('Designation','Department','Pincode','State')->where('added_by',$login_useid)->orderBy('created_at', 'desc')->get(); 
		return view("company.list", ["companys"=>$company, "page_title"=>"List of Visitor"]);
	}    
	public function memberBycompany(Request $request, $id){
		$login_useid = session('userdata')->id;
		$member=Member::with('Company')->where('company_id',$id)->where('added_by',$login_useid)->orderBy('created_at', 'desc')->get();
		
		return view("company.memberlist", ["members"=>$member, "page_title"=>"Member/Contact List "]);
		
	}
	public function addCompany(Request $request){

		$inputdata=$request->input();

		$message = array();
		$validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
		$inputdata=$request->input();
        if($request->file('attchment')){
			$imageName = time().'.'.$request->file('attchment')->getClientOriginalExtension();
			$request->file('attchment')->move(
				base_path() . '/public/attchment/', $imageName
			);
			$inputdata['attchment']=$imageName;
		}

		// $inputdata['attchment']=$imageName;
		// print_r($inputdata);die;

	        if ($validator->fails()) {
	        	$message['message'] = "Name required!";
				$message['type'] = "success";
	        	Session::flash('message', json_encode($message));
	        	return Redirect::to('companies');
	        }else{

	        	if (Company::where([
	        		['name', '=', $request['name']],
					['added_by' ,'=' ,$request['added_by']],
	        	])->exists()) {
		            Session::flash('message', 'Visitor with same name already exists!');
		            return Redirect::to('company/add');
		        } else{
		        	
		        	// print_r($inputdata);die;
					$company=Company::create($inputdata);
					$message['message'] = "Visitor added successfully!";
					$message['type'] = "success";
					Session::flash('message', json_encode($message));
					return Redirect::to('companies');
		        }

	        	
	        }
	} 


	public function editCompany(Request $request,$id = ''){
           
		$company=Company::find($id);
        $userid = session('userdata')->id;
        $rol = DB::table('roles')->where('added_by',$userid)->get();
         
		 $department=Role::where('master_id','1')->get();
		 $designation=Role::where('master_id','2')->get();
		 $state=Role::where('master_id','3')->get();
		 $pincode=Role::where('master_id','4')->get();
		// $designation=Designation::orderBy('name', 'asc')->get();	
		// $state=State::orderBy('name', 'asc')->get();
		// $pincode=Pincode::orderBy('name', 'asc')->get();

		if(empty($company)){
			return view("company.add", ["rols"=> $rol,"departments"=> $department,"designations"=> $designation,"states"=> $state,"pincodes"=> $pincode]);
		}else{
			 return view("company.edit", ["company"=>$company, "rols"=> $rol,"departments"=> $department,"designations"=> $designation,"states"=> $state,"pincodes"=> $pincode]);
		}
	}

	public function updateCompany(Request $request){
		$id = $request->id;
		$message = array();
		$company=Company::find($id);
		if(empty($company)){
			return Redirect::back()->withErrors(['message', 'Visitor not found!']);
		}else{
			if (Company::where([
				['name', '=', $request['name']],
				['added_by' ,'=' ,$request['added_by']],
				['id' , '!=' ,$request->id],
			])->exists()) {
	            Session::flash('message', 'Visitor with same name already exists!');
	            return Redirect::to('company/add');
	        } else{
	        	$company->organization=$request->organization;
	        	$company->name=$request->name;
	        	$company->designation_id=$request->designation_id;
	        	$company->department_id=$request->department_id;
	        	$company->mobile=$request->mobile;
	        	$company->email=$request->email;
	        	$company->phone_number=$request->phone_number;
	        	$company->a_email=$request->a_email;
	        	$company->address=$request->address;
	        	$company->pincode_id=$request->pincode_id;
	        	$company->state_id=$request->state_id;
	        	$company->requirement=$request->requirement;
	        	$company->place=$request->place;
	        	$company->remarks=$request->remarks;
	        	$company->category=$request->category;

	        	$company->r_organization=$request->r_organization;
	        	$company->r_name=$request->r_name;
	        	$company->r_designation=$request->r_designation;
	        	$company->r_mobile=$request->r_mobile;
	        	$company->r_email=$request->r_email;
	        	$company->r_phone=$request->r_phone;
	        	$company->r_address=$request->r_address;
	        	$company->r_zipcode=$request->r_zipcode;
	        	$company->r_category=$request->r_category;
	        	$company->r_remarks=$request->r_remarks;
	        	

	        	if($request->file('attchment')){
					$imageName = time().'.'.$request->file('attchment')->getClientOriginalExtension();

					$request->file('attchment')->move(
						base_path() . '/public/attchment/', $imageName
					);
					$company->attchment=$imageName;
				}	

				// print_r($company);die;
				$company->save();
				$message['message'] = "Visitor Updated!";
				$message['type'] = "success";
				Session::flash('message', json_encode($message));
				return Redirect::to('companies');
	        }
			
		}       
	}

	public function deleteCompany(Request $request, $id){
		$message = array();
		$company=Company::find($id);
		if(empty($company)){
			Session::flash('message', 'Sorry, Visitor record not found!');
			return Redirect::to('allcompanies');
		}else{
			$allreadycompany=Company::with('Member')->find($id);
			$allcompany=$allreadycompany->member->count();
			if($allcompany == 0){
				$company->delete();
				$message['message'] = "Visitor Deleted!";
				$message['type'] = "success";
			}else{
				$message['message'] = "Can not delete '$company->name' as it is currently being used in an active member!";
				$message['type'] = "error";
				}
			Session::flash('message', json_encode($message));
			return Redirect::to('allcompanies');
		}
	}

	public function companystatus(Request $request, $id){
		$company=Company::find($id);
		$message = array();
		if(empty($company)){
			return Redirect::back()->withErrors(['message', 'Visitor not found!']);
		}else{
			if($company->status == '1'){
				$allreadycompany=Company::with('Member')->find($id);
				$allcompany=$allreadycompany->member->count();
				if($allcompany == 0){
					$company->status='0';
					$message['message'] = "Visitor Deactivated!";
					$message['type'] = "success";
				}else{
					$message['message'] = "Can not disable '$company->name' as it is currently being used in an active member!";
					$message['type'] = "error";
				}
			} else{
				$company->status='1';
				$message['message'] = "Visitor Activated!";
				$message['type'] = "success";
			}
			$company->save();

			Session::flash('message', json_encode($message));
			return Redirect::to('allcompanies');
		}
	}

	public function getCompanyDetail(Request $request){

    	$company_id = $request->company_id;
    	$companydetail=Company::where('id',$company_id)->first();
    	// echo "<pre>";
    	// print_r($companylist);
    	// die;
        return response()->json($companydetail);
    }
	public function importMember(Request $request){
		// $company=Company::where('status', '1')->orderBy('name', 'asc')->get();
		$login_useid = session('userdata')->id;
		$company=Company::where('status', '1')->where('added_by', $login_useid)->orderBy('name', 'asc')->get();
		if(!empty($company)){
			return view("company.importMember", ["companys"=> $company, "page_title"=>"Visitor List Import"]);
		}
	}

    public function exportMember(Request $request){
    	// $company=Company::where('status', '1')->orderBy('name', 'asc')->get();
    	$login_useid = session('userdata')->id;
		$company=Company::where('status', '1')->where('added_by', $login_useid)->orderBy('name', 'asc')->get();
		if(!empty($company)){
			return view("company.exportMember", ["companys"=> $company, "page_title"=>"Visitor List Export"]);
		}
    }
 
    public function downloadExcel(Request $request){	

    	$inputdata=$request->input();
    	$login_useid = session('userdata')->id;
    	
    	// $companydetail=Company::find($$login_useid);
    	// $comanyname=$companydetail->name;

    	if($login_useid){

    		$data = Company::where('added_by',$login_useid)->get(['name','email','designation','organization','department','mobile','address','zipcode','state','place','remarks','category','phone_number','a_email','requirement','r_organization','r_name','r_designation','r_mobile','r_email','r_phone','r_address','r_zipcode','r_category','r_remarks'])->toArray();
	        return Excel::create($login_useid.'_visitorList', function($excel) use ($data) {
	            $excel->sheet('member', function($sheet) use ($data)
	            {
	                $sheet->fromArray($data);
	            });
	        })->download('xlsx');
    	}else{
    		Session::flash('message', "Company Details Not Found!");
			return Redirect::back();
    	}
        
    }

   	public function importExcel(Request $request) {

    	$inputdata=$request->input();
		$added_by = $inputdata['added_by'];

        $request->validate([
            'import_file' => 'required'
        ]);
 
        $path = $request->file('import_file')->getRealPath();
        // $data = Excel::load($path)->get();
        $data = Excel::load($path, function ($reader) {
            //excel reader
            $reader->ignoreEmpty();
            // $reader->limitColumns(4);
            log::info('$reader');
        })->get();
    	log::info("Total data : " . $data->count());

        if(!empty($data) && $data->count()){
        	$isError = false;
        	$statusMsg = '';	
        	Excel::load($path, function ($reader) use (&$isError, &$statusMsg) {
            // $reader->setHeaderRow(1);
            $firstrow = $reader->first()->toArray();
            // print_r($firstrow);die;
            log::info($firstrow);
            if (isset($firstrow['sr_no']) && isset($firstrow['name']) &&  isset($firstrow['email'])) {
                $rows = $reader->all();
                foreach ($rows as $row) {
                	// echo $row;
                    //log::info($row->sr_no . ' ' . $row->member_name . ' ' . $row->membershares_inr . ' ' . $row->email . "<br />");
                    log::info("Check new Validation");
                    if (!preg_match("/^[a-zA-Z0-9.&@*#-_()\/ ]*$/", trim($row->name))) {
                        $isError = true;
                        $statusMsg = "Please verify excel sheet of Visitor detail, Some Visitor name are not valid.";
                    } elseif (!preg_match("/^[0-9]*$/", $row->sr_no)) {
                        $isError = true;
                        $statusMsg = "Please verify excel sheet of Visitor detail, Some sr.no. are not valid.";
                    }  elseif (!preg_match("/^[a-zA-Z0-9@.,_-]*$/i", trim($row->email))) {
                        $isError = true;
                        $statusMsg = "Please verify excel sheet of Visitor detail, Some emails are not valid.";
                    }
                }
            } else {
                $statusMsg = 'Title/ Column Header not found kindly verify your excel format.';
                $isError = true;
            }
        });
	        if ($isError) {
	            log::info($isError);
	            // $data = array('statusCode' => 0, 'title' => "errors", "statusMsg" => $statusMsg);
	            // return response()->json($data);
	        	return back()->with('errors', $statusMsg);

	        }
	            foreach ($data as $key => $value) {

	            	if (Company::where('email', '=', $value->email)->exists()) {
	               		$arr[] = ['name' => $value->name, 'email' => $value->email];
	            	}else{

	            		$inputdatamember['name'] = $value->name;
	                    $inputdatamember['email'] = $value->email;
	                    $inputdatamember['designation'] = $value->designation;
	                    $inputdatamember['organization'] = $value->organization;
	                    $inputdatamember['department'] = $value->department;
	                    $inputdatamember['mobile'] = $value->mobile;
	                    $inputdatamember['phone_number'] = $value->phone_number;
	                    $inputdatamember['a_email'] = $value->a_email;
	                    $inputdatamember['address'] = $value->address;
	                    $inputdatamember['zipcode'] = $value->zipcode;
	                    $inputdatamember['state'] = $value->state;
	                    $inputdatamember['place'] = $value->place;
	                    $inputdatamember['remarks'] = $value->remarks;
	                    $inputdatamember['category'] = $value->category;
	                    $inputdatamember['added_by'] = $added_by;
	                    $inputdatamember['r_organization'] = $value->r_organization;
	                    $inputdatamember['r_name'] = $value->r_name;
	                    $inputdatamember['r_designation'] = $value->r_designation;
	                    $inputdatamember['r_mobile'] = $value->r_mobile;
	                    $inputdatamember['r_email'] = $value->r_email;
	                    $inputdatamember['r_phone'] = $value->r_phone;
	                    $inputdatamember['r_address'] = $value->r_address;
	                    $inputdatamember['r_zipcode'] = $value->r_zipcode;
	                    $inputdatamember['r_category'] = $value->r_category;
	                    $inputdatamember['r_remarks'] = $value->r_remarks;
	            		Company::create($inputdatamember);
	            	}
	            }
 			
        }
 		// $data = array('statusCode' => 1, "statusMsg" => 'Member successfully Uploaded Successfully.', 'title' => 'Success',);
   //                  return response()->json($data);
        return back()->with('success', 'Visitor successfully Uploaded Successfully.');
    }
}
