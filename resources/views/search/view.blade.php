@extends('front_law') 
@section('content')

<main role="main" class="container">
	<div class="row">
		<div class="col-md-8 blog-main">
		 <!--  <h3 class="pb-3 mb-4 font-italic border-bottom">   From the Firehose  </h3> -->
		  <div class="blog-post">
		    <h2 class="blog-post-title">{{$law->name}}</h2>
		    <!-- <p class="blog-post-meta">{{ $law->created_at->format('F d, Y') }}  <b></b></p> -->
		    <p class="blog-post-meta">{{ $law->mobile }} <!-- January 1, 2014 --> <b></b></p>
		   	<p class="description"> <!-- {{$law->description}}  -->
		   	<table class="table viewcasetable">
			  <tbody>
			  	<tr>
			      <th scope="row">Name.</th>
			      <td>:</td>
			      <td>{{$law->name}}</td>
			    </tr>
			    <tr>
			      <th scope="row">Email.</th>
			      <td>:</td>
			      <td>{{$law->email}}</td>
			    </tr>
			    <tr>
			    <tr>
			      <th scope="row">Mobile</th>
			      <td>:</td>
			      <td>{{$law->mobile}}</td>
			    </tr>
			    <tr>
			      <th scope="row">Organization</th>
			      <td>:</td>
			      <td>{{$law->organization}}</td>
			    </tr>
			    <tr>
			      <th scope="row">Designation</th>
			      <td>:</td>
			      <td>{{$law->designation}}</td>
			    </tr>
			    <tr>
			      <th scope="row">Department</th>
			      <td>:</td>
			      <td>{{$law->department}}</td>
			    </tr>

			    <tr>
			      <th scope="row">Phone Number</th>
			      <td>:</td>
			      <td>{{$law->phone_number}}</td>
			    </tr>
			    <tr>
			      <th scope="row">Additional email</th>
			      <td>:</td>
			      <td>{{$law->a_email}}</td>
			    </tr>
			    <tr>
			      <th scope="row">Address</th>
			      <td>:</td>
			      <td>{{$law->address}}</td>
			    </tr>
			    <tr>
			      <th scope="row">Zipcode</th>
			      <td>:</td>
			      <td>{{$law->zipcode}}</td>
			    </tr>
			    <tr>
			      <th scope="row">State</th>
			      <td>:</td>
			      <td>{{$law->state}}</td>
			    </tr>
			    <tr>
			      <th scope="row">Requirement</th>
			      <td>:</td>
			      <td>{{$law->requirement}}</td>
			    </tr>
			    <tr>
			      <th scope="row">Place</th>
			      <td>:</td>
			      <td>{{$law->place}}</td>
			    </tr>
			    <tr>
			      <th scope="row">Remarks</th>
			      <td>:</td>
			      <td>{{$law->remarks}}</td>
			    </tr>
			    <tr>
			      <th scope="row">Category</th>
			      <td>:</td>
			      <td>{{$law->category}}</td>
			    </tr>
			   
			  
			  </tbody>
			</table>	

			



		  </div><!-- /.blog-post -->
	</div><!-- /.blog-main -->

	<aside class="col-md-4 blog-sidebar">
		<!-- Sidebar -->
	</aside><!-- /.blog-sidebar -->

	</div><!-- /.row -->

    </main>
<!-- <div class="row">
	<div class="box-header">
		@if(Session::has('message'))
			<div class="alert alert-info alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<p>{{ Session::get('message') }}</p>
			</div>
		@endif
	</div>	
</div> -->
<!-- /.row -->
@endsection
