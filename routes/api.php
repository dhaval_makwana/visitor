<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Auth Routes
Route::post('login', 'ApiController@login');
Route::post('register', 'ApiController@register');
Route::get('forgotpassword/{email}','ApiController@resetPassword');
Route::get('resetpassword/{token}','ApiController@resetLink');
Route::post('resetpassword','ApiController@doResetPassword');
Route::post('checksocial', 'ApiController@checksocial');


/*All data related routes should be under jwt verification*/
Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('details', 'ApiController@details');
    Route::get('profile/{customer_id}','ApiController@getProfile');
    Route::post('profile','ApiController@saveProfile');
    Route::get('branches','ApiController@getBranches');
    Route::get('services/{branch_id}','ApiController@getServicesByBranch');
    Route::get('barbers/{service_id}','ApiController@getBarbersByService');
    Route::get('barber/{barber_id}','ApiController@getBarberDetail');
    Route::post('book','ApiController@saveBooking');
    Route::get('availabledates/{barber_id}','ApiController@getAvailableDates');
    Route::post('availablslots','ApiController@getAvailableSlots');
    Route::get('bookings/{user_id}','ApiController@bookingDetail');
    Route::get('pastbookings/{user_id}','ApiController@pastBookings');
    Route::get('upcomingbooking/{user_id}','ApiController@upcomingBookings');


});

