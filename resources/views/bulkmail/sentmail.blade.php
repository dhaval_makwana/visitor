@extends('hauper_admin') @section('content')
<div class='row'>
	<div class='col-md-12'>
		<form name="sentmailform" id="sentmailform" class="form-horizontal" enctype="multipart/form-data">
			<meta name="csrf-token" content="{{ csrf_token() }}">
			
			<div class="form-group">
				<label for="company_id" class="col-sm-2 control-label">Master Groups </label>
				<div class="col-sm-10"> 
					<select required name="masterrole" id="masterrole" class="form-control" >
						<option value="" >Select Master Groups </option>
						@foreach($masterroles as $masterrole)
							<option value="{{$masterrole->id}}" >{{$masterrole->name}} </option>
			            @endforeach
					</select>
					
		        </div>
			</div>
            <div id="todetail" >
				<div class="form-group">
					<label class="col-sm-2 control-label">Type </label>
					<div class="col-sm-10"> 
						<select required name="role_id" id="role" class="form-control" >
							<option value="">Select Type </option>
						</select>
						
			        </div>
				</div>  
		    </div>		
			<!-- <div id='todetail' style="display: none;">  -->
				<div class="form-group">
					<label for="fromname" class="col-sm-2 control-label">Replay To Name</label>
					<div class="col-sm-10">
						<input name="fromname" type="text" class="form-control" id="fromname" placeholder="Replay To Name" >
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-sm-2 control-label">Replay To Email</label>
					<div class="col-sm-10">
						<input name="email" type="email" class="form-control" id="email" placeholder="Email" >
					</div>
				</div>
			<!-- </div> -->

			<div class="form-group">
				<label for="subject" class="col-sm-2 control-label">Subject</label>
				<div class="col-sm-10">
					<input name="subject" type="text" class="form-control" id="subject" placeholder="Subject" required>
				</div>
			</div>
			
			<div class="form-group">
				<label for="inputdescription" class="col-sm-2 control-label">Mail Content</label>
				<div class="col-sm-10">
					<textarea class="form-control editor" name="content" cols="50" rows="10" id="mailcontent"  placeholder="Description" required></textarea>
					<!-- <input name="description" type="text" class="form-control" id="inputdescription" placeholder="Description" required> -->
				</div>
			</div>

			<!-- <div class="form-group">
				<label for="file" class="col-sm-2 control-label">Attachment</label>
				<div class="col-sm-10">
					<input type="file" name="file[]" class="form-control" id="file" accept="application/pdf" multiple>
					<small class="help-block danger">Maximum file size allowed is 5MB.</small>
				</div>
			</div> -->

			<div id="file_div">
				<div class="form-group">
					<label for="file" class="col-sm-2 control-label">Attachment</label>
					<div class="col-sm-5">
						<input type="file" name="file[]" class="form-control" id="file" >
						<small class="help-block danger">Maximum file size allowed is 5 MB.</small>
					</div>
					<div class="col-sm-5">
						<button type="button" class="btn btn-primary" onclick="add_file();" > <i class="glyphicon glyphicon-plus"></i></button>
					</div>
				</div>
			</div>
			<script type="text/javascript">
				function add_file()
				{
				 $("#file_div").append("<div class='form-group'><label for='file' class='col-sm-2 control-label'>Attachment</label><div class='col-sm-5'><input id='file' type='file' name='file[]' class='form-control'></div><button type='button' class='btn btn-danger' onclick=remove_file(this);><i class='glyphicon glyphicon-remove'></i></button></div>");
				}
				function remove_file(ele)
				{
				 $(ele).parent().remove();
				}
			</script>
			{{ csrf_field() }}
			<input name="added_by" type="hidden" value="{{Session::get('userdata')->id}}">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary" id='submitmail'>Sent Mail</button>
                    <button onclick="window.history.go(-1); return false;" class="btn btn-danger" >Back</button>
                     <img id="loading_img" alt="Loading Image" name="loading_img" src="<?php echo URL::asset('public/images/loadingx2.gif'); ?>" style="height: 30px; width: 30px; display: none;" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					@if(Session::has('message'))
			        	<!-- <p class="text-info">{{ Session::get('message') }}</p> -->
			        	<p class="text-info">{{ json_decode(Session::get('message'))->message }}</p>
			        	
					@endif
				</div>
			</div>

		</form>
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
<script src="https://cdn.ckeditor.com/4.10.0/full/ckeditor.js"></script>
<script>
	CKEDITOR.replace( 'content',{
		toolbarGroups: [
		{ name: 'document',	   groups: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },			
 		{ name: 'clipboard',   groups: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },	
 		{ name: 'editing', 	   groups: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },		
 		{ name: 'forms', 	   groups: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
 		'/',																
 		{ name: 'basicstyles', groups: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
		{ name: 'paragraph', groups: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
		{ name: 'links', groups: [ 'Link', 'Unlink', 'Anchor' ] },
		{ name: 'insert', groups: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
		'/',
		// { name: 'styles', groups: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
		// { name: 'colors', groups: [ 'TextColor', 'BGColor' ] },
		// { name: 'tools', groups: [ 'Maximize', 'ShowBlocks' ] },
		// { name: 'about', groups: [ 'About' ] }
	]
	} );
</script>


@section('javascript')
<script>
$(document).ready(function () {
	   $('#masterrole').change(function(){
		    var masterroleID = $(this).val();    
		    if(masterroleID){
		       $.ajax({
		          type:"GET",
		          url:"{{ env('baseURL') }}/getroledetail?master_id="+masterroleID,
		          success:function(data){ 
		          //console.log(res);              
		           if(data){
			            $("#role").empty();
			            $("#role").append('<option value="">Select</option>');
			           //alert(res);
			           $.each(data, function() {
			              $.each(this, function(k, v){
			                $("#role").append('<option value="'+v.id+'">'+v.name+'</option>');
			              });
			            });           
		           }else{
		              $("#role").empty();
		           }
		          }
		       });
		    }else{
		      // $("#todetail").hide();	
		      $("#role").empty();
		    }      
	    });
	//Bootstrap Validation check and data pass
	$("#file").change(function () 
	{ 
	    var iSize = ($("#file")[0].files[0].size / 1024); 
	    if (iSize >= 5120) { 
	    	swal({
                title: 'Error',
                text: 'Maximum file size allowed is 5 MB',
                closeOnConfirm: true
            });
	        // $('#file').detach();
	        $('#file').val('');
	    }
	});

    $( "#sentmailform" ).submit(function( event ) {
        event.preventDefault();
        	

        	var appUrl ="{{env('baseURL')}}";

        	CKupdate();
            $("#loading_img").show();
            $("#submitmail").prop('disabled', true);
            
		  
			var formData = new FormData(this);

            $.ajax({
            	
		        url: appUrl + "/mail/add",
		        type: 'POST',
		        data: formData,
		        cache:false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    $("#loading_img").hide();
                    if (data.statusCode == 1) {
			            swal({
			                title: data.title,
			                text: data.statusMsg,
			                closeOnConfirm: true
			            });
                        // alert(data.statusMsg);
                        // console.log(data.statusMsg);
                        window.location.href = appUrl + "/bulkmails";
                    } else {
                        alert(data.statusMsg);
                        $("#submitmail").prop('disabled', false);
                    }
                },
                error: function (jqXHR, exception) {
                    // console.log(jqXHR);
                    console.log("Call showAjaxErrorMsg");
                    // showAjaxErrorMsg(jqXHR, exception);
                    $("#submitmail").prop('disabled', false);
                    $("#loading_img").hide();
                }
            });
        
     });
});
function CKupdate(){
    for ( instance in CKEDITOR.instances )
        CKEDITOR.instances[instance].updateElement();
}


// $('#company_id').change(function(){
// 	var companyID = $(this).val();   
// 	var appUrl ="{{env('baseURL')}}"; 
//     if(companyID){
//         $.ajax({
//            type:"GET",
//            url:appUrl + "/company/getcompanydetail?company_id="+companyID,
//            success:function(res){               
//             if(res){
//                 $("#todetail").show();
//                 // console.log(res);
//                 $('#fromname').val(res.toname);
//                 $('#email').val(res.email);

                        
//             }else{
//                $("#todetail").hide();
//             }
//            }
//         });
//     }else{
//         $("#todetail").hide();
//     }      
// });

</script>

@endsection
@stop