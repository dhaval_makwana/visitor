@extends('hauper_admin') @section('content')

<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="{{ env('baseURL') }}/company/add"  enctype="multipart/form-data">
                <div class="card-body">
	                <div class="row">
	                	<!-- <h1>{{ Session::get('userdata')->id }}</h1> -->
					    <div class="form-group col-md-6">
					        <label for="organization">Organization Name: <span>*</span></label>
					        <input name="organization" type="text" class="form-control" id="organization" placeholder="Organization Name" required>
					    </div>
					    <div class="form-group col-md-6">
					        <label for="name">Name of Person: <span>*</span></label>
					        <input name="name" type="text" class="form-control" id="name" placeholder="Name of Person" required>
					    </div>
					</div>
					<div class="row">
					    <div class="form-group col-md-6">
					    	<label for="designation_id">Designation: <span>*</span></label>
								<select required name="designation_id" id="designation_id" class="form-control" >
									<option value="" >Select Designation </option>
									@foreach($designations as $designation)
										<option value="{{$designation->id}}" >{{$designation->name}} </option>
						            @endforeach
								</select>
					        
					        <!-- <label for="designation">Designation: <span>*</span></label>
					        <input name="designation" type="text" class="form-control" id="designation" placeholder="Designation" required> -->
					    </div>
					    <div class="form-group col-md-6">
					    	<label for="department_id">Department: <span>*</span></label>
								<select required name="department_id" id="department_id" class="form-control" >
									<option value="" >Select Designation </option>
									@foreach($departments as $department)
										<option value="{{$department->id}}" >{{$department->name}} </option>
						            @endforeach
								</select>
					        <!-- <label for="department">Department: <span>*</span></label>
					        <input name="department" type="text" class="form-control" id="department" placeholder="Department" required> -->
					    </div>
					</div>
					<div class="row">
					    <div class="form-group col-md-6">
					        <label for="mobile">Mobile Number: <span>*</span></label>
					        <input name="mobile" type="text" class="form-control" id="mobile" placeholder="Mobile Number" required>
					    </div>
					    <div class="form-group col-md-6">
					        <label for="email">Email Id: <span>*</span></label>
					        <input name="email" type="email" class="form-control" id="email" placeholder="Email Id" required>
					    </div>
					</div>
					<div class="row">
					    <div class="form-group col-md-6">
					        <label for="phone_number">Phone Number: </label>
					        <input name="phone_number" type="text" class="form-control" id="phone_number" placeholder="Phone Number">
					    </div>
					    <div class="form-group col-md-6">
					        <label for="a_email">Alternative Email Id </label>
					        <input name="a_email" type="email" class="form-control" id="a_email" placeholder="Alternative Email Id">
					    </div>
					</div>
					<div class="row">
					    <div class="form-group col-md-6">
					        <label for="address">Address: <span>*</span></label>
					        <input name="address" type="text" class="form-control" id="address" placeholder="Address" required>
					    </div>
					    <div class="form-group col-md-6">
					    	<label for="requirement">Requirement: </label>
					        <input name="requirement" type="text" class="form-control" id="requirement" placeholder="Requirement" required>
					        
					    </div>
					</div>
					<div class="row">
					    <div class="form-group col-md-6">
					    	<label for="State">State: <span>*</span></label>
								<select required name="state_id" id="state_id" class="form-control" >
									<option value="" >Select State </option>
									@foreach($states as $state)
										<option value="{{$state->id}}" >{{$state->name}} </option>
						            @endforeach
								</select>
					        <!-- <label for="state">State: <span>*</span></label>
					        <input name="state" type="text" class="form-control" id="state" placeholder="State" required> -->
					    </div>
					    <div class="form-group col-md-6">
					    	<label for="zipcode">Pin Code: <span>*</span></label>
								<select required name="pincode_id" id="pincode_id" class="form-control" >
									<option value="" >Select Pin Code </option>
									@foreach($pincodes as $pincode)
										<option value="{{$pincode->id}}" >{{$pincode->name}} </option>
						            @endforeach
								</select>
							<!-- <label for="zipcode">Pin Code: <span>*</span></label>
					        <input name="zipcode" type="text" class="form-control" id="zipcode" placeholder="Pin Code" required> -->
					      
					    </div>
					</div>
					<div class="row">
					    <div class="form-group col-md-6">
					        <label for="place">Place: <span>*</span></label>
					        <input name="place" type="text" class="form-control" id="place" placeholder="Place" required>
					    </div>
					    <div class="form-group col-md-6">
					        <label for="remarks">Remarks: </label>
					        <input name="remarks" type="text" class="form-control" id="remarks" placeholder="Remarks" required>
					    </div>
					</div>
					<div class="row">
					    <div class="form-group col-md-6">
					        <label for="category">Category: </label>
					        <input name="category" type="text" class="form-control" id="category" placeholder="Category" required>
					    </div>
					    <div class="form-group col-md-6">
					        <label for="attachment" class="custom-file-label">Upload Attchment Document</label>
					        <input type="file" name="attchment" class="form-control" id="file">
					    </div>
					</div>
					<div class="row">
					    <div class="form-group col-md-6">
					        <label for="primate">Reference Person: Yes/ NO: </label>
					        <select id="reference_person" name='reference_person' class="form-control" onchange="showDiv('hidden_div', this)">
							   <option value="0">No</option>
							   <option value="1">Yes</option>
							</select>

					    </div>
					    <div class="form-group col-md-6">
					       
					    </div>
					</div>
					<div id="hidden_div">
						<div class="row">
						    <div class="form-group col-md-6">
						        <label for="r_organization">Organization Name: </label>
						        <input name="r_organization" type="text" class="form-control" id="r_organization" placeholder="Organization Name" >
						    </div>
						    <div class="form-group col-md-6">
						        <label for="r_name">Name of Person: </label>
						        <input name="r_name" type="text" class="form-control" id="r_name" placeholder="Name of Person" >
						    </div>
						</div>
						<div class="row">
						    <div class="form-group col-md-6">
						        <label for="r_designation">Designation: </label>
						        <input name="r_designation" type="text" class="form-control" id="r_designation" placeholder="Designation" >
						    </div>
						    <div class="form-group col-md-6">
						        <label for="r_mobile">Mobile Number: </label>
						        <input name="r_mobile" type="text" class="form-control" id="r_mobile" placeholder="Mobile Number" >
						    </div>
						</div>
						<div class="row">
						    <div class="form-group col-md-6">
						        <label for="r_email">Email Id : </label>
						        <input name="r_email" type="email" class="form-control" id="r_email" placeholder="Email Id " >
						    </div>
						    <div class="form-group col-md-6">
						        <label for="r_phone">Phone Number: </label>
						        <input name="r_phone" type="text" class="form-control" id="r_phone" placeholder="Phone Number" >
						    </div>
						</div>
						<div class="row">
						    <div class="form-group col-md-6">
						        <label for="r_address">Address : </label>
						        <input name="r_address" type="text" class="form-control" id="r_address" placeholder="Address " >
						    </div>
						    <div class="form-group col-md-6">
						        <label for="r_zipcode">Pin Code: </label>
						        <input name="r_zipcode" type="text" class="form-control" id="r_zipcode" placeholder="Pin Code" >
						    </div>
						</div>
						<div class="row">
						    <div class="form-group col-md-6">
						        <label for="r_category">Category : </label>
						        <input name="r_category" type="text" class="form-control" id="r_category" placeholder="Category " >
						    </div>
						    <div class="form-group col-md-6">
						        <label for="r_remarks">Remarks: </label>
						        <input name="r_remarks" type="text" class="form-control" id="r_remarks" placeholder="Remarks" >
						    </div>
						</div>
					</div>
						<div class='row'>
							<input name="added_by" type="hidden" value="{{Session::get('userdata')->id}}">
							<input name="status" type="hidden" value="1">
							{{ csrf_field() }}
							<div class="form-group col-md-6">
									<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
				                    <button onclick="window.history.go(-1); return false;" class="btn btn-danger" ><i class="fa fa-times" aria-hidden="true"></i> Back</button>
							</div>
							<div class="form-group col-md-6">
								@if(Session::has('message'))
						        	<p class="text-info">{{ Session::get('message') }}</p>
								@endif
							</div>
						</div>

		     	</div>
			               
            </form>
            </div>
          </div>
        </div>
      </div>
</section>

<style type="text/css">
#hidden_div {
    display: none;
}
</style>
<script type="text/javascript">

function showDiv(divId, element)
{
    document.getElementById(divId).style.display = element.value == 1 ? 'block' : 'none';
}
</script>


<!-- <div class='row'>
	<div class='col-md-12'>
		<form action="{{ env('baseURL') }}/company/add" method="post" class="form-horizontal" enctype="multipart/form-data">
			<div class='col-md-6'>
				<div class="form-group">
					<label for="name" class="col-sm-2 control-label">Name*</label>
					<div class="col-sm-10">
						<input name="name" type="text" class="form-control" id="name" placeholder="Name" required>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-sm-2 control-label">Email*</label>
					<div class="col-sm-10">
						<input name="email" type="email" class="form-control" id="email" placeholder="Email" required>
					</div>
				</div>
				<div class="form-group">
					<label for="place" class="col-sm-2 control-label">Place</label>
					<div class="col-sm-10">
						<input name="place" type="text" class="form-control" id="place" placeholder="Place"required >
					</div>
				</div>
				<div class="form-group">
					<label for="address" class="col-sm-2 control-label">Address</label>
					<div class="col-sm-10">
						<input name="address" type="text" class="form-control" id="address" placeholder="Address" required>
					</div>
				</div>
				<div class="form-group">
					<label for="remarks" class="col-sm-2 control-label">Remarks</label>
					<div class="col-sm-10">
						<input name="remarks" type="text" class="form-control" id="remarks" placeholder="Remarks" required>
					</div>
				</div>
				<div class="form-group">
					<label for="requirement" class="col-sm-2 control-label">Requirement</label>
					<div class="col-sm-10">
						<input name="requirement" type="text" class="form-control" id="requirement" placeholder="Requirement" required>
					</div>
				</div>
				<div class="form-group">
					<label for="rf_email" class="col-sm-2 control-label">Reference Person Email </label>
					<div class="col-sm-10">
						<input name="rf_email" type="email" class="form-control" id="rf_email" placeholder="Reference Person Email " required>
					</div>
				</div>
				<div class="form-group">
					<label for="rf_address" class="col-sm-2 control-label">Reference Person Address </label>
					<div class="col-sm-10">
						<input name="rf_address" type="text" class="form-control" id="rf_address" placeholder="Reference Person Address " required>
					</div>
				</div>
				<div class="form-group">
					<label for="organization" class="col-sm-2 control-label">Organization</label>
					<div class="col-sm-10">
						<input name="organization" type="text" class="form-control" id="organization" placeholder="Organization" required>
					</div>
				</div>
			</div>
			<div class='col-md-6'>
				<div class="form-group">
					<label for="mobile" class="col-sm-2 control-label">Mobile*</label>
					<div class="col-sm-10">
						<input name="mobile" type="text" class="form-control" id="mobile" placeholder="Mobile" required>
					</div>
				</div>
				<div class="form-group">
					<label for="category" class="col-sm-2 control-label">Category*</label>
					<div class="col-sm-10">
						<input name="category" type="text" class="form-control" id="category" placeholder="Category" required>
					</div>
				</div>
				<div class="form-group">
					<label for="department" class="col-sm-2 control-label">Department</label>
					<div class="col-sm-10">
						<input name="department" type="text" class="form-control" id="department" placeholder="Department" required>
					</div>
				</div>
				<div class="form-group">
					<label for="address_two" class="col-sm-2 control-label">Address_2</label>
					<div class="col-sm-10">
						<input name="address_two" type="text" class="form-control" id="address_two" placeholder="Other Address" required>
					</div>
				</div>
				<div class="form-group">
					<label for="subject" class="col-sm-2 control-label">Subject</label>
					<div class="col-sm-10">
						<input name="subject" type="text" class="form-control" id="subject" placeholder="Subject" required>
					</div>
				</div>
				<div class="form-group">
					<label for="rf_name" class="col-sm-2 control-label">Reference Person name</label>
					<div class="col-sm-10">
						<input name="rf_name" type="text" class="form-control" id="rf_name" placeholder="Reference Person name" required>
					</div>
				</div>
				<div class="form-group">
					<label for="rf_mobile" class="col-sm-2 control-label">Reference Person Mobile </label>
					<div class="col-sm-10">
						<input name="rf_mobile" type="text" class="form-control" id="rf_mobile" placeholder="Reference Person Mobile " required>
					</div>
				</div>
				<div class="form-group">
					<label for="designation" class="col-sm-2 control-label">Designation</label>
					<div class="col-sm-10">
						<input name="designation" type="text" class="form-control" id="designation" placeholder="Designation" required>
					</div>
				</div>
				<div class="form-group">
					<label for="phone_number" class="col-sm-2 control-label">Phone Number</label>
					<div class="col-sm-10">
						<input name="phone_number" type="text" class="form-control" id="phone_number" placeholder="Phone Number" required>
					</div>
				</div>

				<div class="form-group">
					<label for="file" class="col-sm-2 control-label">Attchment</label>
					<div class="col-sm-10">
						<input type="file" name="attchment" class="form-control" id="file">
					</div>
				</div>
			</div>

		
		</form>

	</div>
	
</div> -->

<script src="https://cdn.ckeditor.com/4.10.0/full/ckeditor.js"></script>
<script>
	CKEDITOR.replace( 'signature',{
		toolbarGroups: [
		{ name: 'document',	   groups: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },			
 		{ name: 'clipboard',   groups: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },	
 		{ name: 'editing', 	   groups: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },		
 		{ name: 'forms', 	   groups: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
 		'/',																
 		{ name: 'basicstyles', groups: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
		{ name: 'paragraph', groups: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
		{ name: 'links', groups: [ 'Link', 'Unlink', 'Anchor' ] },
		{ name: 'insert', groups: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
		'/',
		// { name: 'styles', groups: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
		// { name: 'colors', groups: [ 'TextColor', 'BGColor' ] },
		// { name: 'tools', groups: [ 'Maximize', 'ShowBlocks' ] },
		// { name: 'about', groups: [ 'About' ] }
	]
	} );
</script>
@endsection

