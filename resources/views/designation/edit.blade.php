@extends('hauper_admin') @section('content')
<div class='row'>
	<div class='col-md-12'>
		<form action="{{ env('baseURL') }}/designation/update" method="post" class="form-horizontal" enctype="multipart/form-data">
			<input required name="id" type="hidden" value="{{$designation->id}}">
			<div class="form-group">
				<label for="name" class="col-sm-2 control-label">Designation Name*</label>
				<div class="col-sm-10">
					<input name="name" value="{{$designation->name}}" type="text" class="form-control" id="name" placeholder="Name" required>
				</div>
			</div>
			
			{{ csrf_field() }}
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					@if(Session::has('message'))
			        	<p class="text-info">{{ json_decode(Session::get('message'))->message }}</p>
			        	<!-- <p class="text-info">{{ Session::get('message') }}</p> -->
					@endif
				</div>
			</div>
		</form>
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
@endsection