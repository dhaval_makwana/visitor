@extends('hauper_admin') 
@section('content')
<div class="row">
	<div class="col-xs-2">
		<a href="{{ env('baseURL') }}/user/add" class="btn btn-block btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a><br>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<!-- /.box-header -->
			<div class="box-header">
				@if(Session::has('message'))
					<div class="alert alert-info alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
			</div>
			<div class="box-body">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Full Name</th>
							<th>Mobile No</th>
							<th>Email</th>
							<th>No. Visitor</th>
							<!-- <th>Added By</th> -->
							<th>Status</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($users as $user)
						<tr>
							<td>
								<span>
									@if ($user->profile_pic)
									<img style="height: 45px;width: 45px;" src="{{ env('baseURL') }}/public/images/profile_pic/{{$user->profile_pic}}">
									@else
									<img style="height: 45px;width: 45px;" src="{{ env('baseURL') }}/public/avatar.png">
									@endif
								</span>
								<br>
								{{$user->name}} 
								<!-- <a style="text-decoration:underline;" href="user/view/{{$user->id}}">
									
								</a> -->
							</td>
							<td>{{$user->phone}}</td>
							<td>{{$user->email}}</td>
							<td>{{$user->Company->count()}}</td>

							
                                            
							<td width="10%">
								@if ($user->active == '1')
								<a href="{{ env('baseURL') }}/user/togglestatus/{{$user->id}}" type="button" class="btn btn-block btn-info btn-xs">Active</a>
								@else
								<a href="{{ env('baseURL') }}/user/togglestatus/{{$user->id}}" type="button" class="btn btn-block btn-warning btn-xs">Inactive</a>
								@endif
							</td>
                            <td width="10%">
                            	<a href="{{ env('baseURL') }}/user/edit/{{$user->id}}" type="button" class="btn btn-block btn-info btn-xs">Edit</a>
                                <a onclick="deleteSwal('user/delete/{{$user->id}}')" type="button" class="btn btn-block btn-danger btn-xs">Delete</a>
							</td>
						</tr>
						@endforeach 
					</tbody>
					<!--<tfoot>
						<tr>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Email</th>
							<th>Toggle</th>
							<th>Actions</th>
						</tr>
					</tfoot>-->
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
@endsection
@section('javascript')
<script>
$(function () {
	$('#example1').DataTable()
})
function deleteSwal(url){
	swal({
		title: "Are you sure?",
		text: "You will not be able to recover this!",
		type: "warning",
		showCancelButton: true,
		confirmButtonClass: "btn-danger",
		confirmButtonText: "Yes, delete it!",
		cancelButtonText: "No, cancel!",
		closeOnConfirm: true,
		closeOnCancel: true
	},
	function(isConfirm) {
		if (isConfirm) {
			window.location.href = url;
		}
	});
}
</script>
@stop