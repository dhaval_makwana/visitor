<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\Hash;
use App\Department;
use App\Company;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;


class DepartmentController extends Controller{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	
	public function departmentList(Request $request){
		$department=Department::orderBy('created_at', 'desc')->get();
		return view("department.list", ["departments"=>$department, "page_title"=>"All Departments"]);
	}    

	public function addDepartment(Request $request){
		$message = array();
		$validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
	        if ($validator->fails()) {
	        	$message['message'] = "Name required!";
				$message['type'] = "success";
	        	Session::flash('message', json_encode($message));
	        	return Redirect::to('departments');
	        }else{
	        	$inputdata=$request->input();
				
	        	if (Department::where('name', '=', $request['name'])->exists()) {
	        		$message['message'] = "Department with same name already exists!";
					$message['type'] = "success";
					Session::flash('message', json_encode($message));
		            return Redirect::to('department/add');
		        } else{
		        	$department=Department::create($inputdata);
		        	$message['message'] = "Department added successfully!";
					$message['type'] = "success";
					Session::flash('message', json_encode($message));
					return Redirect::to('departments');
		        }
	        }
	} 

	public function editDepartment(Request $request, $id = ''){
		$department=Department::find($id);
		if(empty($department)){
			return view("department.add", ["page_title"=>"Add Department"]);
		}else{
			 return view("department.edit", ["department"=>$department, "page_title"=>"Edit Department"]);
		}
	}
	public function updateDepartment(Request $request){
		$id = $request->id;
		$message = array();
		$department=Department::find($id);
		if(empty($department)){
			return Redirect::back()->withErrors(['message', 'Department not found!']);
		}else{
			if(Department::where(function($result) use ($request) {
					$result->where("name" , $request['name'])->where('id' , '!=' ,$request->id);
				})->exists()) { 
				$message['message'] = "Department with same name already exists!";
				$message['type'] = "success";
				Session::flash('message', json_encode($message));

				// Session::flash('message', 'Company with same Title already exists!');
	            return Redirect::to('department/edit/'.$id);
			}else{
				$department->name=$request->name;
				$department->save();
				$message['message'] = "Department Updated!";
				$message['type'] = "success";
				Session::flash('message', json_encode($message));
				return Redirect::to('departments');
			}
		}       
	}
	public function deleteDepartment(Request $request, $id){
		$message = array();
		$department=Department::find($id);
		if(empty($department)){
			Session::flash('message', 'Sorry, Department record not found!');
			return Redirect::to('departments');
		}else{
			$allreadydepartment=Department::with('Company')->find($id);
			$alldepartment=$allreadydepartment->Company->count();
			if($alldepartment == 0){
				$department->delete();
				$message['message'] = "Department Deleted!";
				$message['type'] = "success";
			}else{
				$message['message'] = "Can not delete '$department->name' as it is currently being used in an active Company!";
				$message['type'] = "error";
				}
			Session::flash('message', json_encode($message));
			return Redirect::to('departments');
		}
	}
	public function departmentstatus(Request $request, $id){
		$department=Department::find($id);
		$message = array();
		if(empty($department)){
			return Redirect::back()->withErrors(['message', 'Department not found!']);
		}else{
			if($department->status == '1'){
				$allreadydepartment=Department::with('Company')->find($id);
				$alldepartment=$allreadydepartment->Company->count();
				if($alldepartment == 0){
					$department->status='0';
					$message['message'] = "Department Deactivated!";
					$message['type'] = "success";
				}else{
					$message['message'] = "Can not disable '$department->name' as it is currently being used in an active Company!";
					$message['type'] = "error";
				}
			} else{
				$department->status='1';
				$message['message'] = "Department Activated!";
				$message['type'] = "success";
			}
			$department->save();

			Session::flash('message', json_encode($message));
			return Redirect::to('departments');
		}
	}
	
}
