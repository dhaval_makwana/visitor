<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Designation;

class Designation extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'designations';

    protected $guarded = [
        'id'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public function Company(){
        return $this->hasMany(Company::class);
    }
}
