<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
	<section class="sidebar">
		<!-- Sidebar Menu -->
		<ul class="sidebar-menu" data-widget="tree">
			@if (Session::get('userdata')->type == 1)
			<li @if(Request::path() == 'dashbord') class="active" @endif>
				<a href="{{ env('baseURL') }}/admindashbord"><i class="fa fa-dashboard"></i>Dashbord</a>
			</li>
			<li @if(Request::path() == 'users' || Request::is('user/*')) class="active" @endif>
				<a href="{{ env('baseURL') }}/users"><i class="fa fa-user"></i>User List</a>
			</li>

			<li @if(Request::path() == 'allcompanies' ) class="active" @endif>
				<a href="{{ env('baseURL') }}/allcompanies"><i class="fa fa-building"></i>Visitor List</a>
			</li>

			<li @if(Request::path() == 'allbulkmails' ) class="active" @endif>
				<a href="{{ env('baseURL') }}/allbulkmails"><i class="fa fa-envelope"></i>Sent Mail List</a>
			</li>
			<li @if(Request::path() == 'settings' || Request::is('setting/*')) class="active" @endif>
				<a href="{{ env('baseURL') }}/settings"><i class="fa fa-envelope-o"></i>Sent Mail</a>
			</li>
			<li @if(Request::path() == 'roles' || Request::is('role/*')) class="active" @endif>
				<a href="{{ env('baseURL') }}/roles"><i class="fa fa-building"></i>Roles</a>
			</li>
			<!-- <li @if(Request::path() == 'designations' || Request::is('company/*')) class="active" @endif>
				<a href="{{ env('baseURL') }}/designations"><i class="fa fa-building"></i>Designation</a>
			</li>
			<li @if(Request::path() == 'departments' || Request::is('company/*')) class="active" @endif>
				<a href="{{ env('baseURL') }}/departments"><i class="fa fa-building"></i>Department</a>
			</li>
			<li @if(Request::path() == 'states' || Request::is('company/*')) class="active" @endif>
				<a href="{{ env('baseURL') }}/states"><i class="fa fa-building"></i>State</a>
			</li>
			<li @if(Request::path() == 'pincodes' || Request::is('company/*')) class="active" @endif>
				<a href="{{ env('baseURL') }}/pincodes"><i class="fa fa-building"></i>Pincode</a>
			</li> -->
			<!-- <li @if(Request::path() == 'allbulkmails' ) class="active" @endif>
				<a href="{{ env('baseURL') }}/allbulkmails"><i class="fa fa-envelope"></i>Bulk Mail List</a>
			</li> -->

			@else	
			<li @if(Request::path() == 'dashbord') class="active" @endif>
				<a href="{{ env('baseURL') }}/dashbord"><i class="fa fa-dashboard"></i>Dashbord</a>
			</li>
			<li @if(Request::path() == 'users' || Request::is('user/*')) class="active" @endif>
				<a href="{{ env('baseURL') }}/users"><i class="fa fa-user"></i>User List</a>
			</li>
			<li @if(Request::path() == 'companies' || Request::is('company/*')) class="active" @endif>
				<a href="{{ env('baseURL') }}/companies"><i class="fa fa-building"></i>Visitor List</a>
			</li>
			<li @if(Request::path() == 'roles' || Request::is('role/*')) class="active" @endif>
				<a href="{{ env('baseURL') }}/roles"><i class="fa fa-building"></i>Roles</a>
			</li>
			<!-- <li @if(Request::path() == 'designations' || Request::is('company/*')) class="active" @endif>
				<a href="{{ env('baseURL') }}/designations"><i class="fa fa-building"></i>Designation</a>
			</li>
			<li @if(Request::path() == 'departments' || Request::is('company/*')) class="active" @endif>
				<a href="{{ env('baseURL') }}/departments"><i class="fa fa-building"></i>Department</a>
			</li>
			<li @if(Request::path() == 'states' || Request::is('company/*')) class="active" @endif>
				<a href="{{ env('baseURL') }}/states"><i class="fa fa-building"></i>State</a>
			</li>
			<li @if(Request::path() == 'pincodes' || Request::is('company/*')) class="active" @endif>
				<a href="{{ env('baseURL') }}/pincodes"><i class="fa fa-building"></i>Pincode</a>
			</li> -->
			<li @if(Request::path() == 'bulkmails' || Request::is('bulkmail/*')) class="active" @endif>
				<a href="{{ env('baseURL') }}/bulkmails"><i class="fa fa-envelope"></i>Sent Mail List</a>
			</li>
			<li @if(Request::path() == 'settings' || Request::is('setting/*')) class="active" @endif>
				<a href="{{ env('baseURL') }}/settings"><i class="fa fa-envelope-o"></i>Sent Mail</a>
			</li>
			<!-- <li @if(Request::path() == 'drafts' || Request::is('draft/*')) class="active" @endif>
				<a href="{{ env('baseURL') }}/drafts"><i class="fa fa-envelope-o"></i>Draft mail</a>
			</li> -->
			

			@endif
		</ul><!-- /.sidebar-menu -->
	</section>
	<!-- /.sidebar -->
</aside>
