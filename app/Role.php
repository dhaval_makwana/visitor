<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Role;

class Role extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'roles';

    protected $guarded = [
        'id'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public function Masterrole(){
        return $this->belongsTo(Masterrole::class,'master_id');
    }

    // public function Company(){
    //     return $this->hasMany(Company::class);
    // }

    public function Company(){
        return $this->hasMany(Company::class,'id');
    }

}
