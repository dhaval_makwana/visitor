@extends('hauper_admin') 
@section('content')
<div class="row">
	<div class="col-xs-2">
		<a href="{{ env('baseURL') }}/role/add" class="btn btn-block btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a><br>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<!-- /.box-header -->
			<div class="box-header">
				@if(Session::has('message'))
					@if(json_decode(Session::get('message'))->type == 'success')
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<p><i class='fa fa-check' aria-hidden='true'></i> {{ json_decode(Session::get('message'))->message }}</p>
					</div>
					@else
					<div class="alert alert-warning alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<p><i class='fa fa-exclamation-triangle' aria-hidden='true'></i> {{ json_decode(Session::get('message'))->message }}</i></p>
					</div>
					@endif
				@endif
			</div>
			<div class="box-body">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Role Name</th>
							<th>Master</th>
							<th style="text-align:center;">Status</th>
							<th style="text-align:center;">Actions</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($roles as $role)
						<tr>
							<td>{{$role->name}}</td>
							<td>{{$role->Masterrole->name}}</td>
							<td width="10%">
								@if ($role->status == '1')
								<a href="{{ env('baseURL') }}/role/rolestatus/{{$role->id}}" type="button" class="btn btn-block btn-info btn-xs"><i class="fa fa-check-square-o" aria-hidden="true"></i> Active</a>
								@else
								<a href="{{ env('baseURL') }}/role/rolestatus/{{$role->id}}" type="button" class="btn btn-block btn-warning btn-xs"><i class="fa fa-ban" aria-hidden="true"></i> Inactive</a>
								@endif
							</td>
							<td width="10%">
								<a href="{{ env('baseURL') }}/role/edit/{{$role->id}}" type="button" class="btn btn-block btn-info btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                <a onclick="deleteSwal('role/delete/{{$role->id}}')" type="button" class="btn btn-block btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>
							</td>
						</tr>
						@endforeach 
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
@endsection
@section('javascript')
<script>
$(function () {
	$('#example1').DataTable()
})
function deleteSwal(url){
	swal({
		title: "Are you sure?",
		text: "You will not be able to recover this!",
		type: "warning",
		showCancelButton: true,
		confirmButtonClass: "btn-danger",
		confirmButtonText: "Yes, delete it!",
		cancelButtonText: "No, cancel!",
		closeOnConfirm: true,
		closeOnCancel: true
	},
	function(isConfirm) {
		if (isConfirm) {
			window.location.href = url;
		}
	});
}
</script>
@stop