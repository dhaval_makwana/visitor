<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\Hash;
use App\Pincode;
use App\Company;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;


class PincodeController extends Controller{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	
	public function pincodeList(Request $request){
		$pincode=Pincode::orderBy('created_at', 'desc')->get();
		return view("pincode.list", ["pincodes"=>$pincode, "page_title"=>"All Pincodes"]);
	}    

	public function addPincode(Request $request){
		$message = array();
		$validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
	        if ($validator->fails()) {
	        	$message['message'] = "Name required!";
				$message['type'] = "success";
	        	Session::flash('message', json_encode($message));
	        	return Redirect::to('pincodes');
	        }else{
	        	$inputdata=$request->input();
				
	        	if (Pincode::where('name', '=', $request['name'])->exists()) {
	        		$message['message'] = "Pincode with same name already exists!";
					$message['type'] = "success";
					Session::flash('message', json_encode($message));
		            return Redirect::to('pincode/add');
		        } else{
		        	$pincode=Pincode::create($inputdata);
		        	$message['message'] = "Pincode added successfully!";
					$message['type'] = "success";
					Session::flash('message', json_encode($message));
					return Redirect::to('pincodes');
		        }
	        }
	} 

	public function editPincode(Request $request, $id = ''){
		$pincode=Pincode::find($id);
		if(empty($pincode)){
			return view("pincode.add", ["page_title"=>"Add Pincode"]);
		}else{
			 return view("pincode.edit", ["pincode"=>$pincode, "page_title"=>"Edit Pincode"]);
		}
	}
	public function updatePincode(Request $request){
		$id = $request->id;
		$message = array();
		$pincode=Pincode::find($id);
		if(empty($pincode)){
			return Redirect::back()->withErrors(['message', 'Pincode not found!']);
		}else{
			if(Pincode::where(function($result) use ($request) {
					$result->where("name" , $request['name'])->where('id' , '!=' ,$request->id);
				})->exists()) { 
				$message['message'] = "Pincode with same name already exists!";
				$message['type'] = "success";
				Session::flash('message', json_encode($message));

				// Session::flash('message', 'Company with same Title already exists!');
	            return Redirect::to('pincode/edit/'.$id);
			}else{
				$pincode->name=$request->name;
				$pincode->save();
				$message['message'] = "Pincode Updated!";
				$message['type'] = "success";
				Session::flash('message', json_encode($message));
				return Redirect::to('pincodes');
			}
		}       
	}
	public function deletePincode(Request $request, $id){
		$message = array();
		$pincode=Pincode::find($id);
		if(empty($pincode)){
			Session::flash('message', 'Sorry, Pincode record not found!');
			return Redirect::to('pincodes');
		}else{
			$allreadypincode=Pincode::with('Company')->find($id);
			$allpincode=$allreadypincode->Company->count();
			if($allpincode == 0){
				$pincode->delete();
				$message['message'] = "Pincode Deleted!";
				$message['type'] = "success";
			}else{
				$message['message'] = "Can not delete '$pincode->name' as it is currently being used in an active Company!";
				$message['type'] = "error";
				}
			Session::flash('message', json_encode($message));
			return Redirect::to('pincodes');
		}
	}
	public function pincodestatus(Request $request, $id){
		$pincode=Pincode::find($id);
		$message = array();
		if(empty($pincode)){
			return Redirect::back()->withErrors(['message', 'Pincode not found!']);
		}else{
			if($pincode->status == '1'){
				$allreadypincode=Pincode::with('Company')->find($id);
				$allpincode=$allreadypincode->Company->count();
				if($allpincode == 0){
					$pincode->status='0';
					$message['message'] = "Pincode Deactivated!";
					$message['type'] = "success";
				}else{
					$message['message'] = "Can not disable '$pincode->name' as it is currently being used in an active Company!";
					$message['type'] = "error";
				}
			} else{
				$pincode->status='1';
				$message['message'] = "Pincode Activated!";
				$message['type'] = "success";
			}
			$pincode->save();

			Session::flash('message', json_encode($message));
			return Redirect::to('pincodes');
		}
	}
	
}
