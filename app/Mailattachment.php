<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mailattachment extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'attachments';

    protected $guarded = [
        'id'
    ];    
    // public function Member()
    // {
    //     return $this->hasMany(Member::class);
    // }
    public function Resolution(){
        return $this->hasMany(Resolution::class);
    }
}
