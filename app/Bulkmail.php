<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bulkmail extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'bulkmails';

    protected $guarded = [
        'id'
    ];    
    
    // public function Resolution(){
    //     return $this->hasMany(Resolution::class);
    // }
    
}
