@extends('hauper_admin') @section('content')
<div class='row'>
	<div class='col-md-12'>
		<form action="{{ env('baseURL') }}/role/update" method="post" class="form-horizontal" enctype="multipart/form-data">
			<input required name="id" type="hidden" value="{{$role->id}}">
			<div class="form-group">
				<label for="company_id" class="col-sm-2 control-label">Role</label>
				<div class="col-sm-10"> 
					
					 <select name="master_id" id="master_id" class="form-control" required>
						<option value="" >Select Role </option>
						@foreach($masteroles as $masterole)
							<option value="{{$masterole->id}}" @if($masterole->id==$role->master_id) selected='selected' @endif >{{$masterole->name}} </option>
			            @endforeach
					</select>
		        </div>
			</div>
			<div class="form-group">
				<label for="name" class="col-sm-2 control-label">Name*</label>
				<div class="col-sm-10">
					<input name="name" value="{{$role->name}}" type="text" class="form-control" id="name" placeholder="Name" required>
				</div>
			</div>
			
			{{ csrf_field() }}
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					@if(Session::has('message'))
			        	<p class="text-info">{{ json_decode(Session::get('message'))->message }}</p>
			        	<!-- <p class="text-info">{{ Session::get('message') }}</p> -->
					@endif
				</div>
			</div>
		</form>
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
@endsection