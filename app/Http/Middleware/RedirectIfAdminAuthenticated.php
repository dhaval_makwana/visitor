<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAdminAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::check() &&  Auth::user()->type == 1 ) {
            //echo "fsdaf";
             // return redirect('/admindashbord');
        } else{
            return redirect('/login');
        }

        return $next($request);
    }
}
