-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2021 at 11:39 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `visitor`
--

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE `attachments` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `resolution_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `filepath` varchar(255) NOT NULL,
  `created_at` varchar(30) NOT NULL,
  `updated_at` varchar(30) NOT NULL,
  `deleted_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bulkmails`
--

CREATE TABLE `bulkmails` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `resolution_id` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `toname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `reason` text NOT NULL,
  `email_status` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` varchar(30) NOT NULL,
  `updated_at` varchar(30) NOT NULL,
  `deleted_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `pincode_id` int(11) DEFAULT NULL,
  `organization` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `a_email` varchar(255) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `zipcode` varchar(30) NOT NULL,
  `state` varchar(50) NOT NULL,
  `requirement` varchar(255) NOT NULL,
  `place` varchar(50) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `category` varchar(30) NOT NULL,
  `attchment` varchar(255) DEFAULT NULL,
  `reference_person` enum('1','0') NOT NULL DEFAULT '0',
  `r_organization` varchar(100) DEFAULT NULL,
  `r_name` varchar(100) DEFAULT NULL,
  `r_designation` varchar(100) DEFAULT NULL,
  `r_mobile` varchar(30) DEFAULT NULL,
  `r_email` varchar(100) DEFAULT NULL,
  `r_phone` varchar(30) DEFAULT NULL,
  `r_address` varchar(250) DEFAULT NULL,
  `r_zipcode` varchar(30) DEFAULT NULL,
  `r_category` varchar(30) DEFAULT NULL,
  `r_remarks` varchar(255) DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_at` varchar(30) NOT NULL,
  `updated_at` varchar(30) NOT NULL,
  `deleted_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `designation_id`, `department_id`, `state_id`, `pincode_id`, `organization`, `name`, `designation`, `department`, `mobile`, `email`, `phone_number`, `a_email`, `address`, `zipcode`, `state`, `requirement`, `place`, `remarks`, `category`, `attchment`, `reference_person`, `r_organization`, `r_name`, `r_designation`, `r_mobile`, `r_email`, `r_phone`, `r_address`, `r_zipcode`, `r_category`, `r_remarks`, `added_by`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, NULL, NULL, NULL, 'Organization Name', 'test', 'Desigi', 'depart', '9870099713', 'info@evotingforibc.com', '9876543210', 'at@test.com', 'address', '360250', 'gujarat', 'requ', 'places', 'remarks', 'category', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, '1', '2021-03-06 18:10:17', '2021-03-06 18:10:17', NULL),
(2, NULL, NULL, NULL, NULL, 'Organizationrwer wer', 'testwer', 'Desigirwer', 'departrwer', '9870099713', 'dhavalit@sunresolution.in', '9876543210', 'at3@test.com', 'address  2 3', '360250', 'gujarat', 'requ', 'places', 'remarks', 'category', NULL, '0', 'R reference name 3', 'name', 'designation', '987654321', 'tes3t@test.com', '987654321', 'address', '360325', '23', NULL, 2, '1', '2021-03-06 18:13:24', '2021-03-06 18:13:24', NULL),
(3, 1, 1, 1, 1, 'Name of Organization', 'dixit Prjapati', 'Manager', 'ioahdjka hd;auih uil;gluisd flaclflg', '987654132', 'info@evotingforibc.com', 'hd kasgldflasj', NULL, 'qfbajkf bjk;', 'qafjkajk fkk', 'qjkdjb', 'adfnaskkdbn', 'andfklaskldnasn', 'adnfkasndkn', 'ansdasndk', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, '1', '2021-03-06 18:40:26', '2021-06-08 16:39:26', NULL),
(4, 1, 1, 1, 1, 'Organization', 'test wrwe', 'Desigi', 'rwerw', '9870099713', 'linkstarinfosys@gmail.com', '9876543210', NULL, 'address', '360250', 'gujarat', 'requ', 'places', 'remarks', 'category', '1615036903.pdf', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, '1', '2021-03-06 18:51:43', '2021-06-08 16:54:38', NULL),
(5, NULL, NULL, NULL, NULL, 'Organizationrwer wer', 'Notice of AGM Form A', 'Desigirwer', 'depart', '9870099713', 'dhavalit@sunresolution.in', '9876543210', NULL, 'address', '360250', 'gujarat', 'requ', 'places', 'remarks', 'category', NULL, '1', 'R reference name', 'name', 'designation', '987654321', 'test@test.com', '987654321', NULL, NULL, NULL, NULL, 2, '1', '2021-03-06 19:04:32', '2021-03-06 19:04:32', NULL),
(6, 3, 2, 2, 3, 'Organization Namedasdasd', 'DHAVAL SCRUTINIZER NEW SERVER', '', '', '2131321313131313', 'info333@evotingforibc.com', '9876543210', 'at@test.com', 'C G Road', '', '', 'requ', 'places', 'remarks', 'frqr', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, '1', '2021-06-08 16:56:47', '2021-06-08 16:56:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` varchar(30) NOT NULL,
  `updated_at` varchar(30) NOT NULL,
  `deleted_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Hr', 1, '2021-06-08 12:01:02', '2021-06-08 12:01:02', NULL),
(2, 'IP', 1, '2021-06-08 16:55:30', '2021-06-08 16:55:30', NULL),
(3, 'BOB', 1, '2021-06-08 16:55:35', '2021-06-08 16:55:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` varchar(30) NOT NULL,
  `updated_at` varchar(30) NOT NULL,
  `deleted_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Developer', 1, '2021-06-08 11:43:18', '2021-06-08 11:43:32', NULL),
(2, 'CA', 1, '2021-06-08 16:55:42', '2021-06-08 16:55:42', NULL),
(3, 'CS', 1, '2021-06-08 16:55:48', '2021-06-08 16:55:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email_status`
--

CREATE TABLE `email_status` (
  `id` int(11) NOT NULL,
  `resolution_id` int(11) NOT NULL,
  `emaildate` varchar(30) NOT NULL,
  `emailid` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `reason` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `masterroles`
--

CREATE TABLE `masterroles` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `updated_at` varchar(30) NOT NULL,
  `created_at` varchar(30) NOT NULL,
  `deleted_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `masterroles`
--

INSERT INTO `masterroles` (`id`, `name`, `status`, `updated_at`, `created_at`, `deleted_at`) VALUES
(1, 'Department', '1', '2021-03-05 19:07:50', '2021-03-05 19:07:50', NULL),
(2, 'Designation', '1', '2021-03-05 19:07:50', '2021-03-05 19:07:50', NULL),
(3, 'State', '1', '2021-03-05 19:07:50', '2021-03-05 19:07:50', NULL),
(4, 'Pincode', '1', '2021-03-05 19:07:50', '2021-03-05 19:07:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `active` enum('1','0') NOT NULL,
  `added_by` int(10) NOT NULL,
  `updated_at` varchar(30) NOT NULL,
  `created_at` varchar(30) NOT NULL,
  `deleted_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pincodes`
--

CREATE TABLE `pincodes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` varchar(30) NOT NULL,
  `updated_at` varchar(30) NOT NULL,
  `deleted_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pincodes`
--

INSERT INTO `pincodes` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '380015', 1, '2021-06-08 15:01:40', '2021-06-08 15:01:40', NULL),
(2, '123456', 1, '2021-06-08 16:56:05', '2021-06-08 16:56:05', NULL),
(3, '380014', 1, '2021-06-08 16:56:12', '2021-06-08 16:56:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `resolutions`
--

CREATE TABLE `resolutions` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `fromname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `sendmailapproved` enum('Y','N','P') DEFAULT 'N',
  `created_at` varchar(30) NOT NULL,
  `updated_at` varchar(30) NOT NULL,
  `deleted_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `master_id` int(11) NOT NULL,
  `added_by` int(11) DEFAULT NULL,
  `status` enum('1','0') NOT NULL,
  `created_at` varchar(30) NOT NULL,
  `updated_at` varchar(30) NOT NULL,
  `deleted_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `master_id`, `added_by`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'HR', 1, 2, '1', '2021-08-09 18:02:20', '2021-08-09 18:02:20', NULL),
(2, 'CA', 2, 2, '1', '2021-08-09 18:02:40', '2021-08-09 18:02:40', NULL),
(3, 'Gujarat', 3, 2, '1', '2021-08-09 18:17:42', '2021-08-10 10:57:45', NULL),
(4, 'IP', 2, 12, '1', '2021-08-10 10:58:44', '2021-08-10 10:59:13', NULL),
(5, 'Developer', 1, 12, '1', '2021-08-10 10:59:07', '2021-08-10 10:59:07', NULL),
(6, 'Mumbai', 3, 12, '1', '2021-08-10 10:59:24', '2021-08-10 10:59:24', NULL),
(7, '360360', 4, 12, '1', '2021-08-10 10:59:35', '2021-08-10 10:59:35', NULL),
(8, '360370', 4, 2, '1', '2021-08-10 11:00:14', '2021-08-10 11:00:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` varchar(30) NOT NULL,
  `updated_at` varchar(30) NOT NULL,
  `deleted_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Gujarat', 1, '2021-06-08 12:58:01', '2021-06-08 12:58:01', NULL),
(2, 'Mumbai', 1, '2021-06-08 16:55:54', '2021-06-08 16:55:54', NULL),
(3, 'Delhi', 1, '2021-06-08 16:56:00', '2021-06-08 16:56:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(500) NOT NULL,
  `profile_pic` varchar(500) DEFAULT NULL,
  `phone` varchar(500) DEFAULT NULL,
  `address` varchar(212) NOT NULL,
  `active` enum('1','0') NOT NULL,
  `type` enum('1','0','2') NOT NULL COMMENT '0 => user/sc 1 => admin ',
  `added_by` int(10) NOT NULL,
  `remember_token` varchar(212) DEFAULT NULL,
  `auth_token` varchar(212) DEFAULT NULL,
  `reset_token` varchar(212) DEFAULT NULL,
  `valid_reset_token` int(1) DEFAULT 0,
  `updated_at` varchar(30) NOT NULL,
  `created_at` varchar(30) NOT NULL,
  `deleted_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `profile_pic`, `phone`, `address`, `active`, `type`, `added_by`, `remember_token`, `auth_token`, `reset_token`, `valid_reset_token`, `updated_at`, `created_at`, `deleted_at`) VALUES
(1, 'Admin', 'info@evotingforibc.com', '$2y$10$gAI8iMwqpIzQAxPpJV89N..TIUlLNyxrSJKS2qa82EM.EsfbdEbey', '1613408814.jpg', '1231231234', '', '1', '1', 0, '', '$2y$10$fAFw8QokuMWitfBRfeNs5.WNv3ImOlomvZNkUiX89QMSii34kVRFG', '$2y$10$7gfwiHocRhCAXKEDmmQqJ.n8YcDIzXaBNacwLoNcKJmqD.v1cHuw2', 1, '2021-02-15 22:36:54', '2018-04-06 13:32:15', NULL),
(2, 'Dhaval Makwana', 'dhavalit@sunresolution.in', '$2y$10$nm3N9dv0bz0Xs7r8yjL4N.YHFRC3d2K8uUJyQscLsJ0WIwl5dMHqu', '1614751898.jpg', '4878799874', '', '1', '0', 1, NULL, NULL, '$2y$10$rynDlApU.RNe41iRd3W.OWMfrhIRRGDcYbT5OIJFSU1VVNgdj1Sq', 0, '2021-03-03 11:41:38', '2018-10-29 09:02:42', NULL),
(3, 'Dixit', 'dixit@sunresolution.in', '$2y$10$Ty7o6bz2B.OcbvgP24u1lOCCEh13RNTa./AZIyN5iFXZmyr9An9Ey', '1570702211.jpg', '1234567890', '', '1', '0', 1, 'RGkDscVrLN6lf0BqPKt7oT21X79DPDt2jVY4vPmfTvVC2TprSQU8Lgt9DFTJ', NULL, '', 0, '2021-02-16 15:05:51', '2019-08-28 07:42:38', NULL),
(9, 'Dhaval Mistry', 'Forensic.rca@gmail.com', '$2y$10$zbzz4oxttAe40uk1xSV4luBNhm.bXEjPMdnHA24JBYMO82Z7hgQIK', NULL, '9909903614', '', '1', '0', 1, NULL, NULL, NULL, 0, '2020-01-13 12:57:51', '2020-01-13 12:57:51', NULL),
(12, 'Ramchandra Dallaram Choudhary', 'rdc_rca@yahoo.com', '$2y$10$uuR3g.G1mH8mRn8mabPzqOsBFIm0uhXE1s1EVfOjz.RcmHLc8BskK', NULL, '9909903614', '', '1', '0', 1, NULL, NULL, NULL, 0, '2021-08-10 10:58:15', '2020-03-04 15:55:37', NULL),
(14, 'vijay', 'vijay@sunresolution.in', '$2y$10$ah5J0ApcAaSTCGzKV0Ngu.Gjd9KiUl0KFK3auPMq3H4ari0IaB8bC', NULL, '9876543210', '', '1', '0', 2, NULL, NULL, NULL, 0, '2021-03-04 18:00:23', '2021-03-04 18:00:23', NULL),
(15, 'test', 'linkstarinfosys@gmail.com', '$2y$10$xzo4lT.iHl.M6zGVl2FCB.knmrtrWO4e4o/9tzvDSsAvMA3iEAELW', NULL, '9870099713', '', '1', '1', 2, NULL, NULL, NULL, 0, '2021-03-05 18:59:33', '2021-03-05 18:59:33', NULL),
(16, 'test', 'super@admin.com', '$2y$10$XnAbg2XKfE2Wnuz.7jlVjOGZ66Y2LC7a2BsS6pnlGJVM.1TRwSEyS', NULL, '9870099713', '', '1', '1', 2, NULL, NULL, NULL, 0, '2021-03-05 19:03:35', '2021-03-05 19:03:35', NULL),
(17, 'AAJ KAL 23 11 2019', 'super123@admin.com', '$2y$10$ml09ptNNXNL.eNZpjIpFNe8TFku2pDLduo1B4BzGh0Pm2LQnN1zyW', NULL, '9870099713', '', '1', '0', 2, NULL, NULL, NULL, 0, '2021-03-05 19:07:50', '2021-03-05 19:07:50', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bulkmails`
--
ALTER TABLE `bulkmails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_status`
--
ALTER TABLE `email_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masterroles`
--
ALTER TABLE `masterroles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pincodes`
--
ALTER TABLE `pincodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resolutions`
--
ALTER TABLE `resolutions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attachments`
--
ALTER TABLE `attachments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bulkmails`
--
ALTER TABLE `bulkmails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `email_status`
--
ALTER TABLE `email_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `masterroles`
--
ALTER TABLE `masterroles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pincodes`
--
ALTER TABLE `pincodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `resolutions`
--
ALTER TABLE `resolutions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
