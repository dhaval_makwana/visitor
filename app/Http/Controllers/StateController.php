<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\Hash;
use App\State;
use App\Company;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;


class StateController extends Controller{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	
	public function stateList(Request $request){
		$state=State::orderBy('created_at', 'desc')->get();
		return view("state.list", ["states"=>$state, "page_title"=>"All States"]);
	}    

	public function addState(Request $request){
		$message = array();
		$validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
	        if ($validator->fails()) {
	        	$message['message'] = "Name required!";
				$message['type'] = "success";
	        	Session::flash('message', json_encode($message));
	        	return Redirect::to('states');
	        }else{
	        	$inputdata=$request->input();
				
	        	if (State::where('name', '=', $request['name'])->exists()) {
	        		$message['message'] = "State with same name already exists!";
					$message['type'] = "success";
					Session::flash('message', json_encode($message));
		            return Redirect::to('state/add');
		        } else{
		        	$state=State::create($inputdata);
		        	$message['message'] = "State added successfully!";
					$message['type'] = "success";
					Session::flash('message', json_encode($message));
					return Redirect::to('states');
		        }
	        }
	} 

	public function editState(Request $request, $id = ''){
		$state=State::find($id);
		if(empty($state)){
			return view("state.add", ["page_title"=>"Add State"]);
		}else{
			 return view("state.edit", ["state"=>$state, "page_title"=>"Edit State"]);
		}
	}
	public function updateState(Request $request){
		$id = $request->id;
		$message = array();
		$state=State::find($id);
		if(empty($state)){
			return Redirect::back()->withErrors(['message', 'State not found!']);
		}else{
			if(State::where(function($result) use ($request) {
					$result->where("name" , $request['name'])->where('id' , '!=' ,$request->id);
				})->exists()) { 
				$message['message'] = "State with same name already exists!";
				$message['type'] = "success";
				Session::flash('message', json_encode($message));

				// Session::flash('message', 'Company with same Title already exists!');
	            return Redirect::to('state/edit/'.$id);
			}else{
				$state->name=$request->name;
				$state->save();
				$message['message'] = "State Updated!";
				$message['type'] = "success";
				Session::flash('message', json_encode($message));
				return Redirect::to('states');
			}
		}       
	}
	public function deleteState(Request $request, $id){
		$message = array();
		$state=State::find($id);
		if(empty($state)){
			Session::flash('message', 'Sorry, State record not found!');
			return Redirect::to('states');
		}else{
			$allreadystate=State::with('Company')->find($id);
			$allstate=$allreadystate->Company->count();
			if($allstate == 0){
				$state->delete();
				$message['message'] = "State Deleted!";
				$message['type'] = "success";
			}else{
				$message['message'] = "Can not delete '$state->name' as it is currently being used in an active Company!";
				$message['type'] = "error";
				}
			Session::flash('message', json_encode($message));
			return Redirect::to('states');
		}
	}
	public function statestatus(Request $request, $id){
		$state=State::find($id);
		$message = array();
		if(empty($state)){
			return Redirect::back()->withErrors(['message', 'State not found!']);
		}else{
			if($state->status == '1'){
				$allreadystate=State::with('Company')->find($id);
				$allstate=$allreadystate->Company->count();
				if($allstate == 0){
					$state->status='0';
					$message['message'] = "State Deactivated!";
					$message['type'] = "success";
				}else{
					$message['message'] = "Can not disable '$state->name' as it is currently being used in an active Company!";
					$message['type'] = "error";
				}
			} else{
				$state->status='1';
				$message['message'] = "State Activated!";
				$message['type'] = "success";
			}
			$state->save();

			Session::flash('message', json_encode($message));
			return Redirect::to('states');
		}
	}
	
}
