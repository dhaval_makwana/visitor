@extends('hauper_admin') 
@section('content')
<div class="row">
</div>
<div class='row'>
	<div class='col-md-12'>
 				@if (Session::has('errors'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <p>{{ Session::get('errors') }}</p>
                    </div>
                @endif
                @if (Session::has('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <p>{{ Session::get('success') }}</p>
                    </div>
                @endif
                <form action="{{ env('baseURL') }}/company/downloadExcel" method="post" class="form-horizontal" enctype="multipart/form-data">
				
				
			{{ csrf_field() }}
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary">Export Visitor List</button>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					@if(Session::has('message'))
			        	<p class="text-info">{{ Session::get('message') }}</p>
					@endif
				</div>
			</div>
		</form>
<!-- 
            <form  action="{{ url('/member/downloadExcel') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
            

 				<input name="added_by" type="hidden" value="{{Session::get('userdata')->id}}">
				{{ csrf_field() }}
           		<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary">Export Member List</button>
				</div>
			</div>

            </form> -->
 
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
@endsection
@section('javascript')
<script>
$(function () {
	$('#example1').DataTable()
})


</script>
@stop