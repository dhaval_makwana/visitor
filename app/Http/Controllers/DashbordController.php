<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Member;
use App\Company;
use App\Resolution;
use App\Bulkmail;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Carbon\Carbon;


class DashbordController extends Controller{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	
	public function dashbord(Request $request){

        //echo $date =Carbon::now()->subDays(15)->toDateTimeString();
        // $user_seven_day=User::where('created_at','>', Carbon::now()->subDays(7)->toDateTimeString())->get();
        // $user_thirty_day=User::where('created_at','>', Carbon::now()->subDays(30)->toDateTimeString())->get();
        // $all_aciveuser=User::where('active','=','1')->get();
        // $insight=Insight::where('created_at','>', Carbon::now()->subDays(15)->toDateTimeString())->get();
        // $unique_user_id=$insight->unique('user_id');
        // $active_user= $unique_user_id->count();
        // $reach_goal=Aspirationstatus::where('completed','=','1')->get();
        // $uniqueuser_reachgoal=$reach_goal->unique('user_id');
        $login_useid = session('userdata')->id;

        $member=Member::where('added_by',$login_useid)->orderBy('created_at', 'desc')->get();
        $activemember=Member::where([
            ['added_by',$login_useid],
            ['active',1]
        ])->orderBy('created_at', 'desc')->get();

        $deactivemember=Member::where([
            ['added_by',$login_useid],
            ['active', "!=" ,1]
        ])->orderBy('created_at', 'desc')->get();

        $company=Company::where('added_by',$login_useid)->orderBy('created_at', 'desc')->get();
        $resolition = Resolution::where('added_by',$login_useid)->orderBy('created_at', 'desc')->get();

        $todaymail=Bulkmail::where([
            ['added_by',$login_useid],
            ['created_at','>', Carbon::today()->toDateString()]
        ])->get();

        $totalmonthmail = Bulkmail::where('added_by', $login_useid)->whereMonth('created_at', Carbon::now()->month)->get();

        // print_r($totalmonthmail);die;
        return view("dashbord", ["page_title"=>"Dashbord","companys"=>$company,'members'=>$member,'resolition'=>$resolition, 'todaymail'=> $todaymail, 'totalmonthmail'=>$totalmonthmail, "activemember"=>$activemember, "deactivemember"=>$deactivemember]);
    }

    public function admindashbordDashbord(Request $request){
        $user=User::where('type',"0")->orderBy('created_at', 'desc')->get();

        $company=Company::orderBy('created_at', 'desc')->get();
        $member=Member::orderBy('created_at', 'desc')->get();
        $resolition = Resolution::orderBy('created_at', 'desc')->get();

        $todaymail=Bulkmail::where([
            ['created_at','>', Carbon::today()->toDateString()]
        ])->get();
      
        $totalmonthmail = Bulkmail::whereMonth('created_at', Carbon::now()->month)->get();

        return view("admindashbord", ["page_title"=>"Dashbord","users"=>$user,"companys"=>$company, 'members'=>$member,'resolition'=>$resolition, 'todaymail'=> $todaymail, 'totalmonthmail'=>$totalmonthmail]);
    }

    public function allCompanies(Request $request){
        $company=Company::with('Member','User')->orderBy('created_at', 'desc')->get();
        // echo "<pre>";
        // print_r($company);die;
        return view("admin.listcompany", ["companys"=>$company, "page_title"=>"List of Companies"]);
    }   

    public function allBulkmails(Request $request){
        $balkmaillist=Resolution::with('Company')->orderBy('created_at', 'desc')->get();
        return view("admin.listbulkmail", ["balkmaillists"=>$balkmaillist, "page_title"=>"Bulk Mail List"]);
    }   
}
