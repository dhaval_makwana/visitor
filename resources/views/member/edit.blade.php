@extends('hauper_admin') @section('content')
<div class='row'>
	<div class='col-md-12'>
		<form action="{{ env('baseURL') }}/member/update" method="post" class="form-horizontal" enctype="multipart/form-data">
			<input required name="id" type="hidden" value="{{$member->id}}">
			<div class="form-group">
				<label for="inputFName" class="col-sm-2 control-label">Name</label>
				<div class="col-sm-10">
					<input name="name" type="text" class="form-control" id="inputFName" placeholder="Member/Contact Name" value="{{$member->name}}" required>
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail" class="col-sm-2 control-label">Email</label>
				<div class="col-sm-10">
					<input type="email" name="email" class="form-control" id="inputEmail" placeholder="Member/Contact Email" value="{{$member->email}}" >
				</div>
			</div>
			
			<div class='form-group'>
				<label for="company_id" class="col-sm-2 control-label">Company/Entity</label>
				<div class="col-sm-10"> 
					<select required name="company_id" id="company_id" class="form-control" required>
						<option value="" >Select Company/Entity</option>
						@foreach($companys as $company)
							<option value="{{$company->id}}" @if($company->id==$member->company_id) selected='selected' @endif >{{$company->name}} </option>
			            @endforeach
					</select>
	        	</div>
		    </div>
			
			{{ csrf_field() }}
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary">Save</button>
                    <button onclick="window.history.go(-1); return false;" class="btn btn-danger" >Back</button>

				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					@if(Session::has('message'))
			        	<p class="text-info">{{ Session::get('message') }}</p>
					@endif
				</div>
			</div>
		</form>
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
@endsection