<!DOCTYPE html>
    <!--
    This is a starter template page. Use this page to start your new project from
    scratch. This page gets rid of all links and provides the needed markup only.
    -->
    <html>
    <head>
        <meta charset="UTF-8">
        <title>{{ $page_title or "IBC" }}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        
       

        <link rel="stylesheet" href="{{env('baseURL')}}/public/bower_components/front/css/jquery-ui.css">
        <link href="{{env('baseURL')}}/public/bower_components/front/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
      
        <link href="{{env('baseURL')}}/public/bower_components/front/css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" media="all"/>
         <link href="{{env('baseURL')}}/public/bower_components/front/css/sweetalert.min.css" rel="stylesheet" type="text/css" />
        <link href="{{env('baseURL')}}/public/css/app-mdl.css" rel="stylesheet" type="text/css" />

        <link href="{{env('baseURL')}}/public/css/law.css" rel="stylesheet" type="text/css" />       
    </head>
    <body>
    <div class="container">

        <!-- Header -->
        @include('front_header')

        <!-- Menu -->
        @include('front_menu')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper1">
            <!-- Content Header (Page header) -->
        <!--     <section class="content-header">
                <h1>
                    {{ $page_title or "Page Title" }}
                    <small>{{ $page_description or null }}</small>
                </h1>
            </section> -->

            <!-- Main content -->
            <section class="content">
                <!-- Your Page Content Here -->
                @yield('content')
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <!-- Footer -->
        @include('front_footer')

    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->
    <script src="{{env('baseURL')}}/public/bower_components/front/js/jquery-1.12.4.js"></script>
    <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->

    <script src="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/bootstrap/dist/js/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript" src="{{env('baseURL')}}/public/bower_components/front/js/sweetalert.min.js"></script>

   
    @yield('javascript')
  
    </body>
</html>