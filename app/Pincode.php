<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Pincode;

class Pincode extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'pincodes';

    protected $guarded = [
        'id'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public function Company(){
        return $this->hasMany(Company::class);
    }
}
