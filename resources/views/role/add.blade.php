@extends('hauper_admin') @section('content')
<div class='row'>
	<div class='col-md-12'>
		<form action="{{ env('baseURL') }}/role/add" method="post" class="form-horizontal" enctype="multipart/form-data">
			<div class="form-group">
				<label for="company_id" class="col-sm-2 control-label">Role</label>
				<div class="col-sm-10"> 
					<select required name="master_id" id="master_id" class="form-control" >
						<option value="" >Select Role </option>
						@foreach($masteroles as $masterole)
							<option value="{{$masterole->id}}" >{{$masterole->name}} </option>
			            @endforeach
					</select>
		        </div>
			</div>
			<div class="form-group">
				<label for="name" class="col-sm-2 control-label">Name*</label>
				<div class="col-sm-10">
					<input name="name" type="text" class="form-control" id="name" placeholder="Name" required>
				</div>
			</div>
			<input name="added_by" type="hidden" value="{{Session::get('userdata')->id}}">
			{{ csrf_field() }}
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
                    <button onclick="window.history.go(-1); return false;" class="btn btn-danger" ><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					@if(Session::has('message'))
			        	<!-- <p class="text-info">{{ Session::get('message') }}</p> -->
			        	<p class="text-info">{{ json_decode(Session::get('message'))->message }}</p>
			        	
					@endif
				</div>
			</div>
		</form>
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
@endsection