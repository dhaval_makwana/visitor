<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Masterrole;
use App\Role;

class Masterrole extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'masterroles';

    protected $guarded = [
        'id'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // public function Role(){
    //     return $this->hasMany(Role::class);
    // }
    public function Role(){
        return $this->hasMany(Role::class);
    }

}
