<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\Hash;
use App\Setting;
use App\Company;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;


class SettingController extends Controller{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	
	public function sendMail(Request $request){
		$login_useid = session('userdata')->id;
		$company=Company::where('status', '1')->where('added_by',$login_useid)->orderBy('created_at', 'desc')->get();
		return view("setting.sentmail", ["companys"=>$company, "page_title"=>"Sent mail"]);
	}    

	
	
}
