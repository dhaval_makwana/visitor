@extends('hauper_admin') 
@section('content')
<div class="row">
	<div class="col-xs-2">
		<!-- <a href="{{ env('baseURL') }}/member/add" class="btn btn-block btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a> --><br>

	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<!-- /.box-header -->
			<div class="box-header">
				@if(Session::has('message'))
					<div class="alert alert-info alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
			</div>
			<div class="box-body">
				<table id="example1" class="table table-bordered table-striped display responsive ">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Created Date</th>
							<th style="width: 20%;">Reson</th>
							<th>Edit</th>
							<th>Resend</th>
							<!-- <th>Email Status</th> -->
						</tr>
					</thead>
					<tbody>
						@foreach ($memberlists as $memberlist)
						<tr>
							<td>{{$memberlist->toname}}	</td>
							<td>{{$memberlist->email}}</td>
							<td>{{$memberlist->created_at}}</td>
							@if ($memberlist->email_status == 'E')
								<td>{{$memberlist->reason}}</td>
							@else
								<td>Delivery</td>
							@endif
							<td><button type="button" onclick="GetUserDetails('{{$memberlist->id}}')" class="btn btn-block btn-info btn-xs">Edit</button></td>
							<td><button type="button" onclick="ResendMail('{{$memberlist->id}}')" class="btn btn-block btn-info btn-xs"> Resend</button></td>
							
							<!-- <td>{{$memberlist->email_status}}</td> -->
						</tr>
						@endforeach 
					</tbody>
					<!--<tfoot>
						<tr>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Email</th>
							<th>Toggle</th>
							<th>Actions</th>
						</tr>
					</tfoot>-->
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
@endsection
@section('javascript')
<script>
$(document).ready(function() {
    $('#example1').DataTable( {
    	"scrollX": true,
        dom: 'B<"col-sm-5 margin-top-15"l><"col-sm-7 margin-top-15"f>tp',
        buttons: [
             'csv', 'excel', 'pdf'
        ]
    } );
} );

function deleteSwal(url){
	swal({
		title: "Are you sure?",
		text: "You will not be able to recover this!",
		type: "warning",
		showCancelButton: true,
		confirmButtonClass: "btn-danger",
		confirmButtonText: "Yes, delete it!",
		cancelButtonText: "No, cancel!",
		closeOnConfirm: true,
		closeOnCancel: true
	},
	function(isConfirm) {
		if (isConfirm) {
			window.location.href = url;
		}
	});
}

function GetUserDetails(MEMBERID) {
	// alert(MEMBERID);
    console.log(MEMBERID);
    $.ajax({
        type: 'get',
        url: "{{env('baseURL')}}"+ "/get-member-detail-by-id",
        data: {
            MEMBERID: MEMBERID,
        },
        success: function (data) {
        	// console.log(data);
            $("#Name").val(data.data.toname);
            $("#Email").val(data.data.email);
            $("#company_id").val(data.data.company_id);
            $("#added_by").val(data.data.added_by);
            $("#memberid").val(data.data.id);
            
        },
    });
    $("#update_user_modal").modal("show");
}

function ResendMail(MEMBERID){

    // window.location.href= BASEURL+'resend-member-mail/' + MEMBERID;

    
    $.ajax({
        type: 'GET',
        url: "{{env('baseURL')}}" + "/resend-member-mail/" + MEMBERID,
        dataType: 'json',
        success: function(data) {
            if(data.statusCode == 1){
                swal({
	                title: data.title,
	                text: data.statusMsg,
	                closeOnConfirm: true
	            });
                // location.reload();
            }else{
            	 swal({
	                title: data.title,
	                text: data.statusMsg,
	                closeOnConfirm: true
	            });
                // alert(data.statusMsg);
            }
        },
        error: function(jqXHR, exception) {
            console.log(jqXHR);
            console.log("Call showAjaxErrorMsg");
            showAjaxErrorMsg(jqXHR, exception);
        }
	});
}



$(document).ready(function () {
	$( "#UpdateMemberDetails" ).submit(function( event ) {
	        event.preventDefault();

	        var formData = new FormData(this);
	        // alert(formData);
	        $.ajax({
	            type: 'post',
	            url: "{{env('baseURL')}}" + "/update-member-detail-by-id",
	            data: formData,
	            cache:false,
                contentType: false,
                processData: false,
	            dataType: 'json',
	            success: function(data) {
	                if(data.statusCode == 1){
	                    // alert(data.statusMsg);
	                    swal({
			                title: data.title,
			                text: data.statusMsg,
			                closeOnConfirm: true
			            });
	                    location.reload();
	                }else{
	                	 swal({
			                title: data.title,
			                text: data.statusMsg,
			                closeOnConfirm: true
			            });
	                    // alert(data.statusMsg);
	                }
	            },
	            error: function(jqXHR, exception) {
	                console.log(jqXHR);
	                console.log("Call showAjaxErrorMsg");
	                showAjaxErrorMsg(jqXHR, exception);
	            }
        	});
	});   
});   
</script>

@stop
<style type="text/css">
	.dt-buttons {
    margin-bottom: 10px;
}
</style>


<!-- Modal - Update User details -->
<div class="modal fade" id="update_user_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Update Member</h4>
            </div>
            <div class="modal-body">
                <form id="UpdateMemberDetails">
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="company_id" id="company_id" value="">
                <input type="hidden" name="added_by" id="added_by" value="">
                <input type="hidden" name="memberid" id="memberid" value="">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="Name" placeholder="Name" value="" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="Email">Email</label>
                    <input type="email" name="email" id="Email" placeholder="Email" value="" class="form-control"/>
                </div>
                <div class="form-group">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" id="submitModal">Save Changes</button>
            </div>
            </form>
            </div>

        </div>
    </div>
</div>
<!-- // Modal -->