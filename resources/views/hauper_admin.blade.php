<!DOCTYPE html>
    <!--
    This is a starter template page. Use this page to start your new project from
    scratch. This page gets rid of all links and provides the needed markup only.
    -->
    <html>
    <head>
        <meta charset="UTF-8">
        <title>{{ $page_title or "Admin Dashboard" }}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/Ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- DataTables -->
        <link rel="stylesheet" href="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <!-- Theme style -->
        <link href="{{env('baseURL')}}/public/bower_components/admin-lte/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
              page. However, you can choose any other skin. Make sure you
              apply the skin class to the body tag so the changes take effect.
        -->
        <link href="{{env('baseURL')}}/public/bower_components/admin-lte/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link rel="stylesheet" href="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/morris.js/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/jvectormap/jquery-jvectormap.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/bootstrap-daterangepicker/daterangepicker.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/timepicker/bootstrap-timepicker.min.css">
        <link rel="stylesheet" href="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/bootstrap-toggle/css/bootstrap-toggle.min.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- fullCalendar -->
        <link rel="stylesheet" href="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/fullcalendar/dist/fullcalendar.min.css">
        <link rel="stylesheet" href="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/fullcalendar/dist/fullcalendar.print.min.css" media="print">
        <!-- Theme style -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
    <div class="wrapper">

        <!-- Header -->
        @include('header')

        <!-- Sidebar -->
        @include('sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    {{ $page_title or "Page Title" }}
                    <small>{{ $page_description or null }}</small>
                </h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- Your Page Content Here -->
                @yield('content')
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <!-- Footer -->
        @include('footer')

    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.3 -->
    <script src="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/jquery-ui/jquery-ui.min.js"></script>
   
   
    <script src="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- bootstrap toggle checkbox -->
    <script src="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js"></script>
    <!-- fullCalendar -->
    <script src="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/moment/moment.js"></script>
    <script src="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{env('baseURL')}}/public/bower_components/admin-lte/dist/js/adminlte.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="{{env('baseURL')}}/public/bower_components/admin-lte/bower_components/bootstrap/dist/js/jquery-ui.js" type="text/javascript"></script>
    @yield('javascript')
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
          Both of these plugins are recommended to enhance the
          user experience -->
    </body>
</html>