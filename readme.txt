This git includes laravel admin with Admin Login/Logout, Admin Profile, User's Module with AdminLTE integrated into that. Steps to install:

1. Clone this repo into your localhost
2. Run composer install to install all dependencies
3. Rename example.env to .env
4. Create database and update creds in .env file
5. Import database.sql file
6. Run https://locaohost/laravel_admin
7. Change branding in various blade template files.

Issues:
1. In #2, if you get error like http://prntscr.com/kitgk8 then create folder "factories" in database and run command again.