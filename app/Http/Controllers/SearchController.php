<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use App\Company;
use App\Department;
use App\Designation;
use App\Pincode;
use App\State;



  
class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        // $department=Department::orderBy('name', 'asc')->get();
        // $designation=Designation::orderBy('name', 'asc')->get();
        // $state=State::orderBy('name', 'asc')->get();
        // $pincode=Pincode::orderBy('name', 'asc')->get();

        return view("search", ["page_title"=>"Add Visitor"]);
        // return view('search');
    }
  
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function autocomplete(Request $request)
    {
        $term = $request->get('term');
        if ( ! empty($term)) {
            // search user by name or description
            $laws = Company::where('status', 1)->where('name', 'LIKE', '%' . $term .'%')
                            ->orWhere('email', 'LIKE', '%' . $term .'%')
                            ->orWhere('organization', 'LIKE', '%' . $term .'%')
                            ->orWhere('designation', 'LIKE', '%' . $term .'%')
                            ->orWhere('department', 'LIKE', '%' . $term .'%')
                            ->orWhere('phone_number', 'LIKE', '%' . $term .'%')
                            ->orWhere('a_email', 'LIKE', '%' . $term .'%')
                            ->orWhere('address', 'LIKE', '%' . $term .'%')
                            ->orWhere('zipcode', 'LIKE', '%' . $term .'%')
                            ->orWhere('state', 'LIKE', '%' . $term .'%')
                            ->orWhere('requirement', 'LIKE', '%' . $term .'%')
                            ->orWhere('place', 'LIKE', '%' . $term .'%')
                            ->orWhere('remarks', 'LIKE', '%' . $term .'%')
                            ->orWhere('category', 'LIKE', '%' . $term .'%')
                            
                            ->get();
            foreach ($laws as $law) {
                $law->label   = $law->name;
            }
            return $laws;
        }
        // print_r($users);die;
        return Response::json($laws);
    }

    public function viewVisitor(Request $request, $id){
        
        $law=Company::where("id",$id)->first();
        
        if($law){
            return view("search.view", ["law"=>$law, "page_title"=>"Visitor Details"]);    
        }else{
            return view("search.view", ["page_title"=>"Visitor Details"]); 

        }
        
    }

    public function searchByCourt(Request $request)
    {
        $court_id = $request->court_id;
        $laws=Law::where('court_id',$court_id)->get();
        return response()->json($laws);
    }  

    public function searchByMatter(Request $request){
        $matter_id = $request->matter_id;
        $laws=Law::where('matter_id',$matter_id)->get();
        return response()->json($laws);
    }
    public function searchByCodeReference(Request $request){
        $codereference_id = $request->codereference_id;
        $laws=Law::where('codereference_id',$codereference_id)->get();
        return response()->json($laws);
    }
    // public function searchBySectionWise(Request $request){
    //     $sectionwise_id = $request->sectionwise_id;
    //     $laws=Law::where('sectionwise_id',$sectionwise_id)->get();
    //     return response()->json($laws);
    // }

    public function searchByregulation(Request $request){
        $regulation_id = $request->regulation_id;
        $laws=Law::where('regulation_id',$regulation_id)->get();
        return response()->json($laws);
    }

    public function searchByChapter(Request $request)
    {
        $chapter_id = $request->chapter_id;
        $laws=Law::where('chapter_id',$chapter_id)->get();
        return response()->json($laws);
    }  

    public function searchByPetitionNo(Request $request){
        $petitionno_id = $request->petitionno_id;
        $laws=Law::where('petitionno_id',$petitionno_id)->get();
        return response()->json($laws);
    }
    public function searchBysearchByApplicant(Request $request){
        $applicant_id = $request->applicant_id;
        $laws=Law::where('applicant_id',$applicant_id)->get();
        return response()->json($laws);
    }
    public function searchBysearchByJudgment(Request $request){
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        $start_Date = date("Y-m-d", strtotime($from_date));
        $end_Date = date("Y-m-d", strtotime($to_date));

        $laws=Law::whereBetween('judgment_date', [$start_Date, $end_Date])->get();
        // $laws=Law::where('judgment_date',$newDate)->get();
        return response()->json($laws);
    }
    public function searchByAuthority(Request $request){
        $authority_id = $request->authority_id;
        $laws=Law::where('authority_id',$authority_id)->get();
        return response()->json($laws);
    }

    
    


}