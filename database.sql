-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 15, 2018 at 10:12 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.1.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel-admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `birthdate` varchar(30) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `password` varchar(500) NOT NULL,
  `profile_pic` varchar(500) DEFAULT NULL,
  `phone` varchar(500) DEFAULT NULL,
  `address` varchar(212) NOT NULL,
  `city` varchar(212) NOT NULL,
  `state` varchar(212) NOT NULL,
  `zip` varchar(212) NOT NULL,
  `website` varchar(212) NOT NULL,
  `active` enum('1','0') NOT NULL,
  `type` enum('1','0') NOT NULL,
  `added_by` int(10) NOT NULL,
  `added_via` varchar(200) NOT NULL,
  `remember_token` varchar(212) DEFAULT NULL,
  `auth_token` varchar(212) DEFAULT NULL,
  `reset_token` varchar(212) DEFAULT NULL,
  `valid_reset_token` int(1) DEFAULT '0',
  `updated_at` varchar(30) NOT NULL,
  `created_at` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `birthdate`, `gender`, `password`, `profile_pic`, `phone`, `address`, `city`, `state`, `zip`, `website`, `active`, `type`, `added_by`, `added_via`, `remember_token`, `auth_token`, `reset_token`, `valid_reset_token`, `updated_at`, `created_at`) VALUES
(1, 'Hauper', 'Admin', 'admin@hauper.com', '', '', '$2y$10$DvdIj4TkkH4HgYoksyWP.e6ogI84pyY8EFOY4FtspEAZZbnHxSDJu', '', NULL, '', '', '', '', '', '1', '1', 0, '', '', '$2y$10$fAFw8QokuMWitfBRfeNs5.WNv3ImOlomvZNkUiX89QMSii34kVRFG', '', 1, '2018-08-15 08:10:18', '2018-04-06 13:32:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
