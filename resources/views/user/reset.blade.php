<meta name="viewport" content="width=device-width, user-scalable=no" />

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<meta name="_token" content="{{csrf_token()}}" />
<div class="container">
        <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="text-center">
                  <h3><i class="fa fa-lock fa-4x"></i></h3>
                  <h2 class="text-center">Forgot Password?</h2>
                  <p>Please provide your new password</p>
                  <div class="panel-body">
    
                     <form method="POST" id="resetform">
                        {!! csrf_field() !!}   
                        <input type="hidden" name="token" id="token" value="{{ $token}}">
                       
                      <div class="form-group">
                         <input type="password" class="form-control" placeholder="New Password" name="password" id="password">
                      </div>
                      <div class="form-group">
                         <input type="password" class="form-control" placeholder="Confirm Password" name="ConfirmPassword" id="ConfirmPassword">
                      </div>
                      <div class="form-group">
                        <button type="submit" id="ajaxSubmit" class="btn btn-primary"> 
                         Reset Password
                        </button>
                      </div>
                      
                    </form>
                    <div id="msg"> </div>
                    <p class="alert" style="display:none"> Password changed successfully! </p>
                    <p class="passmsg" style="display:none"> Password mismatch! </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
    </div>
   
</div>
 <script src="http://code.jquery.com/jquery-3.3.1.min.js"
               integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
               crossorigin="anonymous">
      </script>
      <script>
         jQuery(document).ready(function(){
            jQuery('#ajaxSubmit').click(function(e){
                  e.preventDefault();
                  var password = jQuery("#password").val();
                  var ConfirmPassword = jQuery("#ConfirmPassword").val();
                  if (password == ConfirmPassword) {
                      
                      jQuery('.passmsg').hide();

                      jQuery.ajax({
                        url: "{{ url('/api/resetpassword') }}",
                        method: 'post',
                        data: {
                         reset_token : jQuery('#token').val(),
                         password: jQuery('#password').val()
                      },
                      success: function(result){
                         console.log(result);
                         result=JSON.parse(result);
                         //alert(result);
                         if(result.status == "success"){
                            jQuery('#msg').html(result.message); 
                         }else{
                            jQuery('#msg').html(result.message);
                         }
                         
                      }});
                  }else{
                    jQuery('.passmsg').show();
                    return false;
                  }
               
               });
            });
      </script>