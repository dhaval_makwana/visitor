<?php
namespace App\Http\Controllers;
use Session;
use Illuminate\Support\Facades\Hash;
use ChannelLog;
use Exception;
use Log;
use App\Resolution;
use App\Bulkmail;
use App\Company;
use App\Member;
use App\Mailattachment;
use App\Masterrole;
use App\Role;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Mail;
class BulkMailController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function bulkmailList(Request $request) {
        $login_useid = session('userdata')->id;
        $balkmaillist = Resolution::where('added_by', $login_useid)->orderBy('created_at', 'desc')->get();
        return view("bulkmail.list", ["balkmaillists" => $balkmaillist, "page_title" => "Mail List"]);
    }
    public function sendMail(Request $request) {
        $login_useid = session('userdata')->id;
        // $company=Company::where('status', '1')->where('added_by',$login_useid)->orderBy('created_at', 'desc')->get();
        $masterrole = Masterrole::where('status', '1')->get();
        return view("bulkmail.sentmail", ["masterroles" => $masterrole, "page_title" => "Mail "]);
    }
    public function getroledetail(Request $request) {
        $masterroleid = $request->master_id;
        $role[] = Role::where('master_id', $masterroleid)->get(['id', 'name']);
        return response()->json($role);
    }
    public function getMemberDetailById(Request $request) {
        log::info("Coming into getMemberDetailById");
        try {
            log::info("Coming into try and get member details");
            $memberDetail = Bulkmail::find($request->MEMBERID);
            // print_r($memberDetail);die;
            $data = array('statusCode' => 1, "statusMsg" => 'Success.', "title" => 'Success.', 'data' => $memberDetail);
            return response()->json($data);
        }
        catch(Exception $ex) {
            log::info($ex->getMessage());
            $data = array('statusCode' => 0, "title" => 'Error.', "statusMsg" => $ex->getMessage());
            return response()->json($data);
        }
    }
    public function updateMemberDetailById(Request $request) {
        log::info("Coming into updateMemberDetailById");
        $name = $request['name'];
        $email = $request['email'];
        $company_id = $request['company_id'];
        $added_by = $request['added_by'];
        $memberid = $request['memberid'];
        $Bulkmailmember = Bulkmail::find($memberid);
        // print_r($Bulkmailmember);
        // echo $request->company_id;die;
        try {
            log::info("Checking Validation...");
            $validator = Validator::make($request->all(), ['name' => 'required', 'email' => 'required', ]);
            if ($validator->fails()) {
                Log::info("Validation Failed :- " . $validator->errors());
                $messages = $validator->errors();
                foreach ($messages->all(':message') as $message) {
                    $statusMsg = $message;
                }
                $data = array('statusCode' => 0, "title" => 'Error.', "statusMsg" => $statusMsg);
                return response()->json($data);
            } else {
                log::info("validation success");
                //             if(Member::where([
                // 	['email', '=', $email],
                // 	['company_id' ,'=' ,$company_id],
                // 	['added_by' , '!=' ,$added_by],
                // ])->exists()){
                //             	$data = array('statusCode' => 0, "statusMsg" => "Member with same email already exists!");
                //             	return response()->json($data);
                // }else{
                $Bulkmailmember->toname = $request->name;
                $Bulkmailmember->email = $request->email;
                $Bulkmailmember->save();
                // Member::where('email', $userEmail)
                //       ->update([
                //           'email' => $email
                //        ]);
                $data = array('statusCode' => 1, "title" => 'Sucess.', "statusMsg" => "Record Update Successfully");
                return response()->json($data);
                // }
                
            }
        }
        catch(Exception $ex) {
            log::info($ex->getMessage());
            $data = array('statusCode' => 0, "title" => 'Error.', "statusMsg" => $ex->getMessage());
            return response()->json($data);
        }
    }
    public function sendMailMember(Request $request, $memberid) {
        // echo $id;die;
        $Bulkmailmember = Bulkmail::find($memberid);
        dd($Bulkmailmember);
        if ($Bulkmailmember) {
            $resolutoinid = $Bulkmailmember->resolution_id;
            $resolitiondetail = Resolution::find($resolutoinid);
            $attcmentdetail = Mailattachment::where('resolution_id', $resolutoinid)->get();
            $companydetail = Company::where('id', $resolitiondetail->company_id)->first();
            $email = $Bulkmailmember->email;
            $mail_from = env("MAIL_FROM_ADDRESS");
            $fromname = $resolitiondetail->fromname;
            $fromemail = $resolitiondetail->email;
            $subject = $resolitiondetail->subject;
            $content = $resolitiondetail->content;
            // $signature =$companydetail->signature;
            if (!$attcmentdetail->isEmpty()) {
                Mail::send('bulkmail.bulkmailsent', ['content' => $content], function ($m) use ($email, $fromemail, $fromname, $subject, $resolutoinid, $mail_from, $attcmentdetail) {
                    $m->from($mail_from, $fromname);
                    $m->replyTo($fromemail, $fromname);
                    $m->to($email)->subject($subject);
                    $m->getHeaders()->addTextHeader('resolutionid', $resolutoinid);
                    foreach ($attcmentdetail as $attch) {
                        $m->attach(base_path() . '/public/mail/attachment/' . $attch->filename);
                    }
                });
            } else {
                Mail::send('bulkmail.bulkmailsent', ['content' => $content], function ($m) use ($email, $fromemail, $fromname, $subject, $resolutoinid, $mail_from) {
                    $m->from($mail_from, $fromname);
                    $m->getHeaders()->addTextHeader('resolutionid', $resolutoinid);
                    $m->replyTo($fromemail, $fromname);
                    $m->to($email)->subject($subject);
                });
            }
            $data = array('statusCode' => 1, "title" => 'Sucess.', "statusMsg" => "Mail Sent Successfully");
            return response()->json($data);
        } else {
            $data = array('statusCode' => 1, "title" => 'Error.', "statusMsg" => "Member Not Found!");
            return response()->json($data);
        }
    }
    public function getMailDetailById(Request $request) {
        log::info("Coming into getMailDetailById");
        try {
            log::info("Coming into try and get member details");
            $resolutionDetail = Resolution::find($request->RESOLUTIONID);
            $attachementDetail = Mailattachment::where('resolution_id', $request->RESOLUTIONID)->get(['filename']);
            $data = array('statusCode' => 1, "statusMsg" => 'Success.', "title" => 'Success.', 'data' => $resolutionDetail, 'attchment' => $attachementDetail);
            return response()->json($data);
        }
        catch(Exception $ex) {
            log::info($ex->getMessage());
            $data = array('statusCode' => 0, "title" => 'Error.', "statusMsg" => $ex->getMessage());
            return response()->json($data);
        }
    }
    public function addBulkmail(Request $request) {
        $inputdata = array();
        //company_id is maintion as Visition ID
        $masterrole = $request['masterrole'];
        $role_id = $request['role_id'];
        $content = $request['content'];
        $fromname = $request['fromname'];
        $fromemail = $request['email'];
        $subject = $request['subject'];
        $added_by = $request['added_by'];
        $mail_from = $hostname = env("MAIL_FROM_ADDRESS");
        $login_useid = session('userdata')->id;
        $message = array();
        $validator = Validator::make($request->all(), ['fromname' => 'required', 'email' => 'required', 'subject' => 'required', ]);
        if ($validator->fails()) {
            $message['message'] = "fromname required!";
            $message['type'] = "success";
            Session::flash('message', json_encode($message));
            return Redirect::to('bulkmails');
        } else {
            //
            $Company = Company::where('status', '1')->get();
            foreach ($Company as $Companys) {
                $ds_id = $Companys->designation_id;
                $d_id = $Companys->department_id;
                $s_id = $Companys->state_id;
                $p_id = $Companys->pincode_id;
                //dd($did);
                if ($ds_id == $role_id) {
                    $data = Company::where('designation_id', $role_id)->get();
                }
                if ($d_id == $role_id) {
                    $data = Company::where('department_id', $role_id)->get();
                }
                if ($s_id == $role_id) {
                    $data = Company::where('state_id', $role_id)->get();
                }
                if ($p_id == $role_id) {
                    $data = Company::where('pincode_id', $role_id)->get();
                }
            }
            if (!empty($data)) {
                $resolutiondata = array();
                $resolutiondata['fromname'] = $request['fromname'];
                $resolutiondata['email'] = $request['email'];
                $resolutiondata['subject'] = $request['subject'];
                $resolutiondata['role_id'] = $request['role_id'];
                $resolutiondata['content'] = $request['content'];
                $resolutiondata['added_by'] = $request['added_by'];
                //Created resolution and insert data into resolution table
                $resolution = Resolution::create($resolutiondata);
                if ($resolution) {
                    //Resolutoin last ID
                    $inputdata['resolution_id'] = $resolution->id;
                    $resolutoinid = $resolution->id;
                    $inputdata['role_id'] = $request['role_id'];
                    //store file in attchment table and upload files
                    $attachmentdata = array();
                    if ($request->file('file')) {
                        //sent mail with attchment
                        $files = $request->file('file');
                        foreach ($files as $file) {
                            $attachmentName = time() . '_' . str_replace(' ', '_', $file->getClientOriginalName());
                            $file->move(base_path() . '/public/mail/attachment/', $attachmentName);
                            $attachmentdata['filename'] = $attachmentName;
                            $attachmentdata['resolution_id'] = $resolution->id;
                            $attachmentdata['role_id'] = $request['role_id'];
                            $attachmentdata['filepath'] = base_path() . '/public/mail/attachment/';
                            $mailattachment = Mailattachment::create($attachmentdata);
                        }
                    }
                    if ($request->file('file')) {
                        $attdetail = array();
                        $attdetail = Mailattachment::where('resolution_id', $resolutoinid)->get();
                        $mailattachmenttable = 'Please open the following link to view the attachment';
                        $mailattachmenttable.= '<table border="1"><tr><th>Sr No.</th><th>Attachment</th></tr>';
                        $i = 1;
                        foreach ($attdetail as $attch) {
                            $fileefullurl = env("baseURL") . '/public/mail/attachment/' . $attch->filename;
                            $mailattachmenttable.= '<tr>';
                            $mailattachmenttable.= '<td>' . $i . '</td>';
                            $mailattachmenttable.= '<td> <a href=' . $fileefullurl . '>' . $attch->filename . '</a></td>';
                            $mailattachmenttable.= '</tr></td>';
                            $i++;
                        }
                        $mailattachmenttable.= '</table>';
                    } else {
                        $mailattachmenttable = '';
                    }
                    // print_r($mailattachmenttable);die;
                    foreach ($data as $visitor) {
                        //Store data in bulkmails table and sent mail
                        $email = $visitor->email;
                        $inputdata['toname'] = $visitor->name;
                        $inputdata['email'] = $email;
                        try {
                            Mail::send('bulkmail.bulkmailsent', ['content' => $content, 'mailattachmenttable' => $mailattachmenttable], function ($m) use ($email, $fromemail, $fromname, $subject, $resolutoinid, $mail_from) {
                                $m->from($mail_from, $fromname);
                                $m->replyTo($fromemail, $fromname);
                                $m->to($email)->subject($subject);
                                $m->getHeaders()->addTextHeader('resolutionid', $resolutoinid);
                            });
                            //check for failures
                            if (!Mail::failures()) {
                                // return response showing failed emails
                                log::info("Data successfully mail failures");
                                $inputdata['email_status'] = "Y";
                                $inputdata['status'] = "NA";
                                $inputdata['added_by'] = $request['added_by'];
                                // print_r($inputdata);die;
                                $bulkmail = Bulkmail::create($inputdata);
                            }
                        }
                        catch(Exception $ex) {
                            log::info("Coming into mail catch");
                            log::error($ex->getMessage());
                            log::info("Add status to datatable");
                            $Reason = substr($ex->getMessage(), 0, 100);
                            $reason2 = explode("with message", $ex->getMessage());
                            if (strlen($Reason) >= strlen($reason2[0])) {
                                $inputdata['email_status'] = "E";
                                $inputdata['status'] = $Reason;
                                $inputdata['added_by'] = $request['added_by'];
                                $insertquery = Bulkmail::create($inputdata);
                                // $insertquery = Bulkmail::where('toname', $email)
                                // ->update(['reason' => $Reason, 'email_status' => 'E']);
                                if ($insertquery) {
                                    log::info("Data successfully single status email_status E");
                                }
                            } else {
                                $inputdata['email_status'] = "E";
                                $inputdata['status'] = $reason2[0];
                                $inputdata['added_by'] = $request['added_by'];
                                $insertquery = Bulkmail::create($inputdata);
                                // $insertquery = Bulkmail::where('toname', $email)
                                // ->update(['reason' => $reason2[0], 'email_status' => 'E']);
                                if ($insertquery) {
                                    log::info("Data successfully email_status E");
                                }
                            }
                            // return redirect('/member-detail/' . $ResolutionID)->with('alert', 'Mail service is not responding, Please try again later.');
                            
                        }
                    }
                    $data = array('statusCode' => 1, "statusMsg" => 'Mail Sent successfully!', 'title' => 'Success');
                    return response()->json($data);
                } else {
                    $data = array('statusCode' => 0, "statusMsg" => 'Resolution Not Created', 'title' => 'Error');
                    return response()->json($data);
                }
            } else {
                $data = array('statusCode' => 0, "statusMsg" => 'visitor Detail Not found', 'title' => 'Error');
                return response()->json($data);
            }
        }
    }
    public function memberListByresolution(Request $request, $id) {
        $memberlist = Bulkmail::where('resolution_id', $id)->get();
        return view("bulkmail.memberlist", ["memberlists" => $memberlist, "page_title" => "Member List"]);
    }
    public function sendmailbulkmailapproved() {
        Log::info("Coming into sendmailbulkmailapprovedCron");
        try {
            $CurrentDate = date('Y-m-d');
            Log::info("Send Mail to Only Resolution table status N");
            $Resolution = Resolution::whereIn('sendmailapproved', ['P', 'N'])
            // ->where('ENDDATE', '>', $CurrentDate)
            ->orderBy('id', 'ASC')->first();
            // echo "<pre>";
            //    print_r($Resolution);
            $mail_from = env("MAIL_FROM_ADDRESS");
            if (isset($Resolution)) {
                $resolutoinid = $Resolution->id;
                $company_id = $Resolution->company_id;
                $company = Company::find($company_id);
                $signature = $company->signature;
                $members = Member::where([['company_id', $company_id], ['active', 1]])->get();
                $updatebulkstatus = Resolution::where('id', $resolutoinid)->update(['sendmailapproved' => "P"]);
                if ($updatebulkstatus) {
                    log::info("Update Resolution Status Progress");
                }
                $attdetail = array();
                $attdetail = Mailattachment::where('resolution_id', $resolutoinid)->get();
                if ($attdetail) {
                    $mailattachmenttable = 'Please open the following link to view the attachment<br>';
                    $mailattachmenttable.= '<table border="1"><tr><th>Sr No.</th><th>Attachment</th></tr>';
                    $i = 1;
                    foreach ($attdetail as $attch) {
                        $fileefullurl = env("baseURL") . '/public/mail/attachment/' . $attch->filename;
                        $mailattachmenttable.= '<tr>';
                        $mailattachmenttable.= '<td>' . $i . '</td>';
                        $mailattachmenttable.= '<td> <a href=' . $fileefullurl . '>' . $attch->filename . '</a></td>';
                        $mailattachmenttable.= '</tr></td>';
                        $i++;
                    }
                    $mailattachmenttable.= '</table>';
                } else {
                    $mailattachmenttable = '';
                }
                foreach ($members as $member) {
                    //Store data in bulkmails table and sent mail
                    $email = $member->email;
                    $fromname = $Resolution->fromname;
                    $fromemail = $Resolution->email;
                    $subject = $Resolution->subject;
                    $added_by = $Resolution->added_by;
                    $content = $Resolution->content;
                    // $signature =$Resolution->signature;
                    try {
                        Mail::send('bulkmail.bulkmailsent', ['content' => $content, 'mailattachmenttable' => $mailattachmenttable], function ($m) use ($email, $fromemail, $fromname, $subject, $resolutoinid, $mail_from) {
                            $m->from($mail_from, $fromname);
                            $m->replyTo($fromemail, $fromname);
                            $m->to($email)->subject($subject);
                            $m->getHeaders()->addTextHeader('resolutionid', $resolutoinid);
                        });
                        // check for failures
                        if (!Mail::failures()) {
                            // return response showing failed emails
                            log::info("Data successfully mail to member and update member reson and status Y ");
                            // $inputdata['email_status'] = "Y";
                            // $inputdata['status'] = "NA";
                            // $inputdata['added_by'] = $request['added_by'];
                            // print_r($inputdata);die;
                            // $bulkmail=Bulkmail::create($inputdata);
                            $updatebulkstatus = Bulkmail::where('email', $email)->where('resolution_id', $resolutoinid)->update(['email_status' => "Y", 'status' => "NA", 'added_by' => $added_by]);
                            if ($updatebulkstatus) {
                                log::info("Data successfully single status email Y status NA  in bulkmail tableupdate");
                            }
                        }
                    }
                    catch(Exception $ex) {
                        log::info("Coming into mail catch");
                        log::error($ex->getMessage());
                        log::info("Add status to datatable");
                        $Reason = substr($ex->getMessage(), 0, 100);
                        $reason2 = explode("with message", $ex->getMessage());
                        if (strlen($Reason) >= strlen($reason2[0])) {
                            // $inputdata['email_status'] = "E";
                            // $inputdata['status'] = $Reason;
                            // $inputdata['added_by'] = $request['added_by'];
                            // $insertquery=Bulkmail::create($inputdata);
                            $updatebulkstatus = Bulkmail::where('email', $email)->where('resolution_id', $resolutoinid)->update(['email_status' => "E", 'reason' => $Reason, 'added_by' => $added_by]);
                            // $insertquery = Bulkmail::where('toname', $email)
                            // ->update(['reason' => $Reason, 'email_status' => 'E']);
                            if ($updatebulkstatus) {
                                log::info("Data successfully single status email status E in bulkmail tableupdate");
                            }
                        } else {
                            // $inputdata['email_status'] = "E";
                            // $inputdata['status'] = $reason2[0];
                            // $inputdata['added_by'] = $request['added_by'];
                            // $insertquery=Bulkmail::create($inputdata);
                            $updatebulkstatus = Bulkmail::where('email', $email)->where('resolution_id', $resolutoinid)->update(['email_status' => "E", 'reason' => $reason2[0], 'added_by' => $added_by]);
                            // $insertquery = Bulkmail::where('toname', $email)
                            // ->update(['reason' => $reason2[0], 'email_status' => 'E']);
                            if ($updatebulkstatus) {
                                log::info("Data successfully email_status E in bulkmail Updatetable");
                            }
                        }
                    }
                    /* Check Sent Mail to All Member Or not */
                    $RemainingCountSendMailToMember = Bulkmail::where('email_status', 'N')->where('resolution_id', $resolutoinid)->count();
                    // echo $RemainingCountSendMailToMember;
                    if ($RemainingCountSendMailToMember > 0) {
                        Log::info("Remaining to Send Mail No. of member ");
                    } else {
                        Log::info("Resolution Going To Update Flag Success(Y) for send mail to All member where Resolution ID is " . $resolutoinid);
                        $UpdateFlagSuccess = Resolution::where('id', $resolutoinid)->update(['sendmailapproved' => "Y"]);
                        if ($UpdateFlagSuccess) {
                            Log::info("Resolution Going To Update Flag Success(Y) for send mail to All member where Resolution ID is $resolutionid ");
                        }
                    }
                }
                $data = array('statusCode' => 1, "statusMsg" => "Mail Sent successfully to member!", 'title' => 'Success');
                return response()->json($data);
            } else {
                $data = array('statusCode' => 0, "statusMsg" => 'Resolution/Bulkmail List Detail Notfound', 'title' => 'Error');
                return response()->json($data);
            }
            echo json($data);
            die;
            // return response()->json($data);
            
        }
        catch(Exception $ex) {
            Log::info('sendmailtomember Ex Catch');
            // DB::rollBack();
            // ChannelLog::info('sendmailtomember', "Db Rollback");
            // ChannelLog::error('sendmailtomember', $ex->getMessage());
            $data = array('statusCode' => 0, "statusMsg" => $ex->getMessage(), 'title' => 'Error');
            return response()->json($data);
        }
    }
}
