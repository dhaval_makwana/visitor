@extends('hauper_admin') @section('content')
<div class='row'>
	<div class='col-md-12'>
		<form action="{{ env('baseURL') }}/user/update" method="post" class="form-horizontal" enctype="multipart/form-data">
			<input required name="id" type="hidden" value="{{$user->id}}">
			<div class="form-group">
				<label for="inputFName" class="col-sm-2 control-label">Full Name</label>
				<div class="col-sm-10">
					<input name="name" type="text" class="form-control" id="inputFName" placeholder="Full Name" value="{{$user->name}}" required>
				</div>
			</div>
			<div class="form-group">
				<label for="inputPhone" class="col-sm-2 control-label">Mobile No</label>
				<div class="col-sm-10">
					<input type="text" name="phone" pattern="\d*" title="Please input valid Mobile No" maxlength="10" minlength="10" class="form-control" id="inputPhone" placeholder="Mobile No" value="{{$user->phone}}">
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail" class="col-sm-2 control-label">Email</label>
				<div class="col-sm-10">
					<input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email" value="{{$user->email}}" readonly="">
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword" class="col-sm-2 control-label">Password</label>
				<div class="col-sm-10">
					<input name="password" type="text" class="form-control" id="inputPassword" placeholder="New Password">
					<span class="text-default">Leave this blank if don't want to change</span>
				</div>
			</div>
			
			<div class="form-group">
				<label for="file" class="col-sm-2 control-label">Profile Pic</label>
				<div class="col-sm-10">
					<input type="file" name="file" class="form-control" id="file"><br>
					@if ($user->profile_pic)
					<img style="height: 90px;width: 90px;" src="{{ env('baseURL') }}/public/images/profile_pic/{{$user->profile_pic}}">
					@endif
				</div>
			</div>
			
			{{ csrf_field() }}
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-danger">Save</button>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					@if(Session::has('message'))
			        	<p class="text-info">{{ Session::get('message') }}</p>
					@endif
				</div>
			</div>
		</form>
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
@endsection