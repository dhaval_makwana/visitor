@extends('hauper_admin') @section('content')
<div class='row'>
	<div class='col-md-12'>
		<form name="sentmailform" id="sentmailform" class="form-horizontal" enctype="multipart/form-data">
			<meta name="csrf-token" content="{{ csrf_token() }}">

			<div class="form-group">
				<label for="fromname" class="col-sm-2 control-label">From Name</label>
				<div class="col-sm-10">
					<input name="fromname" type="text" class="form-control" id="fromname" placeholder="From Name" required>
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail" class="col-sm-2 control-label">From Email</label>
				<div class="col-sm-10">
					<input name="email" type="email" class="form-control" id="email" placeholder="Email" required>
				</div>
			</div>
			<div class="form-group">
				<label for="subject" class="col-sm-2 control-label">Mail Subject</label>
				<div class="col-sm-10">
					<input name="subject" type="text" class="form-control" id="subject" placeholder="Mail Subject" required>
				</div>
			</div>
			<div class="form-group">
				<label for="company_id" class="col-sm-2 control-label">Company</label>
				<div class="col-sm-10"> 
					<select required name="company_id" id="company_id" class="form-control" >
						<option value="" >Select Company </option>
						@foreach($companys as $company)
							<option value="{{$company->id}}" >{{$company->name}} </option>
			            @endforeach
					</select>
		        </div>
			</div>
			<div class="form-group">
				<label for="inputdescription" class="col-sm-2 control-label">Mail Content</label>
				<div class="col-sm-10">
					<textarea class="form-control editor" name="content" cols="50" rows="10" id="mailcontent"  placeholder="Description" required></textarea>
					<!-- <input name="description" type="text" class="form-control" id="inputdescription" placeholder="Description" required> -->
				</div>
			</div>

			<div class="form-group">
				<label for="file" class="col-sm-2 control-label">Attachment</label>
				<div class="col-sm-10">
					<input type="file" name="file" class="form-control" id="file" accept="application/pdf">
				</div>
			</div>
			{{ csrf_field() }}
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary" id='submitmail'>Sent Mail</button>
                    <!-- <button onclick="window.history.go(-1); return false;" class="btn btn-danger" >Back</button> -->
                     <img id="loading_img" alt="Loading Image" name="loading_img" src="<?php echo URL::asset('public/images/loadingx2.gif'); ?>" style="height: 30px; width: 30px; display: none;" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					@if(Session::has('message'))
			        	<!-- <p class="text-info">{{ Session::get('message') }}</p> -->
			        	<p class="text-info">{{ json_decode(Session::get('message'))->message }}</p>
			        	
					@endif
				</div>
			</div>

		</form>
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
<script src="https://cdn.ckeditor.com/4.10.0/full/ckeditor.js"></script>
<script>
	CKEDITOR.replace( 'content',{
		toolbarGroups: [
		{ name: 'document',	   groups: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },			
 		{ name: 'clipboard',   groups: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },	
 		{ name: 'editing', 	   groups: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },		
 		{ name: 'forms', 	   groups: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
 		'/',																
 		{ name: 'basicstyles', groups: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
		{ name: 'paragraph', groups: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
		{ name: 'links', groups: [ 'Link', 'Unlink', 'Anchor' ] },
		{ name: 'insert', groups: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
		'/',
		// { name: 'styles', groups: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
		// { name: 'colors', groups: [ 'TextColor', 'BGColor' ] },
		// { name: 'tools', groups: [ 'Maximize', 'ShowBlocks' ] },
		// { name: 'about', groups: [ 'About' ] }
	]
	} );
</script>
@endsection

@section('javascript')
<script>
$(document).ready(function () {
	//Bootstrap Validation check and data pass
    $( "#sentmailform" ).submit(function( event ) {
        event.preventDefault();
        
        	var appUrl ="{{env('baseURL')}}";

        	CKupdate();
            $("#loading_img").show();
            $("#submitmail").prop('disabled', true);
            
   //          var file = $('#file').val();
   //          var data = $('#sentmailform').serializeArray();
			// data.push({name: 'file', value: file});
			var formData = new FormData(this);

            $.ajax({
            	
		        url: appUrl + "/mail/add",
		        type: 'POST',
		        data: formData,
		        cache:false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function (data) {
                    $("#loading_img").hide();
                    if (data.statusCode == 1) {
			            swal({
			                title: data.title,
			                text: data.statusMsg,
			                closeOnConfirm: true
			            });
                        // alert(data.statusMsg);
                        // console.log(data.statusMsg);
                        window.location.href = appUrl + "/bulkmails";
                    } else {
                        alert(data.statusMsg);
                        $("#submitmail").prop('disabled', false);
                    }
                },
                error: function (jqXHR, exception) {
                    // console.log(jqXHR);
                    console.log("Call showAjaxErrorMsg");
                    // showAjaxErrorMsg(jqXHR, exception);
                    $("#submitmail").prop('disabled', false);
                    $("#loading_img").hide();
                }
            });
        

    });
});
function CKupdate(){
    for ( instance in CKEDITOR.instances )
        CKEDITOR.instances[instance].updateElement();
}
</script>
@stop