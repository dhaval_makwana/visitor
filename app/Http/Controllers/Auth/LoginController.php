<?php

namespace App\Http\Controllers\Auth;

use Session;
use Auth;
use Mail;
use App\User;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index(Request $request){
        return view('login');
    }

    public function login(Request $request){ 
        $validator = Validator::make($request->all(), [            
            'email' => 'required',
            'password' => 'required'
        ]);

        if (!filter_var($request->input('email'), FILTER_VALIDATE_EMAIL) === false) {
            $login_via = 'email';
        } else {
            $login_via = 'phone';
        }

        if($login_via == 'email'){
            $credentials = $request->only('email', 'password');
        } else{
            $credentials['phone'] = $request->input('email');
            $credentials['password'] = $request->input('password');
        }
        $results=array();                        
        if(Auth::attempt($credentials)){
            $userdata = User::find(Auth::id());
            if($userdata->active == "0"){
                Session::flush();
                Auth::logout();
                Session::flash('message', 'User is not active yet!');
                return Redirect::to('login');
            }

            if(!$userdata->profile_pic){
                $userdata->profile_pic = 'public/avatar.png';
            } else{
                $userdata->profile_pic = 'public/images/profile_pic/'.$userdata->profile_pic;
            }
		
            Session(['userdata' => $userdata]);
            if($userdata->type == "1")
                return Redirect::to('admindashbord');
            else if($userdata->type == "0")
                return Redirect::to('dashbord');
            else 
                Session::flush();
                Auth::logout();
                Session::flash('message', 'Incorrect credentials!');
                return Redirect::to('login');
        }else{
            Session::flash('message', 'Incorrect credentials!');
            return Redirect::to('login');
        }
    }
	
	public function userchangepassword(Request $request){
        $validator = Validator::make($request->all(), [            
            'id' => 'required',
            'new_password' => 'required',
			'current_password' => 'required'
        ]);
	
		$credentials['id'] = $request->input('id');
        $credentials['password'] = $request->input('current_password');
		
		if(Auth::attempt($credentials)){
			$user = User::find(Auth::id());
				
            if($user->active == "0"){
                $response['status'] = 'error';
                $response['message'] = 'User is not active yet!';
            } else{
                $new_password_hash = bcrypt($request->new_password);
  				$user->password=$new_password_hash;
				$user->save();

				$response['status']="success";
				$response['message']="Password changed!";
            }
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Incorrect credentials!';
        }
        echo json_encode($response);exit;
    }
	
    public function userlogin(Request $request){
        $validator = Validator::make($request->all(), [            
            'email' => 'required',
            'password' => 'required'
        ]);

        if (!filter_var($request->input('email'), FILTER_VALIDATE_EMAIL) === false) {
            $login_via = 'email';
        } else {
            $login_via = 'phone';
        }

        if($login_via == 'email'){
            $credentials = $request->only('email', 'password');
        } else{
            $credentials['phone'] = $request->input('email');
            $credentials['password'] = $request->input('password');
        }
        $response = array();
        if(Auth::attempt($credentials)){
            $userdata = User::find(Auth::id());
            if($userdata->active == "0"){
                $response['status'] = 'error';
                $response['message'] = 'User is not active yet!';
            } else{
                $response['status'] = 'success';
                $response['message'] = 'Login Succsessful!';
                $response['data']['id'] = $userdata['id'];
                $response['data']['first_name'] = $userdata['first_name'];
                $response['data']['last_name'] = $userdata['last_name'];
                $response['data']['email'] = $userdata['email'];
                $response['data']['profile_pic'] = $userdata['profile_pic'];
                $response['data']['phone'] = $userdata['phone'];
            }
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Incorrect credentials!';
        }
        echo json_encode($response);exit;
    }
	
    public function create(Request $request){
        $validator = Validator::make($request->all(), [            
            'phone' => 'required|numeric'
        ]);
        if ($validator->fails()){            
            Session::flash('message', 'Phone number is not valid!');
            return Redirect::to('register');
        }

        if (User::where('email', '=', $request['email'])->orWhere('phone', '=', $request['phone'])->exists()) {
            Session::flash('message', 'User with same email/phone number already exists!');
            return Redirect::to('register');
        } else{
            $dataArray = array();
            $dataArray['type'] = "0";
            $dataArray['active'] = "0";
            $dataArray['first_name'] = $request['first_name'];
            $dataArray['last_name'] = $request['last_name'];
            $dataArray['phone'] = $request['phone'];
            $dataArray['email'] = $request['email'];
            $dataArray['password'] = bcrypt($request['password']);
            if(User::create($dataArray)){
                $credentials = $request->only('email', 'password');
                if(Auth::attempt($credentials)){
                    $userdata = User::find(Auth::id());
                    if($userdata->active == "0"){
                        Session::flush();
                        Auth::logout();
                        Session::flash('message', 'User is not active yet!');
                        return Redirect::to('login');
                    }

                    if(!$userdata->profile_pic){
                        $userdata->profile_pic = 'public/avatar.png';
                    } else{
                        $userdata->profile_pic = 'public/images/profile_pic/'.$userdata->profile_pic;
                    }
                    Session(['userdata' => $userdata]);
                    return Redirect::to('users');
                }else{
                    Session::flash('message', 'If you cannot remember your login details, please contact support!');
                    return Redirect::to('register');
                }
            } else{
                Session::flash('message', 'Can not able to signup, please contact support!');
                return Redirect::to('register');
            }
        }
    }

    public function logout(Request $request){
        Session::flush();
        Auth::logout();
        return Redirect::to('login');
    }
}
