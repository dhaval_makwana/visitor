@extends('hauper_admin') @section('content')

<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="{{ env('baseURL') }}/company/update"  enctype="multipart/form-data">
   				<input required name="id" type="hidden" value="{{$company->id}}">

                <div class="card-body">
	                <div class="row">
					    <div class="form-group col-md-6">
					        <label for="organization">Organization Name: <span>*</span></label>
					        <input name="organization" type="text" class="form-control" id="organization" placeholder="Organization Name" value="{{$company->organization}}" required>
					    </div>
					    <div class="form-group col-md-6">
					        <label for="name">Name of Person: <span>*</span></label>
					        <input name="name" type="text" class="form-control" id="name" placeholder="Name of Person" value="{{$company->name}}" required>
					    </div>
					</div>
					<div class="row">
					    <div class="form-group col-md-6">
					        <label for="designation">Designation: <span>*</span></label>
					        <select name="designation_id" id="designation_id" class="form-control" required>
								<option value="" >Select Designation</option>
								@foreach($designations as $designation)
									<option value="{{$designation->id}}" @if($designation->id==$company->designation_id) selected='selected' @endif >{{$designation->name}} </option>
					            @endforeach
							</select>
					        <!-- <input name="designation" type="text" class="form-control" id="designation" placeholder="Designation" value="{{$company->designation}}" required> -->
					    </div>
					    <div class="form-group col-md-6">
					        <label for="department">Department: <span>*</span></label>
					        <select name="department_id" id="department_id" class="form-control" required>
								<option value="" >Select Department</option>
								@foreach($departments as $department)
									<option value="{{$department->id}}" @if($department->id==$company->department_id) selected='selected' @endif >{{$department->name}} </option>
					            @endforeach
							</select>
					        <!-- <input name="department" type="text" class="form-control" id="department" placeholder="Department" value="{{$company->department}}" required> -->
					    </div>
					</div>
					<div class="row">
					    <div class="form-group col-md-6">
					        <label for="mobile">Mobile Number: <span>*</span></label>
					        <input name="mobile" type="text" class="form-control" id="mobile" placeholder="Mobile Number" value="{{$company->mobile}}" required>
					    </div>
					    <div class="form-group col-md-6">
					        <label for="email">Email Id: <span>*</span></label>
					        <input name="email" type="email" class="form-control" id="email" placeholder="Email Id" value="{{$company->email}}" required>
					    </div>
					</div>
					<div class="row">
					    <div class="form-group col-md-6">
					        <label for="phone_number">Phone Number: </label>
					        <input name="phone_number" type="text" class="form-control" id="phone_number" placeholder="Phone Number" value="{{$company->phone_number}}">
					    </div>
					    <div class="form-group col-md-6">
					        <label for="a_email">Alternative Email Id </label>
					        <input name="a_email" type="email" class="form-control" id="a_email" placeholder="Alternative Email Id" value="{{$company->a_email}}">
					    </div>
					</div>
					<div class="row">
					    <div class="form-group col-md-6">
					        <label for="address">Address: <span>*</span></label>
					        <input name="address" type="text" class="form-control" id="address" placeholder="Address" value="{{$company->address}}" required>
					    </div>
					    <div class="form-group col-md-6">
					    	<label for="requirement">Requirement: </label>
					        <input name="requirement" type="text" class="form-control" id="requirement" placeholder="Requirement" value="{{$company->requirement}}" required>
					    </div>
					</div>
					<div class="row">
					    <div class="form-group col-md-6">
					        <label for="state">State: <span>*</span></label>
					        <select name="state_id" id="state_id" class="form-control" required>
								<option value="" >Select State</option>
								@foreach($states as $state)
									<option value="{{$state->id}}" @if($state->id==$company->state_id) selected='selected' @endif >{{$state->name}} </option>
					            @endforeach
							</select>
					    </div>
					    <div class="form-group col-md-6">
					    	<label for="zipcode">Pin Code: <span>*</span></label>
					        <select name="pincode_id" id="pincode_id" class="form-control" required>
								<option value="" >Select Pincode </option>
								@foreach($pincodes as $pincode)
									<option value="{{$pincode->id}}" @if($pincode->id==$company->pincode_id) selected='selected' @endif >{{$pincode->name}} </option>
					            @endforeach
							</select>

					    </div>
					</div>
					<div class="row">
					    <div class="form-group col-md-6">
					        <label for="place">Place: <span>*</span></label>
					        <input name="place" type="text" class="form-control" id="place" placeholder="Place" value="{{$company->place}}" required>
					    </div>
					    <div class="form-group col-md-6">
					        <label for="remarks">Remarks: </label>
					        <input name="remarks" type="text" class="form-control" id="remarks" placeholder="Remarks" value="{{$company->remarks}}" required>
					    </div>
					</div>
					<div class="row">
					    <div class="form-group col-md-6">
					        <label for="category">Category: </label>
					        <input name="category" type="text" class="form-control" id="category" placeholder="Category" value="{{$company->category}}" required>
					    </div>
					    <div class="form-group col-md-6">
					        <label for="attachment" class="custom-file-label">Upload Attchment Document</label>
					        <input type="file" name="attchment" class="form-control" id="file">
					        @if ($company->attchment)
							<a href="{{ env('baseURL') }}/public/attchment/{{$company->attchment}}" target="_blank"> View</a>
							<!-- <img style="height: 90px;width: 90px;" src="{{ env('baseURL') }}/public/attchment/{{$company->attchment}}"> -->
							@endif
					    </div>
					</div>
					<div class="row">
					    <div class="form-group col-md-6">
					        <label for="primate">Reference Person: Yes/ NO: </label>
					        <select id="reference_person" name='reference_person' class="form-control" onchange="showDiv('hidden_div', this)">
							   <option value="0" @if($company->reference_person==0) selected='selected' @endif >No</option>
							   <option value="1" @if($company->reference_person==1) selected='selected' @endif >Yes</option>
							</select>
					    </div>
					    <div class="form-group col-md-6">
					       
					    </div>
					</div>
					<div id="hidden_div">
						<div class="row">
						    <div class="form-group col-md-6">
						        <label for="r_organization">Organization Name: </label>
						        <input name="r_organization" type="text" class="form-control" id="r_organization" placeholder="Organization Name" value="{{$company->r_organization}}">
						    </div>
						    <div class="form-group col-md-6">
						        <label for="r_name">Name of Person: </label>
						        <input name="r_name" type="text" class="form-control" id="r_name" placeholder="Name of Person" value="{{$company->r_name}}">
						    </div>
						</div>
						<div class="row">
						    <div class="form-group col-md-6">
						        <label for="r_designation">Designation: </label>
						        <input name="r_designation" type="text" class="form-control" id="r_designation" placeholder="Designation" value="{{$company->r_designation}}">
						    </div>
						    <div class="form-group col-md-6">
						        <label for="r_mobile">Mobile Number: </label>
						        <input name="r_mobile" type="text" class="form-control" id="r_mobile" placeholder="Mobile Number" value="{{$company->r_mobile}}">
						    </div>
						</div>
						<div class="row">
						    <div class="form-group col-md-6">
						        <label for="r_email">Email Id : </label>
						        <input name="r_email" type="email" class="form-control" id="r_email" placeholder="Email Id " value="{{$company->r_email}}">
						    </div>
						    <div class="form-group col-md-6">
						        <label for="r_phone">Phone Number: </label>
						        <input name="r_phone" type="text" class="form-control" id="r_phone" placeholder="Phone Number" value="{{$company->r_phone}}">
						    </div>
						</div>
						<div class="row">
						    <div class="form-group col-md-6">
						        <label for="r_address">Address : </label>
						        <input name="r_address" type="text" class="form-control" id="r_address" placeholder="Address " value="{{$company->r_address}}">
						    </div>
						    <div class="form-group col-md-6">
						        <label for="r_zipcode">Pin Code: </label>
						        <input name="r_zipcode" type="text" class="form-control" id="r_zipcode" placeholder="Pin Code" value="{{$company->r_zipcode}}">
						    </div>
						</div>
						<div class="row">
						    <div class="form-group col-md-6">
						        <label for="r_category">Category : </label>
						        <input name="r_category" type="text" class="form-control" id="r_category" placeholder="Category " value="{{$company->r_category}}">
						    </div>
						    <div class="form-group col-md-6">
						        <label for="r_remarks">Remarks: </label>
						        <input name="r_remarks" type="text" class="form-control" id="r_remarks" placeholder="Remarks" value="{{$company->r_remarks}}">
						    </div>
						</div>
					</div>
						<div class='row'>
							
							{{ csrf_field() }}
							<div class="form-group col-md-6">
									<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
				                    <button onclick="window.history.go(-1); return false;" class="btn btn-danger" ><i class="fa fa-times" aria-hidden="true"></i> Back</button>
							</div>
							<div class="form-group col-md-6">
								@if(Session::has('message'))
						        	<p class="text-info">{{ Session::get('message') }}</p>
								@endif
							</div>
						</div>

		     	</div>
			               
            </form>
            </div>
          </div>
        </div>
      </div>
</section>
<style type="text/css">
@if($company->reference_person==0)
#hidden_div {
    display: none;
}
@else
#hidden_div {
    display: block;
}

@endif 
</style>
<script type="text/javascript">

function showDiv(divId, element)
{
    document.getElementById(divId).style.display = element.value == 1 ? 'block' : 'none';
}
</script>
<!-- 
<div class='row'>
	<div class='col-md-12'>
		<form action="{{ env('baseURL') }}/company/update" method="post" class="form-horizontal" enctype="multipart/form-data">
			<input required name="id" type="hidden" value="{{$company->id}}">
			
			<div class='col-md-12'>
				<div class="form-group">
					<label for="name" class="col-sm-2 control-label">Name*</label>
					<div class="col-sm-10">
						<input name="name" value="{{$company->name}}" type="text" class="form-control" id="name" placeholder="Name" required>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-sm-2 control-label">Email*</label>
					<div class="col-sm-10">
						<input name="email" value="{{$company->email}}" type="email" class="form-control" id="email" placeholder="Email" required>
					</div>
				</div>
				<div class="form-group">
					<label for="place" class="col-sm-2 control-label">Place</label>
					<div class="col-sm-10">
						<input name="place" value="{{$company->place}}" type="text" class="form-control" id="place" placeholder="Place">
					</div>
				</div>
				<div class="form-group">
					<label for="address" class="col-sm-2 control-label">Address</label>
					<div class="col-sm-10">
						<input name="address" value="{{$company->address}}" type="text" class="form-control" id="address" placeholder="Address">
					</div>
				</div>
				<div class="form-group">
					<label for="remarks" class="col-sm-2 control-label">Remarks</label>
					<div class="col-sm-10">
						<input name="remarks" value="{{$company->remarks}}" type="text" class="form-control" id="remarks" placeholder="Remarks">
					</div>
				</div>
				<div class="form-group">
					<label for="requirement" class="col-sm-2 control-label">Requirement</label>
					<div class="col-sm-10">
						<input name="requirement" value="{{$company->requirement}}" type="text" class="form-control" id="requirement" placeholder="Requirement">
					</div>
				</div>
				<div class="form-group">
					<label for="rf_email" class="col-sm-2 control-label">Reference Person Email </label>
					<div class="col-sm-10">
						<input name="rf_email" value="{{$company->rf_email}}" type="email" class="form-control" id="rf_email" placeholder="Reference Person Email ">
					</div>
				</div>
				<div class="form-group">
					<label for="rf_address" class="col-sm-2 control-label">Reference Person Address </label>
					<div class="col-sm-10">
						<input name="rf_address" value="{{$company->rf_address}}" type="text" class="form-control" id="rf_address" placeholder="Reference Person Address ">
					</div>
				</div>
				<div class="form-group">
					<label for="organization" class="col-sm-2 control-label">Organization</label>
					<div class="col-sm-10">
						<input name="organization" value="{{$company->organization}}" type="text" class="form-control" id="organization" placeholder="Organization">
					</div>
				</div>
			</div>
			<div class='col-md-12'>
				<div class="form-group">
					<label for="mobile" class="col-sm-2 control-label">Mobile*</label>
					<div class="col-sm-10">
						<input name="mobile" value="{{$company->mobile}}" type="text" class="form-control" id="mobile" placeholder="Mobile" required>
					</div>
				</div>
				<div class="form-group">
					<label for="category" class="col-sm-2 control-label">Category*</label>
					<div class="col-sm-10">
						<input name="category" value="{{$company->category}}" type="text" class="form-control" id="category" placeholder="Category" required>
					</div>
				</div>
				<div class="form-group">
					<label for="department" class="col-sm-2 control-label">Department</label>
					<div class="col-sm-10">
						<input name="department" value="{{$company->department}}" type="text" class="form-control" id="department" placeholder="Department">
					</div>
				</div>
				<div class="form-group">
					<label for="address_two" class="col-sm-2 control-label">Address_2</label>
					<div class="col-sm-10">
						<input name="address_two" value="{{$company->address_two}}" type="text" class="form-control" id="address_two" placeholder="Other Address">
					</div>
				</div>
				<div class="form-group">
					<label for="subject" class="col-sm-2 control-label">Subject</label>
					<div class="col-sm-10">
						<input name="subject" value="{{$company->subject}}" type="text" class="form-control" id="subject" placeholder="Subject">
					</div>
				</div>
				<div class="form-group">
					<label for="rf_name" class="col-sm-2 control-label">Reference Person name</label>
					<div class="col-sm-10">
						<input name="rf_name" value="{{$company->rf_name}}" type="text" class="form-control" id="rf_name" placeholder="Reference Person name">
					</div>
				</div>
				<div class="form-group">
					<label for="rf_mobile" class="col-sm-2 control-label">Reference Person Mobile </label>
					<div class="col-sm-10">
						<input name="rf_mobile" value="{{$company->rf_mobile}}" type="text" class="form-control" id="rf_mobile" placeholder="Reference Person Mobile ">
					</div>
				</div>
				<div class="form-group">
					<label for="designation" class="col-sm-2 control-label">Designation</label>
					<div class="col-sm-10">
						<input name="designation" value="{{$company->designation}}" type="text" class="form-control" id="designation" placeholder="Designation">
					</div>
				</div>
				<div class="form-group">
					<label for="phone_number" class="col-sm-2 control-label">Phone Number</label>
					<div class="col-sm-10">
						<input name="phone_number" value="{{$company->phone_number}}" type="text" class="form-control" id="phone_number" placeholder="Phone Number">
					</div>
				</div>
				<div class="form-group">
					<label for="file" class="col-sm-2 control-label">Attchment</label>
					<div class="col-sm-10">
						<input type="file" name="attchment" class="form-control" id="file"><br>
						@if ($company->attchment)
						<a href="{{ env('baseURL') }}/public/attchment/{{$company->attchment}}" target="_blank"> View</a>
						@endif
					</div>
				</div>
			</div>
		
			{{ csrf_field() }}
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
					<button onclick="window.history.go(-1); return false;" class="btn btn-danger" ><i class="fa fa-times" aria-hidden="true"></i> Back</button>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					@if(Session::has('message'))
			        	<p class="text-info">{{ Session::get('message') }}</p>
					@endif
				</div>
			</div>
		</form>
	</div>
</div> -->

</script>
@endsection

