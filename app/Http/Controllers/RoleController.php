<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Support\Facades\Hash;
use App\Role;
use App\Company;
use App\Masterrole;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;


class RoleController extends Controller{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	
	public function roleList(Request $request){
		$login_useid = session('userdata')->id;
		$role=Role::with('Masterrole')->where('added_by',$login_useid)->orderBy('created_at', 'desc')->get();
		// echo "<pre>";
		// print_r($role);die;
		return view("role.list", ["roles"=>$role, "page_title"=>"All Roles"]);
	}    

	public function addRole(Request $request){
		// print_r($request);die;
		$message = array();
		$validator = Validator::make($request->all(), [
            'name' => 'required',
            'master_id' => 'required',
        ]);
	        if ($validator->fails()) {
	        	$message['message'] = "Name required!";
				$message['type'] = "success";
	        	Session::flash('message', json_encode($message));
	        	return Redirect::to('roles');
	        }else{
	        	$inputdata=$request->input();
				// print_r($inputdata);die;
	        	if (Role::where('name', '=', $request['name'])->exists()) {
	        		$message['message'] = "Role with same name already exists!";
					$message['type'] = "success";
					Session::flash('message', json_encode($message));
		            return Redirect::to('role/add');
		        } else{
		        	$role=Role::create($inputdata);
		        	$message['message'] = "Role added successfully!";
					$message['type'] = "success";
					Session::flash('message', json_encode($message));
					return Redirect::to('roles');
		        }
	        }
	} 

	public function editRole(Request $request, $id = ''){
		$role=Role::find($id);
		$masterole = Masterrole::orderBy('name', 'asc')->get();
		if(empty($role)){
			return view("role.add", ["masteroles"=>$masterole,  "page_title"=>"Add Role"]);
		}else{
			 return view("role.edit", ["role"=>$role, "masteroles"=>$masterole, "page_title"=>"Edit Role"]);
		}
	}
	public function updateRole(Request $request){
		$id = $request->id;
		$message = array();
		$role=Role::find($id);
		if(empty($role)){
			return Redirect::back()->withErrors(['message', 'Role not found!']);
		}else{
			if(Role::where(function($result) use ($request) {
					$result->where("name" , $request['name'])->where('id' , '!=' ,$request->id);
				})->exists()) { 
				$message['message'] = "Role with same name already exists!";
				$message['type'] = "success";
				Session::flash('message', json_encode($message));

				// Session::flash('message', 'Company with same Title already exists!');
	            return Redirect::to('role/edit/'.$id);
			}else{
				$role->name=$request->name;
				$role->master_id=$request->master_id;
				$role->save();
				$message['message'] = "Role Updated!";
				$message['type'] = "success";
				Session::flash('message', json_encode($message));
				return Redirect::to('roles');
			}
		}       
	}
	public function deleteRole(Request $request, $id){
		$message = array();
		$role=Role::find($id);
		if(empty($role)){
			Session::flash('message', 'Sorry, Role record not found!');
			return Redirect::to('roles');
		}else{
			$allreadyrole=Role::with('Masterrole')->find($id);
			$allrole=$allreadyrole->Masterrole->count();
			if($allrole == 0){
				$role->delete();
				$message['message'] = "Role Deleted!";
				$message['type'] = "success";
			}else{
				$message['message'] = "Can not delete '$role->name' as it is currently being used in an active Visitor!";
				$message['type'] = "error";
				}
			Session::flash('message', json_encode($message));
			return Redirect::to('roles');
		}
	}
	public function rolestatus(Request $request, $id){
		$role=Role::find($id);
		$message = array();
		if(empty($role)){
			return Redirect::back()->withErrors(['message', 'Role not found!']);
		}else{
			if($role->status == '1'){
				$allreadyrole=Role::with('Masterrole')->find($id);
				$allrole=$allreadyrole->Masterrole->count();
				if($allrole == 0){
					$role->status='0';
					$message['message'] = "Role Deactivated!";
					$message['type'] = "success";
				}else{
					$message['message'] = "Can not disable '$role->name' as it is currently being used in an active Visitor!";
					$message['type'] = "error";
				}
			} else{
				$role->status='1';
				$message['message'] = "Role Activated!";
				$message['type'] = "success";
			}
			$role->save();

			Session::flash('message', json_encode($message));
			return Redirect::to('roles');
		}
	}
	
}
