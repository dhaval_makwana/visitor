@extends('hauper_admin') 
@section('content')
<div class="row">
	<div class="col-xs-2">
		<!-- <a href="{{ env('baseURL') }}/member/add" class="btn btn-block btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a> --><br>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<!-- /.box-header -->
			<div class="box-header">
				@if(Session::has('message'))
					<div class="alert alert-info alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
			</div>
			<div class="box-body">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Mail Report</th>
							<!-- <th>From Name</th> -->
							<th>Replay Email</th>
							<th>Subject</th>
							<th>Company/Entity Name</th>
							<th>Created Date & Time</th>
							<th>Mail Format</th>
							<!-- <th>Total Members</th> -->
						</tr>
					</thead>
					<tbody>
						@foreach ($balkmaillists as $balkmaillist)
						<tr>
							<td> <a href="{{ env('baseURL') }}/memberdetail/{{$balkmaillist->id}}">Member/Contact Mail Report</a></td>
							<!-- <td>{{$balkmaillist->fromname}}	</td> -->
							<td>{{$balkmaillist->email}}</td>
							<td>{{$balkmaillist->subject}}</td>
							<td>{{$balkmaillist->Company->name}}</td>
							<td>{{$balkmaillist->created_at}}</td>
							<td> <button class="btn btn-primary" type="button" onclick="holdResolutionMemberMail('{{$balkmaillist->id}}')"><i class="glyphicon glyphicon-envelope"></i></button></td>
						</tr>
						@endforeach 
					</tbody>
					<!--<tfoot>
						<tr>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Email</th>
							<th>Toggle</th>
							<th>Actions</th>
						</tr>
					</tfoot>-->
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
@endsection
@section('javascript')
<script src="https://cdn.ckeditor.com/4.10.0/full/ckeditor.js"></script>
<script>
	CKEDITOR.replace( 'signature',{
		toolbarGroups: [
		{ name: 'document',	   groups: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },			
 		{ name: 'clipboard',   groups: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },	
 		{ name: 'editing', 	   groups: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },		
 		{ name: 'forms', 	   groups: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
 		'/',																
 		{ name: 'basicstyles', groups: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
		{ name: 'paragraph', groups: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
		{ name: 'links', groups: [ 'Link', 'Unlink', 'Anchor' ] },
		{ name: 'insert', groups: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
		'/',
		// { name: 'styles', groups: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
		// { name: 'colors', groups: [ 'TextColor', 'BGColor' ] },
		// { name: 'tools', groups: [ 'Maximize', 'ShowBlocks' ] },
		// { name: 'about', groups: [ 'About' ] }
	]
	} );
</script>
<script>
// $(document).ready(function() {
//     $('#example1').DataTable( {
//         dom: 'Bfrtip',
//         buttons: [
//             'copy', 'csv', 'excel', 'pdf', 'print'
//         ]
//     } );
// } );

$(function () {
	$('#example1').DataTable();
})

function deleteSwal(url){
	swal({
		title: "Are you sure?",
		text: "You will not be able to recover this!",
		type: "warning",
		showCancelButton: true,
		confirmButtonClass: "btn-danger",
		confirmButtonText: "Yes, delete it!",
		cancelButtonText: "No, cancel!",
		closeOnConfirm: true,
		closeOnCancel: true
	},
	function(isConfirm) {
		if (isConfirm) {
			window.location.href = url;
		}
	});
}

function holdResolutionMemberMail(RESOLUTIONID){

    $.ajax({
        type: 'get',
        url: "{{env('baseURL')}}"+ "/get-mail-detail-by-id",
        data: {
            RESOLUTIONID: RESOLUTIONID,
        },
        success: function (data) {
        	console.log(data);
        	console.log(data.attchment);
            $("#Name").val(data.data.fromname);
            $("#Email").val(data.data.email);
            CKEDITOR.instances['signature'].setData(data.data.content);
		    
            $.each(data.attchment, function(index, value){
		        // $("#attchment").append('<a href="#">'+field.filename+'</a>');
		        $("#attchment").append("<a href='{{env('baseURL')}}/public/mail/attachment/"+value.filename +"'>" + value.filename + '</a><br>');
		        // $('#table > tbody').append('<tr><th>' + field + '</th><td>' + data[i][n] +  '</td></tr>')
		    });         
		  
        },
    });
    $("#update_mail_modal").modal("show");
}
</script>
@stop

<!-- Modal - Update User details -->
<div class="modal fade" id="update_mail_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Mail Content</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="Name" placeholder="Name" value="" class="form-control" readonly="" />
                </div>
                <div class="form-group">
                    <label for="Email">Email</label>
                    <input type="email" name="email" id="Email" placeholder="Email" value="" class="form-control" readonly="" />
                </div>
                <div class="form-group">
					<label for="signature" class="">Mail Body</label>
					<div class="">
						<textarea class="form-control editor" name="signature" cols="50" rows="10" id="signature" readonly="">  
						</textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="attchment" class="">Mail Attchment</label>
               		<div id="attchment"> </div>
               	</div>
            </div>

        </div>
    </div>
</div>
<!-- // Modal -->