@extends('hauper_admin') @section('content')
<div class='row'>
	<div class='col-md-12'>
		<form action="{{ env('baseURL') }}/user/add" method="post" class="form-horizontal" enctype="multipart/form-data">
			<div class="form-group">
				<label for="inputFName" class="col-sm-2 control-label">Full Name</label>
				<div class="col-sm-10">
					<input name="name" type="text" class="form-control" id="inputFName" placeholder="Full Name" required>
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail" class="col-sm-2 control-label">Email</label>
				<div class="col-sm-10">
					<input name="email" type="email" class="form-control" id="inputEmail" placeholder="Email" required>
				</div>
			</div>
			<div class="form-group">
				<label for="inputPhone" class="col-sm-2 control-label">Mobile No</label>
				<div class="col-sm-10">
					<input name="phone" type="text" pattern="\d*" title="Please input valid Mobile No" maxlength="10" minlength="10" class="form-control" id="inputPhone" placeholder="Mobile No">
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword" class="col-sm-2 control-label">Password</label>
				<div class="col-sm-10">
					<input name="password" type="password" class="form-control" id="inputPassword" placeholder="New Password" required>
				</div>
			</div>
			

			<div class="form-group">
				<label for="file" class="col-sm-2 control-label">Profile Pic</label>
				<div class="col-sm-10">
					<input type="file" name="file" class="form-control" id="file">
				</div>
			</div>
			<input name="type" type="hidden" value="0">
			<input name="added_by" type="hidden" value="{{Session::get('userdata')->id}}">
			{{ csrf_field() }}
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary">Save</button>
                    <button onclick="window.history.go(-1); return false;" class="btn btn-danger" >Cancel</button>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					@if(Session::has('message'))
			        	<p class="text-info">{{ Session::get('message') }}</p>
					@endif
				</div>
			</div>
		</form>
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
@endsection