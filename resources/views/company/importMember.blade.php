@extends('hauper_admin') 
@section('content')
<div class="row">
	
</div>
<div class='row'>
	<div class='col-md-12'>
		
 				@if (Session::has('errors'))
                    <div class="alert alert-info">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <p>{{ Session::get('errors') }}</p>
                    </div>
                @endif
                @if (Session::has('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <p>{{ Session::get('success') }}</p>
                    </div>
                @endif
            <form  action="{{ env('baseURL') }}/company/importExcel" class="form-horizontal" method="post" enctype="multipart/form-data">
            

 				<input name="added_by" type="hidden" value="{{Session::get('userdata')->id}}">
				{{ csrf_field() }}
               
 				<div class="form-group">
					<label for="member" class="col-sm-2 control-label">Visitor<span class="mandatory">*</span></label>
					<div class="col-sm-4"> 
               			<input type="file" name="import_file" accept=".xlsx, .xls" />
               			<p>only excle upload </p>
                	</div>
                	<div class="col-md-4">
                   	<a href="{{ env('baseURL') }}/public/excelformat/Visitor List & New update Excel sheet format.xlsx"><i class="glyphicon glyphicon-file"></i>Download Visitor List & New update Excel sheet format</a>
                </div>
           		</div>

           		<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button class="btn btn-primary">Import File</button>
                    <button onclick="window.history.go(-1); return false;" class="btn btn-danger" >Back</button>
				</div>
			</div>

            </form>
 
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
@endsection
@section('javascript')
<script>
$(function () {
	$('#example1').DataTable()
})

// $(document).ready(function () {
// 	$( "#member-inport-form" ).click(function(e) {
        
//         var appUrl ="{{env('baseURL')}}";
//         // alert(appUrl);
//         e.preventDefault();
// 	    $("#upload_excel").prop('disabled', true);
// 	    $("#excel_loading_img").show();
// 	    $("#submit_btn").prop('disabled', true);
	    
// 	    $.ajax({
// 	        method: 'post',
// 	        headers: {
// 	            'X-CSRF-Token': $("#_token").val()
// 	        },
// 	       	url: appUrl + "/member/importvarifiy",
// 	        dataType: 'json',
// 	        data: new FormData(this),
// 	        contentType: false,
// 	        cache: false,
// 	        processData: false,
// 	        success: function (resp) {
// 	            console.log(resp);
// 	        }
// 	});
// });
</script>
@stop