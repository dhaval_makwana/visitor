@extends('hauper_admin') 
@section('content')
<div class="row">
	<!-- <div class="col-xs-2">
		<a href="{{ env('baseURL') }}/member/add" class="btn btn-block btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a><br>
	</div>
	<div class="col-xs-2">
		<a href="{{ env('baseURL') }}/member/import" class="btn btn-block btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Import</a><br>
	</div>
	<div class="col-xs-2">
		<a href="{{ env('baseURL') }}/member/export" class="btn btn-block btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Export</a><br>
	</div> -->
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<!-- /.box-header -->
			<div class="box-header">
				@if(Session::has('message'))
					<div class="alert alert-info alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<p>{{ Session::get('message') }}</p>
					</div>
				@endif
			</div>
			<div class="box-body">
				<table id="example1" class="table table-bordered table-striped responsive">
					<thead>
						<tr>
							<th>Full Name</th>
							<th>Email</th>
							<th>Entitie/Company Name</th>
							<!-- <th>Added Via</th> -->
							<!-- <th>Added By</th> -->
							<th>Status</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($members as $member)
						<tr>
							<td>
								{{$member->name}}
								<!-- <a style="text-decoration:underline;" href="member/view/{{$member->id}}">
									
								</a> -->
							</td>
							<td>{{$member->email}}</td>
							<td>{{$member->Company->name}}</td>
							
                                            
							<td width="10%">
								@if ($member->active == '1')
								<a href="{{ env('baseURL') }}/member/togglestatus/{{$member->id}}" type="button" class="btn btn-block btn-info btn-xs">Active</a>
								@else
								<a href="{{ env('baseURL') }}/member/togglestatus/{{$member->id}}" type="button" class="btn btn-block btn-warning btn-xs">Inactive</a>
								@endif
							</td>
                            <td width="10%">
                            	<a href="{{ env('baseURL') }}/member/edit/{{$member->id}}" type="button" class="btn btn-block btn-info btn-xs">Edit</a>
                                <a onclick="deleteSwal('member/delete/{{$member->id}}')" type="button" class="btn btn-block btn-danger btn-xs">Delete</a>
							</td>
						</tr>
						@endforeach 
					</tbody>
					<!--<tfoot>
						<tr>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Email</th>
							<th>Toggle</th>
							<th>Actions</th>
						</tr>
					</tfoot>-->
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
@endsection
@section('javascript')
<script>
$(function () {
	$('#example1').DataTable();
})
function deleteSwal(url){
	swal({
		title: "Are you sure?",
		text: "You will not be able to recover this!",
		type: "warning",
		showCancelButton: true,
		confirmButtonClass: "btn-danger",
		confirmButtonText: "Yes, delete it!",
		cancelButtonText: "No, cancel!",
		closeOnConfirm: true,
		closeOnCancel: true
	},
	function(isConfirm) {
		if (isConfirm) {
			window.location.href = url;
		}
	});
}
</script>
@stop