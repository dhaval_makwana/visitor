<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Company;

class Company extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'companies';

    protected $guarded = [
        'id'
    ];    
    public function Member()
    {
        return $this->hasMany(Member::class);
    }
    public function Resolution(){
        return $this->hasMany(Resolution::class);
    }

    public function User(){
        return $this->belongsTo(User::class,'added_by');
    }
    
    // public function Department(){
    //     return $this->belongsTo(Department::class);
    // }
    // public function Designation(){
    //     return $this->belongsTo(Designation::class);
    // }
    // public function Pincode(){
    //     return $this->belongsTo(Pincode::class);
    // }
    // public function State(){
    //     return $this->belongsTo(State::class);
    // }

    public function Designation(){
        return $this->belongsTo(Role::class,'designation_id');
    }

    public function Department(){
        return $this->belongsTo(Role::class,'department_id');
    }
    public function Pincode(){
        return $this->belongsTo(Role::class,'pincode_id');
    }
    public function State(){
        return $this->belongsTo(Role::class,'state_id');
    }
}
